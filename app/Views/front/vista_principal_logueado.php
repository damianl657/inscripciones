<form method="POST" action="<?php echo site_url('datos/inicio_alumno/'); ?><?php echo $idcolegio; ?>" id="formlogin">
	<div class="home-azul">
		<div class="container">
			<div class="row">
				<div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
					<header>
						<h1><?php echo $titulo_web; ?></h1>
						<hr />
						<h3>Aquí prodrá realizar la reserva de la matrícula para el ciclo lectivo 2021 y correspondiente inscripción. Se le solicitará fotos legibles de todos los requisitos.</h3>
						<hr />
						<h2>Requisitos disponibles para descargar</h2>
						<h5><?php echo $requisitos; ?></h5>
						<br>
					</header>
				</div>
				<div id=ingreso class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 ">
					<div class="container-fluid ingreso">
						<h4 class="d-block text-center">
							Ingrese el documento del alumno:
						</h4>
						<div class="form-row">
							<div class="form-group col-lg-12">
								<input type="text" class="form-control" id="documento" name="documento" placeholder="Documento" pattern=".{5,15}" title="Ingrese un documento correcto" autocomplete="off">
								<input type="text" class="" id="documento_tutor" name="documento_tutor" hidden>
							</div>
						</div>
						<br>
						<button class="btn btn-primary" type="button" id="comprobar_si_alumno" name="comprobar_si_alumno" onclick="comprobar_alumno()">Comenzar </button>
						<button class="btn btn-primary" type="button" id="spinner" name="spinner" disabled hidden>
							<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
							Ingresando...
						</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	function comprobar_alumno() {
		if (jQuery('#documento').val().length == 0) {
			Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'El documento no puede ser vacio'
			});
		} else {
			jQuery("#spinner").attr("hidden", false);
			jQuery("#comprobar_si_alumno").hide();
			jQuery.ajax({
				method: "POST",
				url: '<?php echo site_url("front/revisar_inscripcion_relacion/") ?>',
				dataType: "JSON",
				data: {
					documento: document.getElementById("documento").value,
					idcolegio: <?php echo $idcolegio; ?>,
				}
			}).done(function(data) {
				//console.log(data);
				
				if (data['status'] == true) {
					/*
					Swal.fire({
						icon: 'success',
						title: 'El alumno se encuentra inscripto ',
						text: 'El número de inscripción es: Nº0000' + data['id']
					});
					*/
					window.location.replace('<?php echo site_url("alumnos/legajo/") ?>');
			
					jQuery("#spinner").attr("hidden", true);
					jQuery("#comprobar_si_alumno").show();
				} else if (data['relacion'] == true && $data['corresponde_alumno_nivel']==true) {
					jQuery("#documento_tutor").val(data['documento_tutor']);
					document.forms['formlogin'].submit();
				} else if ($data['relacion']==false) {
					Swal.fire({
						icon: 'info',
						title: 'No puede inscribir al alumno ',
						text: 'No encontramos una relación de parentesco en nuestra base de datos'
					});
					jQuery("#spinner").attr("hidden", true);
					jQuery("#comprobar_si_alumno").show();

				} else{
					Swal.fire({
						icon: 'info',
						title: 'No puede inscribir al alumno ',
						text: 'El nivel (inicial, primario, secundario) elegido no es correcto'
					});
					jQuery("#spinner").attr("hidden", true);
					jQuery("#comprobar_si_alumno").show();

				}

			});
		}
		/*
		Swal.fire({
				icon: 'info',
				title: 'El sistema se habilitará según el siguiente orden',
				html: '<b>- Primaria</b> 5 de octubre<br><b>- Secundaria</b> 8 de octubre<br><b>- Inicial</b> 12 de octubre'
			});
			*/
	}

	jQuery(document).ready(function() {




	});
</script>