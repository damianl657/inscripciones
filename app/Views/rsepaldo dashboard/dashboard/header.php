<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Administracíon de Inscripciones</title>
  <link rel="shortcut icon" href="<?=base_url()?>/public/demo/img/favicon.ico?v=2" type="image/x-icon">
	<link rel="icon" href="<?=base_url()?>/public/demo/img/favicon.ico?v=2" type="image/x-icon">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url()?>/public/demo/plugins/fontawesome-free/css/all.min.css">
   <!-- overlayScrollbars -->
   <link rel="stylesheet" href="<?=base_url()?>/public/demo/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>/public/demo/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?=base_url()?>/public/demo/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=base_url()?>/public/demo/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=base_url()?>/public/demo/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=base_url()?>/public/demo/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="<?=base_url()?>/public/demo/css/generic.css">
     <!--  extension responsive  -->
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css">
     <style>
    table thead{
        background-color: #bad6f3;        
    }
    </style>
  
</head>