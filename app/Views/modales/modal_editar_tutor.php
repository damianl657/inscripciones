<!-- Modal Observaciones -->
<form method="POST" action="" id="editar_tutor">
  <div id="modal_editar_tutor" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Editar Tutor</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">
          <input type="hidden" id="tutor_id" name="tutor_id" value="">

          <div class="row">
            <div class="form-group col-md-6">
              <label for="nombre_tutor_edit">Nombre</label>
              <input type="text" class="form-control" id="nombre_tutor_edit" name="nombre_tutor_edit" maxlength="50" required>
            </div>

            <div class="form-group col-md-6">
              <label for="apellido_tutor_edit">Apellido</label>
              <input type="text" class="form-control" id="apellido_tutor_edit" name="apellido_tutor_edit" maxlength="50" required>
            </div>
          </div>
          <br>

          <div class="row">
             <div class="form-group col-md-6">
              <label for="documento_tutor_edit">Documento</label>
              <input type="text" class="form-control" id="documento_tutor_edit" name="documento_tutor_edit" maxlength="50" required>
            </div>

            <div class="form-group col-md-3">
              <label for="rol_tutor_edit">Rol</label>
              <select class="form-control" id="rol_tutor_edit" name="rol_tutor_edit">
                <option value=1>Padre</option>
                <option value=2>Madre</option>
                <option value=3>Tutor</option>
              </select>
            </div>

            <div class="form-group col-md-3">
              <label for="vive_tutor_edit">Vive?</label>
              <select class="form-control" id="vive_tutor_edit" name="vive_tutor_edit">
                <option value=1>Si</option>
                <option value=0>No</option>
              </select>
            </div>
          </div>
          <br>

          <div class="row">
            <div class="form-group col-md-6">
              <label for="correo_tutor_edit">Correo</label>
              <input type="email" class="form-control" id="correo_tutor_edit" name="correo_tutor_edit" maxlength="50">
            </div>

            <div class="form-group col-md-3">
              <label for="telefono1_tutor_edit">Telefono Particular</label>
              <input type="text" class="form-control" id="telefono1_tutor_edit" name="telefono1_tutor_edit" maxlength="50">
            </div>

            <div class="form-group col-md-3">
              <label for="telefono2_tutor_edit">Telefono Laboral</label>
              <input type="text" class="form-control" id="telefono2_tutor_edit" name="telefono2_tutor_edit" maxlength="50">
            </div>
          </div>
          <br>

          <div class="row">
            <div class="form-group col-md-6">
              <label for="domicilio_tutor_edit">Domicilio</label>
              <input type="text" class="form-control" id="domicilio_tutor_edit" name="domicilio_tutor_edit">
            </div>
          </div>
          
          <div class="row">
            <div class="form-group col-md-6">
              <label for="ocupacion_tutor_edit">Ocupacion</label>
              <input type="email" class="form-control" id="ocupacion_tutor_edit" name="ocupacion_tutor_edit" maxlength="50">
            </div>

            <div class="form-group col-md-6">
              <label for="empresa_tutor_edit">Empresa</label>
              <input type="text" class="form-control" id="empresa_tutor_edit" name="empresa_tutor_edit" maxlength="50">
            </div>
            
          </div>
          <br>

          <div class="row">
            <div class="form-group col-md-6">
              <label for="cargo_tutor_edit">Cargo</label>
              <input type="text" class="form-control" id="cargo_tutor_edit" name="cargo_tutor_edit" maxlength="50">
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="editar_tutor()">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </div>
</form>