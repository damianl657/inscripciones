<div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1" id="modal_bloqueo">
    <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
    <div class="card">
  <h5 class="card-header">Inscripción pausada</h5>
  <div class="card-body">
    <h5 class="card-title">Motivo:</h5>
    <p class="card-text">La solicitud de inscripcion no puede continuar para este alumno, debe comunicarse con la estableciomiento educativo para resolver el problema</p>
   
  </div>
</div>
    </div>
</div>