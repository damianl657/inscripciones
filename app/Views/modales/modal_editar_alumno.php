<!-- Modal Observaciones -->
<form method="POST" action="" id="editar_alumno">
  <div id="modal_editar_alumno" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Editar Alumno</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <input type="text" id="id_alumno_editar" name="id_alumno_editar" value="<?php echo $alumno[0]["id"]; ?>" hidden>
          <input type="text" id="id_preinscripcion_editar" name="id_preinscripcion_editar" value="<?php echo $id_preinscripcion; ?>" hidden>
          <div class="row">
            <div class="form-group col-md-3">
              <label for="sala_alumno">Reserva en el nivel:</label>
              <select class="form-control" id="reserva_nivel_alumno_editar" name="reserva_nivel_alumno_editar">
                <option value="4" <?= $anio_nivel_reserva[0]["nombre_nivel"] == 'INICIAL' ? ' selected="selected"' : ''; ?>>Inicial</option>
                <option value="3" <?= $anio_nivel_reserva[0]["nombre_nivel"] == 'PRIMARIO' ? ' selected="selected"' : ''; ?>>Primario</option>
                <option value="1" <?= $anio_nivel_reserva[0]["nombre_nivel"] == 'SECUNDARIA' ? ' selected="selected"' : ''; ?>>Secundario</option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label for="anio_alumno_editar">Reserva en:</label>
              <select class="form-control" id="sala_alumno_editar" name="sala_alumno_editar">
                <option value="10" <?= $anio_nivel_reserva[0]["anio_reserva"] == 'Sala de 4' ? ' selected="selected"' : ''; ?>>Sala de 4</option>
                <option value="11" <?= $anio_nivel_reserva[0]["anio_reserva"] == 'Sala de 5' ? ' selected="selected"' : ''; ?>>Sala de 5</option>
              </select>
              <select class="form-control " id="anio_alumno_editar" name="anio_alumno_editar">
                <option value="1" <?= $anio_nivel_reserva[0]["anio_reserva"] == '1º' ? ' selected="selected"' : ''; ?>>1º Año</option>
                <option value="2" <?= $anio_nivel_reserva[0]["anio_reserva"] == '2º' ? ' selected="selected"' : ''; ?>>2º Año</option>
                <option value="3" <?= $anio_nivel_reserva[0]["anio_reserva"] == '3º' ? ' selected="selected"' : ''; ?>>3º Año</option>
                <option value="4" <?= $anio_nivel_reserva[0]["anio_reserva"] == '4º' ? ' selected="selected"' : ''; ?>>4º Año</option>
                <option value="5" <?= $anio_nivel_reserva[0]["anio_reserva"] == '5º' ? ' selected="selected"' : ''; ?>>5º Año</option>
                <option value="6" <?= $anio_nivel_reserva[0]["anio_reserva"] == '6º' ? ' selected="selected"' : ''; ?>>6º Año</option>
              </select>
              <select class="form-control" id="grado_alumno_editar" name="grado_alumno_editar">
                <option value="1" <?= $anio_nivel_reserva[0]["anio_reserva"] == '1º' ? ' selected="selected"' : ''; ?>>1º Grado</option>
                <option value="2" <?= $anio_nivel_reserva[0]["anio_reserva"] == '2º' ? ' selected="selected"' : ''; ?>>2º Grado</option>
                <option value="3" <?= $anio_nivel_reserva[0]["anio_reserva"] == '3º' ? ' selected="selected"' : ''; ?>>3º Grado</option>
                <option value="4" <?= $anio_nivel_reserva[0]["anio_reserva"] == '4º' ? ' selected="selected"' : ''; ?>>4º Grado</option>
                <option value="5" <?= $anio_nivel_reserva[0]["anio_reserva"] == '5º' ? ' selected="selected"' : ''; ?>>5º Grado</option>
                <option value="6" <?= $anio_nivel_reserva[0]["anio_reserva"] == '6º' ? ' selected="selected"' : ''; ?>>6º Grado</option>
              </select>
            </div>

            <div class="form-group col-md-3">
              <label for="documento_alumno_editar">Documento</label>
              <input type="text" class="form-control" id="documento_alumno_editar" name="documento_alumno_editar">
            </div>

            <div class="form-group col-md-3">
              <label for="estado_reserva_alumno_editar"> Estado de reserva:</label>
              <select class="form-control" id="estado_reserva_alumno_editar" name="estado_reserva_alumno_editar">
                <option value="0" <?= $alumno[0]["aceptado"] == '0' ? ' selected="selected"' : ''; ?>>Rechazada</option>
                <option value="1" <?= $alumno[0]["aceptado"] == '1' ? ' selected="selected"' : ''; ?>>Pendiente</option>
                <option value="2" <?= $alumno[0]["aceptado"] == '2' ? ' selected="selected"' : ''; ?>>Completado 1</option>
                <option value="3" <?= $alumno[0]["aceptado"] == '3' ? ' selected="selected"' : ''; ?>>Completado 2</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-5">
              <label for="nombre_alumno_editar">Nombre</label>
              <input type="text" class="form-control" id="nombre_alumno_editar" name="nombre_alumno_editar">
            </div>
            <div class="form-group col-md-5">
              <label for="apellido_alumno_editar">Apellido</label>
              <input type="text" class="form-control" id="apellido_alumno_editar" name="apellido_alumno_editar">
            </div>
            <div class="form-group col-md-2">
              <label for="legajo_alumno_editar">Legajo</label>
              <input type="text" class="form-control" id="legajo_alumno_editar" name="legajo_alumno_editar">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-9">
              <label for="domicilio_alumno_editar">Domicilio</label>
              <input type="text" class="form-control" id="domicilio_alumno_editar" name="domicilio_alumno_editar">
            </div>
            <div class="form-group col-md-3">
              <label for="fecha_nac_alumno_editar">Fecha de Nacimiento</label>
              <input type="date" class="form-control" id="fecha_nac_alumno_editar" name="fecha_nac_alumno_editar">
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
              <label for="correo_alumno_editar">Correo</label>
              <input type="text" class="form-control" id="correo_alumno_editar" name="correo_alumno_editar">
            </div>
            <div class="form-group col-md-3">
              <label for="provincia_alumno_editar">Provincia</label>
              <input type="text" class="form-control" id="provincia_alumno_editar" name="provincia_alumno_editar" value="<?php echo $alumno[0]["provincia_id"]; ?>" data-value-property='id' data-selection-required='true'>
            </div>
            <div class="form-group col-md-3">
              <label for="ciudad_alumno_editar">Departamento</label>
              <input type="text" class="form-control" id="ciudad_alumno_editar" name="ciudad_alumno_editar">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="editar_alumno()">Guardar</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </div>
</form>