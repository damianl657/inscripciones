<!-- Modal Observaciones -->
<form method="POST" action="" id="observacion">
<div id="modal_observacion" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Observaciones</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <input id="id_pre" name="id_pre" type="text" value="<?php echo $id_preinscripcion; ?>" hidden>
      <input type="text" id="txt_observacion" name="txt_observacion" class="form-control"> 
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="editar_observacion()">Guardar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
</form>