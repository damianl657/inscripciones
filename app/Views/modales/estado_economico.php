<form method="POST" action="" id="editar_economico">
<div id="modal_economico" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Pago de matrícula</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <input id="id_pre" name="id_pre" type="text" hidden>
      <div class="form-group">
    <label for="select_economico">Seleccione estado:</label>
    <select class="form-control" id="select_economico" name="select_economico">
    <option value="1">Pendiente</option>
    <option value="2">Aprobado</option>
    </select>
  </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="editar_estado_economico()">Guardar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
</form>
