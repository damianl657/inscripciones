<form method="POST" action="<?php echo site_url('datos/preinscripcion_alumno_admin/'); ?>" id="pre_alumno">
    <div id="reserva_alumno" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Preinscripción</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <input id="id_col" name="id_col" type="text" hidden>
                    <div class="form-group">
                        <label for="select_economico">Seleccione a quien va a inscribir:</label>
                        <select class="form-control" id="select_tipo_alu" name="select_tipo_alu">
                            <option value="1">Ingresante</option>
                            <option value="2">Alumno</option>
                        </select>
                        <div class="form-group ">
                            <label for="documento_alumno_preinscribir">Ingrese el documento:</label>
                            <input type="text" class="form-control" id="documento_alumno_preinscribir" name="documento_alumno_preinscribir" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="verificar_alumno()">Comenzar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</form>