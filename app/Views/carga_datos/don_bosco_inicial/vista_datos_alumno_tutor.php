<div class="container ">
	<div class="row">
		<form method="POST" id="regForm" autocomplete="off" action="" onsubmit="enviar()" enctype="multipart/form-data" accept-charset="utf-8">
			<input type="text" id="colegio_id" name="colegio_id" value="<?php echo $idcolegio; ?>" hidden>
			<h1>Anexo 1 - Solicitud de Inscripción - Declaración Jurada </h1>

			<div class="tab ventana"><strong><u>
						<p style="font-size:23px">Complete con los datos del ALUMNO:</p>
					</u></strong>
				<br><br>
				<div class="row">
					<p>
						<div class="form-group col-sm-4">
							<label for="nombre_alumno">Nombres:*</label>
							<input type="text" class="" id="alumno_id" name="alumno_id" value="<?php echo $alumno["id"]; ?>" hidden>
							<input type="text" class="form-control validar" id="nombre_alumno" name="nombre_alumno" placeholder="Como figura en el documento" pattern="[A-Za-z]" title="Ingrese un nombre correcto" value="<?php echo $alumno["nombre"]; ?>" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-4">
							<label for="apellido_alumno">Apellidos:*</label>
							<input type="text" class="form-control validar" id="apellido_alumno" name="apellido_alumno" placeholder="Como figura en el documento" pattern="[A-Za-z]" title="Ingrese un nombre correcto" value="<?php echo $alumno["apellido"]; ?>" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-4">
							<label for="documento_alumno">Documento:*</label>
							<input type="text" class="form-control validar" id="documento_alumno" name="documento_alumno" placeholder="Documento" value="<?php echo $alumno["documento"]; ?>" readonly>
						</div>
					</p>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="domicilio_alumno">Domicilio:*</label>
							<input type="text" class="form-control validar" id="domicilio_alumno" name="domicilio_alumno" placeholder="Domicilio donde vive el alumno" value="<?php echo $alumno["domicilio"]; ?>" required>
						</div>

					</p>

					<p>

						<div class="form-group col-sm-3">

							<label for="fecha_nacimiento_alumno">Fecha de nacimiento:*</label>

							<input class="form-control validar" type="date" id="fecha_nacimiento_alumno" name="fecha_nacimiento_alumno" value="<?php echo $alumno["fecha_nac"]; ?>" required>

						</div>

					</p>

					<p>

						<div class="form-group col-sm-3" id="div_sexo_alumno">

							<label for="sexo_alumno">Selecione el sexo:*</label>

							<select class="form-control flexdatalist" id="sexo_alumno" name="sexo_alumno" onchange="this.options[0].selected=true">

								<option value="M">Masculino</option>

								<option value="F">Femenino</option>

								<!--<option value="O">Otro</option>-->

							</select>

						</div>

					</p>

				</div>

				<br>

				<div class="row">

					<p>

						<div class="form-group col-sm-4">

							<label for="provincia_alumno">Provincia:*</label>

							<input type="text" class="form-control validar" id="provincia_alumno" name="provincia_alumno" placeholder="Provincia" value="<?php echo $alumno["documento"]; ?>" required>

						</div>

					</p>

					<p>

						<div class="form-group col-sm-4">

							<label for="ciudad_alumno">Departamento:*</label>

							<input type="text" class="form-control validar" id="ciudad_alumno" name="ciudad_alumno" placeholder="Ciudad" value="<?php echo $alumno["ciudad"]; ?>" required>
						</div>

					</p>
					<p>
						<div class="form-group col-sm-4">
							<label for="nacionalidad_alumno">Indique la nacionalidad:</label>
							<input type="text" class="form-control validar" id="nacionalidad_alumno" name="nacionalidad_alumno" placeholder="Nacionalidad" value="<?php echo $alumno["documento"]; ?>" required>

						</div>

					</p>

				</div>

				<br>

				<div class="row">

					<p>

						<div class="form-group col-sm-4">

							<label for="correo_alumno">Correo:</label>

							<input type="email" class="form-control" id="correo_alumno" name="correo_alumno" placeholder="Correo del alumno" value="<?php echo $alumno["correo"]; ?>" required>

						</div>

					</p>

					<p>
						<div class="form-group col-sm-3">
							<label for="correo_alumno">Teléfono:</label>
							<input type="text" class="form-control" id="telefono_alumno" name="telefono_alumno" placeholder="Fijo / Celular" value="<?php echo $alumno["telefono"]; ?>" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-5">
							<label for="sala_alumno">Sala en el cual desea inscribirse:*</label>
							<select class="form-control flexdatalist validar" id="anio_alumno" name="anio_alumno">
								<option value="10">Sala de 4</option>
								<option value="11">Sala de 5</option>
							</select>
						</div>
					</p>
				</div>
			</div>

			<div class="tab ventana"><strong><u>
						<p style="font-size:23px">Datos del adulto responsable 1:</p>
					</u></strong>
				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="nombre_tutor1">Nombres:*</label>
							<input type="text" class="" id="tutor1_id" name="tutor1_id" value="<?php echo $tutor1["id"]; ?>" hidden>
							<input type="text" class="form-control validar" id="nombre_tutor1" name="nombre_tutor1" placeholder="Como figura en el documento" value="<?php echo $tutor1["nombre"]; ?>" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-6">
							<label for="apellido_tutor1">Apellidos:*</label>
							<input type="text" class="form-control validar" id="apellido_tutor1" name="apellido_tutor1" placeholder="Como figura en el documento" value="<?php echo $tutor1["apellido"]; ?>" required>
						</div>

					</p>

				</div>

				<br>

				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="domicilio_tutor1">Domicilio:*</label>
							<input type="text" class="form-control validar" id="domicilio_tuto1" name="domicilio_tutor1" placeholder="Domicilio donde vive" value="<?php echo $tutor1["domicilio"]; ?>" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-3">
							<label for="documento_tutor1">Documento:*</label>
							<input type="text" class="form-control validar" id="documento_tutor1" name="documento_tutor1" placeholder="Sin puntos" value="<?php echo $tutor1["documento"]; ?>" required>
						</div>
					</p>
					<p>
						<div class="form-group col-md-3">
							<label for="relacion_tutor2">Vive el adulto</label>
							<select class="form-control validar" id="vive_tutor1" name="vive_tutor1">
								<option value="1">SI</option>
								<option value="0">NO</option>
							</select>
						</div>
					</p>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-3">
							<label for="tel_particular_tutor1">Teléfono Particular:*</label>
							<div class="input-group ">
								<input type="text" class="form-control validar" id="tel_particular_tutor1" name="tel_particular_tutor1" placeholder="Solo números" value="<?php echo $tutor1["telefono_particular"]; ?>">
							</div>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-3">
							<label for="tel_laboral_tutor1">Teléfono laboral:*</label>
							<div class="input-group ">
								<input type="text" class="form-control validar" id="tel_laboral_tutor1" name="tel_laboral_tutor1" placeholder="Solo números" value="<?php echo $tutor1["telefono_laboral"]; ?>">
							</div>
						</div>
					</p>
					<p>
						<div class="form-group col-6">
							<label for="relacion_tutor2">Relación con el alumno:*</label>
							<select class="form-control flexdatalist validar" id="relacion_tutor1" name="relacion_tutor1">
								<option value="3" <?= $tutor1["rol"] == '3' ? ' selected="selected"' : ''; ?>>Tutor</option>
								<option value="1" <?= $tutor1["rol"] == '1' ? ' selected="selected"' : ''; ?>>Padre</option>
								<option value="2" <?= $tutor1["rol"] == '2' ? ' selected="selected"' : ''; ?>>Madre</option>
							</select>
						</div>
					</p>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="ocupacion_tutor1">Ocupación:*</label>
							<input type="text" class="form-control validar" id="ocupacion_tutor1" name="ocupacion_tutor1" placeholder="Indique la ocupación" value="<?php echo $tutor1["ocupacion"]; ?>" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-6">
							<label for="empresa_tutor1">Empresa/Organismo:*</label>
							<input type="text" class="form-control validar" id="empresa_tutor1" name="empresa_tutor1" placeholder="Empresa / organismo" value="<?php echo $tutor1["empresa"]; ?>" required>
						</div>
					</p>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="ocupacion_tutor1">Cargo:*</label>
							<input type="text" class="form-control validar" id="cargo_tutor1" name="cargo_tutor1" placeholder="Cargo que posee en la empresa" value="<?php echo $tutor1["cargo"]; ?>" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-6">
							<label for="correo_tutor1">Correo:*</label>
							<input type="email" class="form-control validar" id="correo_tutor1" name="correo_tutor1" placeholder="Correo que utiliza" value="<?php echo $tutor1["correo"]; ?>" required>
						</div>
					</p>
				</div>
			</div>



			<div class="tab ventana"><strong><u>
						<p style="font-size:23px">Datos del adulto responsable 2:</p>
					</u></strong>
				<div class="row">
					<div class="form-group col-md-8">
						¿El alumno tiene dos adultos responsables? NO
						<label class="switch">
							<input type="checkbox" id="posee_adulto2" name="posee_adulto2" checked>
							<span class="slider round"></span>
						</label> SI
					</div>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="nombre_tutor2">Nombres:*</label>
							<input type="text" class="form-control validar" id="nombre_tutor2" name="nombre_tutor2" placeholder="Como figura en el documento" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-6">
							<label for="apellido_tutor2">Apellidos:*</label>
							<input type="text" class="form-control validar" id="apellido_tutor2" name="apellido_tutor2" placeholder="Como figura en el documento" required>
						</div>
					</p>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="domicilio_tutor2">Domicilio:*</label>
							<input type="text" class="form-control validar" id="domicilio_tutor2" name="domicilio_tutor2" placeholder="Domicilio donde vive" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-3">
							<label for="documento_tutor2">Documento:*</label>
							<input type="text" class="form-control validar" id="documento_tutor2" name="documento_tutor2" placeholder="Sin puntos" value="" required>
						</div>
					</p>
					<p>
						<div class="form-group col-md-3">
							<label for="relacion_tutor2">Vive el adulto</label>
							<select class="form-control validar" id="vive_tutor2" name="vive_tutor2">
								<option value="1">SI</option>
								<option value="0">NO</option>
							</select>
						</div>
					</p>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-3">
							<label for="tel_particular_tutor2">Teléfono Particular:*</label>
							<div class="input-group ">
								<input type="text" class="form-control validar" id="tel_particular_tutor2" name="tel_particular_tutor2" placeholder="Solo números">
							</div>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-3">
							<label for="tel_laboral_tutor1">Teléfono laboral:*</label>
							<div class="input-group ">
								<input type="text" class="form-control validar" id="tel_laboral_tutor2" name="tel_laboral_tutor2" placeholder="Solo números">
							</div>

						</div>

					</p>
					<p>
						<div class="form-group col-sm-6">
							<label for="relacion_tutor2">Relación con el alumno:*</label>
							<select class="form-control flexdatalist validar" id="relacion_tutor2" name="relacion_tutor2">
								<option value="3">Tutor</option>
								<option value="1">Padre</option>
								<option value="2">Madre</option>
							</select>
						</div>
					</p>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="ocupacion_tutor2">Ocupación:*</label>
							<input type="text" class="form-control validar" id="ocupacion_tutor2" name="ocupacion_tutor2" placeholder="Indique la ocupación" required>
						</div>
					</p>
					<p>
						<div class="form-group col-sm-6">
							<label for="empresa_tutor2">Empresa/Organismo:*</label>
							<input type="text" class="form-control validar" id="empresa_tutor2" name="empresa_tutor2" placeholder="Empresa/organismo donde trabaja" required>
						</div>
					</p>
				</div>
				<br>
				<div class="row">
					<p>
						<div class="form-group col-sm-6">
							<label for="ocupacion_tutor2">Cargo:*</label>
							<input type="text" class="form-control validar" id="cargo_tutor2" name="cargo_tutor2" placeholder="Cargo que posee en la empresa" required>

						</div>
					</p>
					<p>
						<div class="form-group col-sm-6">
							<label for="correo_tutor2">Correo:*</label>
							<input type="email" class="form-control validar" id="correo_tutor2" name="correo_tutor2" placeholder="Correo que utiliza" required>
						</div>
					</p>
				</div>
			</div>

			<div class="tab ventana"><strong><u>
						<p style="font-size:23px">Debe adjuntar las siguientes imágenes:</p>
					</u></strong>
				<p>
					<div class="alert alert-primary" role="alert">
						Al adjuntar una imagen espere que aparezca el tilde verde, <img src="http://inscripciones.e-nodos.com/public/demo/img/tildeve2.png" alt="Se adjunto correctamente" height="20" width="20"> la demora dependerá de su conexión a internet. Debe adjuntar todas las imágenes solicitadas para poder realizar la reserva de matrícula.
					</div>

				</p>

				<p>

					<div id="documento_alumno_archivo" class="form-group"><label style="font-size:18px">Documento del alumno (frente y dorso)</label>

						<input type="file" name="files[]" id="filer_input" class="validar" multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Seleccione o arrastre hasta 5 archivos" />

						<ul id="listarchivos" style="padding-top:4px"></ul>

					</div>

				</p>

				<p>

					<div id="documento_tutor1_archivo" class="form-group"><label style="font-size:18px">Documento del adulto responsable 1(frente y dorso)</label>

						<input type="file" name="files[]" id="filer_input1" class="validar" multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Seleccione o arrastre hasta 5 archivos" />

						<ul id="listarchivos1" style="padding-top:4px"></ul>

					</div>

				</p>
				<p>
					<div id="documento_tutor2_archivo" class="form-group"><label style="font-size:18px">Documento del adulto responsable 2(frente y dorso) si existe</label>
						<input type="file" name="files[]" id="filer_input2" class="" multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Seleccione o arrastre hasta 5 archivos" />
						<ul id="listarchivos2" style="padding-top:4px"></ul>
					</div>
				</p>
			</div>
			<div class="tab ventana"><strong><u>
						<p style="font-size:23px">Para finalizar adjunte las siguientes imágenes:</p>
					</u></strong>
				<p>
					<div class="alert alert-primary" role="alert">
						Al adjuntar una imagen espere que aparezca el tilde verde, <img src="http://inscripciones.e-nodos.com/public/demo/img/tildeve2.png" alt="Se adjunto correctamente" height="20" width="20"> la demora dependerá de su conexión a internet. Debe adjuntar todas las imágenes solicitadas para poder realizar la reserva de matrícula.
					</div>
				</p>
				<p>
					<div id="foto_alumno_archivo" class="form-group"><label style="font-size:18px">Una foto actualizada del alumno</label>
						<input type="file" name="files[]" id="filer_input3" class="validar" multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Seleccione o arrastre hasta 5 archivos" />
						<ul id="listarchivos3" style="padding-top:4px"></ul>
					</div>
				</p>
				<p>
					<div id="libre_deuda_alumno_archivo" class="form-group"><label style="font-size:18px">Libre deuda o plan de pago del colegio</label>
						<input type="file" name="files[]" id="filer_input4" class="validar" multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Seleccione o arrastre hasta 5 archivos" />
						<ul id="listarchivos4" style="padding-top:4px"></ul>
					</div>
				</p>
				<p>
					<div id="boleta_servicio_alumno_archivo" class="form-group"><label style="font-size:18px">Boleta de servicio donde figure la dirección actual</label>
						<input type="file" name="files[]" id="filer_input5" class="validar" multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Seleccione o arrastre hasta 5 archivos" />
						<ul id="listarchivos5" style="padding-top:4px"></ul>
					</div>
				</p>

			</div>
			<div style="overflow:auto;">
				<div style="float:right;">
					<button type="button" class="form-control boton" id="prevBtn" onclick="nextPrev(-1)">Atrás</button>
					<button type="button" class="form-control boton" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
					<button type="button" class="form-control boton" id="reservando" name="reservando" disabled hidden>
						<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
						Finalizando, espere...
					</button>
				</div>
			</div>

			<!-- Circles which indicates the steps of the form: -->
			<div style="text-align:center;margin-top:40px;">
				<span class="step"></span>
				<span class="step"></span>
				<span class="step"></span>
				<span class="step"></span>
				<span class="step"></span>
			</div>
		</form>
	</div>
</div>
</div>

<!-- Script para el manejo de del wizard de carga de datos -->
<script type="text/javascript">
	var currentTab = 0; // La pestaña actual está configurada para ser la primera pestaña (0)
	showTab(currentTab); // Mostrar la pestaña actual

	function showTab(n) {
		// Esta función mostrará la pestaña específica del formulario ...
		var x = document.getElementsByClassName("ventana");
		x[n].style.display = "block";
		// ... y arregle los botones Anterior / Siguiente:
		if (n == 0) {
			document.getElementById("prevBtn").style.display = "none";
		} else {
			document.getElementById("prevBtn").style.display = "";
		}
		if (n == (x.length - 1)) {
			document.getElementById("nextBtn").innerHTML = "Finalizar";
		} else {
			document.getElementById("nextBtn").innerHTML = "Siguiente";
		}
		// ... y ejecute una función que muestre el indicador de paso correcto:
		fixStepIndicator(n)
	}

	function nextPrev(n) {
		// Esta función determinará qué pestaña mostrar
		var x = document.getElementsByClassName("ventana");
		// Salga de la función si algún campo de la pestaña actual no es válido:
		if (n == 1 && !validateForm(currentTab)) return false;
		// Ocultar la pestaña actual:
		x[currentTab].style.display = "none";
		// Aumentar o disminuir la pestaña actual en 1:
		currentTab = currentTab + n;
		// si ha llegado al final del formulario ...:
		if (currentTab >= x.length) {
			//... el formulario se envía:
			enviar();
			//document.getElementById("regForm").submit();
			return false;
		}
		// De lo contrario, muestre la pestaña correcta:
		showTab(currentTab);
	}

	function validateForm(step) {
		// Esta función se ocupa de la validación de los campos del formulario.
		var x, y, i, valid = true;
		x = document.getElementsByClassName("tab");
		y = x[currentTab].getElementsByClassName("validar");
		//y = x[currentTab].getElementsByTagName("input");
		// Un ciclo que verifica cada campo de entrada en la pestaña actual:
		for (i = 0; i < y.length; i++) {
			// Si un campo está vacío ...
			if (y[i].value == "") {
				// agregue una clase "inválida" al campo:
				y[i].className += " invalid";
				// y establezca el estado válido actual en falso:
				valid = false;
				if (step == 3 || step.length == 4) {
					Swal.fire({
						icon: 'warning',
						title: 'Faltan imágenes por adjuntar',
						text: 'Es necesario que adjunte todas las imágenes solicitadas para poder continuar con el reserva de matrícula'
					});
				}
			}
		}
		// Si el estado válido es verdadero, marque el paso como terminado y válido:
		if (valid) {
			document.getElementsByClassName("step")[currentTab].className += " finish";
		}
		return valid; // devolver el estado válido
	}

	function fixStepIndicator(n) {
		// Esta función elimina la clase "activa" de todos los pasos ...
		var i, x = document.getElementsByClassName("step");
		for (i = 0; i < x.length; i++) {
			x[i].className = x[i].className.replace(" active", "");
		}
		//... y agrega la clase "activa" al paso actual:
		x[n].className += " active";
	}
</script>

<script>
	jQuery(document).ready(function() {
		/*Swal.fire({
			icon: 'info',
			title: 'Reserva de matrícula',
			text: 'El trámite estará completo cuando obtenga el número de reserva'
		});
*/
		$("#posee_adulto2").on('change', function() {
			if ($(this).is(':checked')) {
				$("#nombre_tutor2,#apellido_tutor2,#domicilio_tutor2,#documento_tutor2,#tel_particular_tutor2,#tel_laboral_tutor2,#relacion_tutor2,#ocupacion_tutor2,#empresa_tutor2,#cargo_tutor2,#correo_tutor2").prop("disabled", false);
				$("#nombre_tutor2,#apellido_tutor2,#domicilio_tutor2,#documento_tutor2,#tel_particular_tutor2,#tel_laboral_tutor2,#relacion_tutor2,#ocupacion_tutor2,#empresa_tutor2,#cargo_tutor2,#correo_tutor2").addClass("validar");
			} else {
				$("#nombre_tutor2,#apellido_tutor2,#domicilio_tutor2,#documento_tutor2,#tel_particular_tutor2,#tel_laboral_tutor2,#relacion_tutor2,#ocupacion_tutor2,#empresa_tutor2,#cargo_tutor2,#correo_tutor2").prop("disabled", true);
				$("#nombre_tutor2,#apellido_tutor2,#domicilio_tutor2,#documento_tutor2,#tel_particular_tutor2,#tel_laboral_tutor2,#relacion_tutor2,#ocupacion_tutor2,#empresa_tutor2,#cargo_tutor2,#correo_tutor2").removeClass("validar");
			}
		});
		jQuery('#filer_input').on('change', function(event) {
			prepareUpload(event, 'A', 'aul', 'listarchivos');
		});
		jQuery('#filer_input1').on('change', function(event) {
			prepareUpload(event, 'A', 'aul', 'listarchivos1');
		});
		jQuery('#filer_input2').on('change', function(event) {
			prepareUpload(event, 'A', 'aul', 'listarchivos2');
		});
		jQuery('#filer_input3').on('change', function(event) {
			prepareUpload(event, 'A', 'aul', 'listarchivos3');
		});
		jQuery('#filer_input4').on('change', function(event) {
			prepareUpload(event, 'A', 'aul', 'listarchivos4');
		});
		jQuery('#filer_input5').on('change', function(event) {
			prepareUpload(event, 'A', 'aul', 'listarchivos5');
		});
	});
</script>

<script>
	jQuery('#provincia_alumno').flexdatalist({
		valueProperty: 'id',
		minLength: 3,
		selectionRequired: true,
		visibleProperties: ["provincia"],
		searchContain: true,
		searchDelay: 200,
		searchIn: 'provincia',
		noResultsText: 'No se encontraron coincidencias para "{keyword}"',
		data: '<?php echo site_url(); ?>provincia/autocompletar_provincia/'
	});
	jQuery('#nacionalidad_alumno').flexdatalist({
		valueProperty: 'id',
		minLength: 3,
		selectionRequired: true,
		visibleProperties: ["nombre"],
		searchContain: true,
		searchDelay: 200,
		searchIn: 'nombre',
		noResultsText: 'No se encontraron coincidencias para "{keyword}"',
		data: '<?php echo site_url(); ?>nacionalidad/autocompletar_nacionalidad/'

	});
</script>

<script>
	function enviar() {
		jQuery("#reservando").attr("hidden", false);
		jQuery("#nextBtn").hide();
		var matriz_archivos = JSON.stringify(id_archivos_alumno());

		jQuery.ajax({
			method: "POST",
			url: '<?php echo site_url('datos/guardar_priamria/'); ?>' + matriz_archivos,
			dataType: "JSON",
			data: jQuery("#regForm").serialize(),
		}).done(function(data) {
			if (data['status'] == true) {
				//console.log(data);
				Swal.fire({
					icon: 'success',
					title: 'La reserva de matrícula se realizó con exito ',
					text: 'El número de reserva es: Nº0000' + data['id'],
					confirmButtonText: `Finalizar`,
				}).then((result) => {
					window.location.replace('<?php echo site_url("donboscoinicial") ?>');
				});
			} else {
				Swal.fire({
					icon: 'error',
					title: 'La reserva de matrícula no pudo ser realizada ',
					text: 'Inténtelo de nuevo mas tarde',
					confirmButtonText: `OK`,
				}).then((result) => {
					window.location.replace('<?php echo site_url("donboscoinicial") ?>');
				});
			}
		});
	}
</script>