<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="<?= base_url() ?>/public/demo/img/favicon.ico?v=2" type="image/x-icon">
  <link rel="icon" href="<?= base_url() ?>/public/demo/img/favicon.ico?v=2" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css2?family=Alata&family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="<?= base_url() ?>/public/demo/css/style.css">
  <link rel="stylesheet" href="<?= base_url() ?>/public/demo/css/media-queries.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS   -->
  <script src="<?= base_url() ?>/public/demo/js/jquery-3.5.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
  <title>Pre-inscripción Online</title>
</head>

<body>

  <div class="container">
 
    <br>

    <?php if (session('msg')) : ?>
      <div class="alert alert-info alert-dismissible">
        <?= session('msg') ?>
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
      </div>
    <?php endif ?>


    <div class="row">
      <div class="col-md-9">
        <form action="" name="ajax_form" id="ajax_form" onsubmit="validar_subida_archivos()" method="post" accept-charset="utf-8" enctype="multipart/form-data">


          <div id="dni_alumno" class="form-group"><label>Adjuntar Recursos</label>
            <input type="file" name="files[]" id="filer_input" multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Seleccione o arrastre hasta 5 archivos" />
            <ul id="listarchivos" style="padding-top:4px"></ul>
          </div>

      </div>

      <div class="form-group">
        <button type="submit" id="send_form" class="btn btn-success">Submit</button>
      </div>

      </form>
    </div>

  </div>

  </div>

    <script>
      $(document).ready(function() {
        $('#filer_input').on('change', function(event) {
          prepareUpload(event, 'A', 'aul', 'listarchivos');
        });
        draganddropUpload_generic('A', 'aul', 'content_seccion', 'listarchivos');
        var nombre = 'documento_alumno_archivo'

        
          jQuery.ajax({
				method: "POST",
				url: '<?php echo site_url("Form/buscar_nombre_div_archivos/") ?>',
				dataType: "JSON",
				data: {
					nombre: nombre
				}
			}).done(function(data) {
				console.log(data);
			});
        
      });
    </script>