<style type="text/css">
  .dragandrophandler {
    border: 3px dashed #0B85A1;
    /*width:400px;*/
    height: auto;
    color: #92AAB0;
    text-align: left;
    vertical-align: middle;
    padding: 10px 10px 10 10px;
    margin-bottom: 10px;
    font-size: 100%;
  }

  .namefile {
    display: inline-block;
    overflow: hidden;
    padding: 3px 0;
    text-overflow: ellipsis;
    vertical-align: bottom;
    white-space: nowrap;
    max-width: 315px;
    color: #15c;
  }

  .itemfile {
    background-color: #f5f5f5;
    border: 1px solid #212529;
    font-weight: bold;
    margin: 0 7px 9px;
    overflow-y: hidden;
    padding: 4px 4px 4px 8px;
    max-width: 448px;
    font-size: 13px;
  }

  .sizefile {
    color: #777;
    display: inline-block;
    padding: 3px 0;
  }
</style>

<script>
  urlApiArchivos = 'https://archivos-nodos.com.ar/api_archivos/index.php';
  passApiKey = '8kg0kw8swcgkkogw8woksokg4k4cg8ssskg4ogso';
  var edita_files = 0;

  function subirarchivo(data, name, alta = 'A', origen = 'pre', listName = 'listarchivos') {

    edita_files = 1;
    $.each(data, function(key, value) {
      //console.log(value);
    })
    //console.log(data[0].values());
    //console.log(data);
    var listNameD = listName + 'D';

    var urlRequest = urlApiArchivos + "/upload/upload_file";
    var nameaux = name.replace('.jpeg', '.jpg').replace('.png', '.jpg').replace('.gif', '.jpg');
    var id_seccion = "0";
    if (origen == 'aul') {
      id_seccion = $('#content_id_seccion').val();
    } else {
      if (origen == 'aau') {
        id_seccion = $('#comentario_id').val();
      }
    }

    //ajax nuevo
    $.ajax({
        url: urlRequest,
        type: 'POST',
        headers: {
          'APIKEY': passApiKey,
          //'userid' : idUser,
          'origen': 'pre',
          'Access-Control-Allow-Origin': '*'
        },
        data: data,
        cache: false,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        beforeSend: function(xhr) {
          //aqui iria un loading
          var li = $('#' + listName + ' :contains("' + nameaux + '")').parent('div').parent('li');
          $(li).find('.sizefile').after("<img id='loadingUP' src='http://inscripciones.e-nodos.com/public/demo/img/loading.gif' height='15' width='15' />");
        }
      })
      .done(function(data) {

        if (console && console.log) {
         // console.log("Sample of data:", data.slice(0, 100));
        }
        //done codigo del otro ajax
        //console.log(data);
        var data = jQuery.parseJSON(data);
        if (data['error']) {

          $.notify(data['error'], "danger"  ,{ position:"right middle" });


          if (alta == 'D')
            var li = $('#' + listNameD + ' :contains("' + data['nombre'] + '")').parent('div').parent('li');
          else
            var li = $('#' + listName + ' :contains("' + data['nombre'] + '")').parent('div').parent('li');


          $(li).remove();

        } else {

          data = data['files'];


          for (var i = 0; i < data.length; i++) //aca coloco id de archivo al li. y agrego tilde suces
          {
            //console.log(data[i]['nombre']);
            if (alta == 'D')
              var li = $('#' + listNameD + ' :contains("' + data[i]['nombre'] + '")').parent('div').parent('li');
            else
              var li = $('#' + listName + ' :contains("' + data[i]['nombre'] + '")').parent('div').parent('li');


            // console.log(li);
            $(li).attr('id', data[i]['id']);
            // $('#dni_alumno').find("li").attr('id');
            // console.log($('#dni_alumno').find("li").attr('id'));
            var tilde = '<img src="http://inscripciones.e-nodos.com/public/demo/img/tildeve2.png" alt="Se adjunto correctamente" height="20" width="20">';

            $(li).find('#loadingUP').remove();

            $(li).find('.sizefile').append(tilde);

            if ($(li).find('.sizefile')[0].innerHTML.indexOf('tildeve2.png') < 0) {
              $(li).find('.sizefile').append(tilde);
            }

            delete_files(data[i]['id']);
          }
        }
        //
      });


    //

  }

  var cantArchivos = 5;
  var sizeArchivo = 20; //MB
  function buscar_nombre_div_archivos(nombre) {
    jQuery.ajax({
				method: "POST",
				url: '<?php echo site_url("Datos/buscar_nombre_div_archivos/") ?>',
				dataType: "JSON",
				data: {
					nombre: nombre
				}
			}).done(function(data) {
				if (data['status'] == true) {
         
        return data['id'];
      } else {
        return data['status'];
      }
			});

  }
 
  function id_archivos() {
    var matriz_envio = [];
    let documento_alumno_archivo=[], documento_tutor1_archivo = [],documento_tutor2_archivo = [],foto_alumno_archivo=[];
    let libre_deuda_alumno_archivo=[], boleta_servicio_alumno_archivo = [],libreta_alumno_archivo = [],certificado_conducta_alumno_archivo=[];
    let lis = document.getElementById('documento_alumno_archivo').getElementsByTagName('li');
    documento_alumno_archivo.push('1');
    for (var i = 0; i < lis.length; i++) {
      documento_alumno_archivo.push(lis[i].id);
    }
    matriz_envio.push(documento_alumno_archivo);
    let lis1 = document.getElementById('documento_tutor1_archivo').getElementsByTagName('li');
    documento_tutor1_archivo.push('2');
    for (var i = 0; i < lis1.length; i++) {
      documento_tutor1_archivo.push(lis1[i].id);
    }
    matriz_envio.push(documento_tutor1_archivo);
    let lis2 = document.getElementById('documento_tutor2_archivo').getElementsByTagName('li');
    documento_tutor2_archivo.push('3');
    for (var i = 0; i < lis2.length; i++) {
      documento_tutor2_archivo.push(lis2[i].id);
    }
    matriz_envio.push(documento_tutor2_archivo);
    let lis3 = document.getElementById('foto_alumno_archivo').getElementsByTagName('li');
    foto_alumno_archivo.push('4');
    for (var i = 0; i < lis3.length; i++) {
      foto_alumno_archivo.push(lis3[i].id);
    }
    matriz_envio.push(foto_alumno_archivo);
    let lis4 = document.getElementById('libre_deuda_alumno_archivo').getElementsByTagName('li');
    libre_deuda_alumno_archivo.push('5');
    for (var i = 0; i < lis4.length; i++) {
      libre_deuda_alumno_archivo.push(lis4[i].id);
    }
    matriz_envio.push(libre_deuda_alumno_archivo);
    let lis5 = document.getElementById('boleta_servicio_alumno_archivo').getElementsByTagName('li');
    boleta_servicio_alumno_archivo.push('6');
    for (var i = 0; i < lis5.length; i++) {
      boleta_servicio_alumno_archivo.push(lis5[i].id);
    }
    matriz_envio.push(boleta_servicio_alumno_archivo);
    let lis6 = document.getElementById('libreta_alumno_archivo').getElementsByTagName('li');
    libreta_alumno_archivo.push('7');
    for (var i = 0; i < lis6.length; i++) {
      libreta_alumno_archivo.push(lis6[i].id);
    }
    matriz_envio.push(libreta_alumno_archivo);
    let lis7 = document.getElementById('certificado_conducta_alumno_archivo').getElementsByTagName('li');
    certificado_conducta_alumno_archivo.push('8');
    for (var i = 0; i < lis7.length; i++) {
      certificado_conducta_alumno_archivo.push(lis7[i].id);
    }
    matriz_envio.push(certificado_conducta_alumno_archivo);

    return matriz_envio;
  }
  
  function id_archivos_alumno() {
    var matriz_envio = [];
    let documento_alumno_archivo=[], documento_tutor1_archivo = [],documento_tutor2_archivo = [],foto_alumno_archivo=[];
    let libre_deuda_alumno_archivo=[], boleta_servicio_alumno_archivo = [];
    let lis = document.getElementById('documento_alumno_archivo').getElementsByTagName('li');
    documento_alumno_archivo.push('1');
    for (var i = 0; i < lis.length; i++) {
      documento_alumno_archivo.push(lis[i].id);
    }
    matriz_envio.push(documento_alumno_archivo);
    let lis1 = document.getElementById('documento_tutor1_archivo').getElementsByTagName('li');
    documento_tutor1_archivo.push('2');
    for (var i = 0; i < lis1.length; i++) {
      documento_tutor1_archivo.push(lis1[i].id);
    }
    matriz_envio.push(documento_tutor1_archivo);
    let lis2 = document.getElementById('documento_tutor2_archivo').getElementsByTagName('li');
    documento_tutor2_archivo.push('3');
    for (var i = 0; i < lis2.length; i++) {
      documento_tutor2_archivo.push(lis2[i].id);
    }
    matriz_envio.push(documento_tutor2_archivo);
    let lis3 = document.getElementById('foto_alumno_archivo').getElementsByTagName('li');
    foto_alumno_archivo.push('4');
    for (var i = 0; i < lis3.length; i++) {
      foto_alumno_archivo.push(lis3[i].id);
    }
    matriz_envio.push(foto_alumno_archivo);
    let lis4 = document.getElementById('libre_deuda_alumno_archivo').getElementsByTagName('li');
    libre_deuda_alumno_archivo.push('5');
    for (var i = 0; i < lis4.length; i++) {
      libre_deuda_alumno_archivo.push(lis4[i].id);
    }
    matriz_envio.push(libre_deuda_alumno_archivo);
    let lis5 = document.getElementById('boleta_servicio_alumno_archivo').getElementsByTagName('li');
    boleta_servicio_alumno_archivo.push('6');
    for (var i = 0; i < lis5.length; i++) {
      boleta_servicio_alumno_archivo.push(lis5[i].id);
    }
    matriz_envio.push(boleta_servicio_alumno_archivo);
    
    return matriz_envio;
  }
  function delete_files(id, idevento = 0) {

    //if(alta='A')
    $('#' + id + ' .close').click(function(e) {
      e.stopPropagation();
      e.preventDefault();

      //console.log('delete');
      edita_files = 1;

      var li = $(this).parent('div').parent('li');
      var idfile = $(li).attr('id');

      $(li).remove();
      var urlRequest = urlApiArchivos + "/upload/delete_file";


      if (!$(li).hasClass("old")) {

        /*$.ajax({
                url: urlRequest,
                type: 'POST',
                headers: {
                'APIKEY' : passApiKey,
                //'userid' : idUser
              },
              data:{idfile:idfile, idevento:idevento},
              success: function(data){
                if(data['status']==false)
                {
                  //console.log(data['message']);
                  /*if(data['message']=='NO LOGUIN')
                    location.href = "<?php //echo base_url('login'); 
                                      ?>"; //no està logueado
                  //terminadoP4(1); terminadoP4(2); terminadoP4(3);
                }
                else
                $.notify("Archivo Eliminado", "success");


            },
            error: function(response){
                console.log(response);
            }
        });*/
        //ajax nuevo
        $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {
              'APIKEY': passApiKey,
              //'userid' : idUser,
              'origen': 'pre',
              'Access-Control-Allow-Origin': '*'
            },
            data: {
              idfile: idfile,
              idevento: idevento
            },
          })
          .done(function(data) {

            if (data['status'] == false) {
              //console.log(data['message']);
              //if(data['message']=='NO LOGUIN')
              //location.href = "<?php //echo base_url('login'); 
                                  ?>"; //no està logueado
              //terminadoP4(1); terminadoP4(2); terminadoP4(3);
            } else
              $.notify("Archivo Eliminado", "success", { position:"right middle" });

          });


        //fin ajax nuevo
      }
    });
    //else

  }


  //boton examinar
  function prepareUpload(event, alta = 'A', origen = 'eve', listName = 'listarchivos') {
    event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening



    var listNameD = listName + 'D';

    var ecxedesize = 0;

    files = event.target.files; //esta lina es la unica diferencia con el prepareUpload2

    $.each(files, function(key, value) {
      var data = new FormData();
      data.append(key, value);
      //data.append(value);
      //console.log(value)
      //console.log(key)
      //console.log(data);
      var size = value.size;
      if (size > 1024) {
        var temp = size / 1024;
        //temp = number_format(temp, 2, '.', '');
        if (temp > 1024) {
          size = (temp / 1024).toFixed(2) + " MB";
          if ((temp / 1024).toFixed(2) > sizeArchivo)
            ecxedesize = 1;
        } else
          size = temp.toFixed(2) + " KB";
      } else
        size = size.toFixed(2) + " Bytes";

      if (alta == 'D')
        var cantActual = $("#" + listNameD + " li").length;
      else
        var cantActual = $("#" + listName + " li").length;


      var name = value.name;
      name = name.replace(new RegExp(' ', 'g'), "_");
      var nameaux = name.replace('.jpeg', '.jpg').replace('.png', '.jpg').replace('.gif', '.jpg');

      if (cantActual < cantArchivos) {
        if (ecxedesize == 0) {
          //alert(value.name); alert(alta);
          if (alta == 'D')
            var li = $('#' + listNameD + ' :contains("' + nameaux + '")').parent('div').parent('li');
          else
            var li = $('#' + listName + ' :contains("' + nameaux + '")').parent('div').parent('li');

          if (alta == 'R') //R reenviar
            var clase = 'new';
          else var clase = '';


          if (li.length == 0) {
            var li = '<li class="' + clase + '"> <div class="itemfile"><div class="namefile">' + nameaux + '</div> <div class="sizefile"> &nbsp;&nbsp;&nbsp;  (' + size + ') </div>' +
              '<button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>' +
              '</div></li>';

            if (alta == 'D')
              $('#' + listNameD).append(li);
            else
              $('#' + listName).append(li);


            //delete_files(alta);

            subirarchivo(data, name, alta, origen, listName);
          } else
            $.notify("El archivo ya fue añadido", "warn", { position:"right middle" });
        } else
          $.notify("No puede superar los " + sizeArchivo + " MB", "warn", { position:"right middle" });
      } else
        $.notify("Alcanzó el límite de arhivos", "warn", { position:"right middle" });

    });

  }



  function listar_archivos(idevento, solomostrar = 0, alta = 'A', origen = 'eve') //2do parametro indica que puedo o no borrar un archivo. el tercero para diferenciar si viene por un formulario de alta o de una modificacion
  {
    var url = '<?= base_url("/index.php/eventos/get_eventofiles_ajax") ?>';

    if (origen == 'aul' || origen == 'aau') {
      url = '<?= base_url("/index.php/eventos/get_seccionfiles_ajax") ?>';
      ignoreCache = 1;
    }

    var texto = '';

    $.ajax({
      url: url,
      type: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      data: {
        idevento: idevento,
        ignoreCache: ignoreCache,
        origen: origen
      },
      //async: false,
      success: function(data) {
        //console.log('colegio: '+idcolegio+' nombre_grupo: '+nombregrupo);

        var data = jQuery.parseJSON(data);
        //console.log(data);
        if (data['status'] == false) {
          //console.log(data['message']);
          if (data['message'] == 'NO LOGUIN')
            location.href = "<?php echo base_url('login'); ?>"; //no està logueado
          //terminadoP4(1); terminadoP4(2); terminadoP4(3);
        } else {
          //var data = jQuery.parseJSON( data );
          //console.log(data);

          for (var i = 0; i < data['files'].length; i++) //aca solo listo niveles y especialidades
          {
            var nombre = data['files'][i]['nombre'];
            var size = data['files'][i]['size'];
            var idfile = data['files'][i]['id'];


            var url = data['files'][i]['url'];


            //texto = texto + "<li>  <a  class='glyphicon glyphicon-circle-arrow-down' href='"+url+"' download> "+ nombre +" </a> </li>"  ;

            if (alta == 'R') //R reenviar
              var clase = 'old';
            else var clase = '';

            if (solomostrar == 0) {
              var li = '<li id="' + idfile + '" class="' + clase + '"> <div class="itemfile"><div class="namefile"><a  target="_blank" class="glyphicon glyphicon-circle-arrow-down" href="' + url + '" download> ' + nombre + ' </a></div> <div class="sizefile"> &nbsp;&nbsp;&nbsp;  (' + size + ') </div>' +
                '<button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>' +
                '</div></li>';
            } else {
              var li = '<li id="' + idfile + '" class="' + clase + '"> <div class="itemfile"><div class="namefile"><a target="_blank"  class="glyphicon glyphicon-circle-arrow-down" href="' + url + '" download> ' + nombre + ' </a></div> <div class="sizefile"> &nbsp;&nbsp;&nbsp;  (' + size + ') </div>' +
                '</div></li>';
            }


            if (alta == 'D')
              $('#listarchivosD').append(li);
            else
              $('#listarchivos').append(li);

            delete_files(idfile, idevento);
          }


        }
      },
      error: function(response) {
       // console.log(response);
      }
    });

    return texto;
  }


  function draganddropUpload(alta = 'A', origen) //alta o edit
  {
    if (alta == 'A') {
      //var obj = $("#dragupload");
      var obj = $(".modal-content").find('[id="dragupload"]');
    } else {
      //var obj = $("#draguploadD");
      //onsole.log(obj);
      var obj = $(".modal-content").find('[id="draguploadD"]');
      //console.log(obj);
    }

    obj.on('dragenter', function(e) {
      e.stopPropagation();
      e.preventDefault();
      //$(this).add('border', '4px solid #0B85A1');
      if (siguiente == 0)
        $(this).addClass('dragandrophandler');
    });
    obj.on('dragover', function(e) {
      e.stopPropagation();
      e.preventDefault();
    });
    obj.on('drop', function(e) {

      e.stopPropagation();
      e.preventDefault();
      if (siguiente == 0) {
        $(this).removeClass('dragandrophandler');
        var files = e.originalEvent.dataTransfer.files;
        //We need to send dropped files to Server
        prepareUpload2(files, alta, origen);
      }

    });



    $(document).on('dragenter', function(e) {
      e.stopPropagation();
      e.preventDefault();
    });
    $(document).on('dragover', function(e) {
      e.stopPropagation();
      e.preventDefault();

      if (alta == 'A') {
        //$("#dragupload").removeClass('dragandrophandler');
        var aux = $(".modal-content").find('[id="dragupload"]');
        $(aux).removeClass('dragandrophandler');
      } else {
        var aux = $(".modal-content").find('[id="draguploadD"]');
        $(aux).removeClass('dragandrophandler');
      }

    });
    $(document).on('drop', function(e) {
      e.stopPropagation();
      e.preventDefault();
    });

  }

  function draganddropUpload_generic(alta = 'A', origen, objId, listName = 'listarchivos') //alta o edit
  {
    if (alta == 'A') {
      //var obj = $("#dragupload");
      var obj = $("#" + objId);
    } else {
      //var obj = $("#draguploadD");
      //onsole.log(obj);
      var obj = $("#" + objId);
      //console.log(obj);
    }

    obj.on('dragenter', function(e) {
      e.stopPropagation();
      e.preventDefault();
      //$(this).add('border', '4px solid #0B85A1');
      if (siguiente == 0)
        $(this).addClass('dragandrophandler');
    });
    obj.on('dragover', function(e) {
      e.stopPropagation();
      e.preventDefault();
    });
    obj.on('drop', function(e) {

      e.stopPropagation();
      e.preventDefault();
      if (siguiente == 0) {
        $(this).removeClass('dragandrophandler');
        var files = e.originalEvent.dataTransfer.files;
        //We need to send dropped files to Server
        prepareUpload2(files, alta, origen, listName);
      }

    });



    $(document).on('dragenter', function(e) {
      e.stopPropagation();
      e.preventDefault();
    });
    $(document).on('dragover', function(e) {
      e.stopPropagation();
      e.preventDefault();

      if (alta == 'A') {
        //$("#dragupload").removeClass('dragandrophandler');
        var aux = $("#" + objId);
        $(aux).removeClass('dragandrophandler');
      } else {
        var aux = $("#" + objId);
        $(aux).removeClass('dragandrophandler');
      }

    });
    $(document).on('drop', function(e) {
      e.stopPropagation();
      e.preventDefault();
    });

  }
</script>