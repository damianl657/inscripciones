<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <!--
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Inicio</a>
                </li>
-->
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">

                <!-- Notifications Dropdown Menu -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        < <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="" class="brand-link">
                <img style="width:50px; height:50px;" src="<?= base_url() ?>/public/demo/fotos_colegios/logo_nodos.png">
                <span class="brand-text font-weight-light">Legajo Digital</span>
            </a>


            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">

                </div>

                <!-- SidebarSearch Form -->
                <div class="form-inline">
                    <div class="input-group" data-widget="sidebar-search">
                        <input class="form-control form-control-sidebar" type="search" placeholder="Buscar" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-sidebar">
                                <i class="fas fa-search fa-fw"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-header">Instructivos</li>
                        <li class="nav-item">
                            <a href="http://inscripciones.e-nodos.com/public/instructivos/2da_etapa_inscripciones.pdf" download class="nav-link">
                                <i class="nav-icon fas fa-file"></i>
                                <p>Carga de información </p>
                            </a>
                            </li>
                    <?php
                    /*
                    if( count($colegios) > 0){

                        foreach ($colegios as $cole) {
                            $colegioName = $cole['tipo'];
                            $colegioId   = $cole['id'];
                            $niveles     = $cole['niveles'];
                            */
                    ?>

                <li class="nav-header">Alumnos a cargo</li>

                            <?php
                                if( count($hijos_tutor) > 0){

                                foreach ($hijos_tutor as $hijos) {
                                    $nombre_hijo = $hijos['nombre'];
                                    $reserva_id   = $hijos['id_reserva'];
                            ?>
                                    <li class="nav-item">
                                        <a href="<?php echo site_url("alumnos/ver_datos_tutor_alumno/$reserva_id") ?>" class="nav-link">
                                            <i class="nav-icon fas fa-child"></i>
                                            <p>
                                                <?php echo ucwords(strtolower( $nombre_hijo));?>
                                            </p>
                                         </a>
                                    </li>
                    <?php  
                           
                        } //fin for colegios
                    } // fin if colegios
                    ?>
 
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>