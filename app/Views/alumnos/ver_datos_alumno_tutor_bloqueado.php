<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Gallery</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Gallery</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="callout callout-danger ">
          <h1 ><i class="fa fa-exclamation-triangle" style="color:#ec4747"></i> Inscripción pausada </h1>
          <p class="card-text">La solicitud de inscripcion no puede continuar para este alumno, debe comunicarse con la estableciomiento educativo para resolver el problema.</p>
        </div>
      </div>
    </div>
  </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5">
          <!-- Profile Image -->
          <div class="card card-success card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="<?= base_url() ?>/public/demo/img/usuario_azul.png" alt="">
              </div>
              <h3 class="profile-username text-center">
                <div class="row">
                  <div class="col-md-6 text-right" id="nombre_alumno"><?php echo $alumno[0]["nombre"]; ?></div>
                  <div class="col-md-6 text-left" id="apellido_alumno"><?php echo $alumno[0]["apellido"]; ?></div>
                </div>
              </h3>

              <p class="text-muted text-center">
                <div class="row">
                  <div id="anio_reserva" class="col-md-6 text-right">Reserva a <?php echo $anio_nivel_reserva[0]["anio_reserva"]; ?> </div>
                  <div id="nombre_nivel_reserva" class="col-md-4 text-left"><?php echo $anio_nivel_reserva[0]["nombre_nivel"]; ?></div>
                 
                </div>
              </p>
              <ul class="list-group list-group-unbordered mb-4">
                <li class="list-group-item">
                  <b>Curso actual</b> <a class="float-right">
                    <div id="anio_division_actual_alumno"><?php echo $alumno[0]["anio_actual"]; ?> <?php echo $alumno[0]["division_actual"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Legajo</b> <a class="float-right">
                    <div id="legajo_alumno"><?php echo $alumno[0]["legajo"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Documento</b> <a class="float-right">
                    <div id="documento_alumno"><?php echo $alumno[0]["documento"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Domicilio</b> <a class="float-right">
                    <div id="domicilio_alumno"><?php echo $alumno[0]["domicilio"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Fecha de nacimiento</b> <a class="float-right">
                    <div id="fecha_nac_alumno"><?php echo $alumno[0]["fecha_nac"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Correo</b> <a class="float-right">
                    <div id="correo_alumno"><?php echo $alumno[0]["correo"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Provincia</b> <a class="float-right">
                    <div id="provincia_alumno"><?php echo $alumno[0]["provincia"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Departamento</b> <a class="float-right">
                    <div id="ciudad_alumno"><?php echo $alumno[0]["ciudad"]; ?></div>
                  </a>
                </li>
              </ul>
          
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-7">
          <div class="card card-primary card-outline">
            <div class="card-header p-2">
              <ul class="nav nav-tabs">
                <?php
                if (count($tutores) > 0) {
                  for ($i = 0; $i < count($tutores); $i++) {
                    $tutorId = $tutores[$i]['id'];

                    if ($i == 0)
                      $active = "active";
                    else
                      $active = "";
                ?>

                    <li class="nav-item"><a class="nav-link <?= $active ?>" data-toggle="tab" href="#tutor_<?= $tutorId ?>">Adulto Responsable <?= $i + 1 ?></a></li>

                <?php
                  } //fin for tutores
                } //fin if tutores
                ?>
              </ul>
            </div><!-- /.card-header -->

            <div class="card-body">
              <div class="tab-content">

                <?php
                if (count($tutores) > 0) {
                  for ($i = 0; $i < count($tutores); $i++) {
                    $tutorId = $tutores[$i]['id'];
                    if ($i == 0)
                      $active = "active";
                    else
                      $active = "";

                    $datosJson = json_encode($tutores[$i]);

                    if ($tutores[$i]["vive"] == 1)
                      $tutores[$i]["vive"] = "Si";
                    else
                      $tutores[$i]["vive"] = "No";

                    if ($tutores[$i]["rol"] == 1)
                      $tutores[$i]["rol"] = "Padre";
                    else
                          if ($tutores[$i]["rol"] == 2)
                      $tutores[$i]["rol"] = "Madre";
                    else
                      $tutores[$i]["rol"] = "Tutor";
                ?>

                    <div class="tab-pane <?= $active ?>" id="tutor_<?= $tutorId ?>">

                      <input type="hidden" class="datosTutor" value='<?= $datosJson ?>'>

                      <!-- Post -->
                      <div class="post">
                        <div class="user-block">
                          <img class="img-circle img-bordered-sm" src="<?= base_url() ?>/public/demo/img/usuario_azul.png">
                          <span class="username">
                            <?php echo $tutores[$i]["nombre"]; ?> <?php echo $tutores[$i]["apellido"]; ?>
                          </span>
                          <span class="description"><?php echo $tutores[$i]["rol"]; ?></span>
                        </div>
                        <!-- /.user-block -->
                        <p>
                          <div class="col-md-12 ">
                            <div class="table-responsive">
                              <table class="table m-0">
                                <tbody>
                                  <tr>
                                    <th scope="row">Vive</th>
                                    <td class="vive"><?php echo $tutores[$i]["vive"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Documento</th>
                                    <td class="documento"><?php echo $tutores[$i]["documento"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Teléfonos Particular</th>
                                    <td class="telefono1"><?php echo $tutores[$i]["telefono_particular"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Teléfonos Laboral</th>
                                    <td class="telefono2"><?php echo $tutores[$i]["telefono_laboral"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Domicilio</th>
                                    <td class="domicilio"><?php echo $tutores[$i]["domicilio"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Ocupación/Empresa/Cargo</th>
                                    <td class="ocupacion"><?php echo $tutores[$i]["ocupacion"]; ?> / <?php echo $tutores[$i]["empresa"]; ?> / <?php echo $tutores[$i]["cargo"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Correo</th>
                                    <td class="correo"><?php echo $tutores[$i]["correo"]; ?></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </p>
                        <!--<p>
                            <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal_editar_tutor"><b>Editar</b></a>
                          </p>-->
  
                      </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="timeline">
                      <!-- The timeline -->
                    </div>
                    <!-- /.tab-pane -->

                <?php
                  } //fin for tutores
                } //fin if tutores
                ?>

              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->

          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
 
  
<!-- /.content -->
</div>
<!-- /.content-wrapper -->



<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url() ?>/public/demo/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url() ?>/public/demo/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="<?= base_url() ?>/public/demo/plugins/filterizr/jquery.filterizr.min.js"></script>
-->
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>/public/demo/dist/js/demo.js"></script>
<!-- Sweet Alert 2 -->


<!-- Page specific script -->
<script>
  var id_preinscripcion = <?php echo $id_preinscripcion ?>;

</script>
<script>

  jQuery(document).ready(function() {
    //$('#modal_bloqueo').modal('show');
    var idpreinscripcion = $('#id_pre').val();

  

    $('#observacion').on('hidden.bs.modal', function() {
      $(":text").val("");
    });
    $('#observacion').on('show.bs.modal', function() {
      get_observacion($('#id_pre').val());
    });
    $('#modal_editar_alumno').on('hidden.bs.modal', function() {
     $(this).find("input,textarea,select").val('');
    });
    $('#modal_editar_alumno').on('show.bs.modal', function() {
      get_alumno();
    });

    $('#modal_editar_tutor').on('show.bs.modal', function(e) {
      //var id = $(this).data('id');
      var tutorId = $(e.relatedTarget).data('tutor-id');

      var datos = $('#tutor_' + tutorId).find('.datosTutor').val();
      datos = JSON.parse(datos);

      $(this).find('#tutor_id').val(datos['id']);
      $(this).find('#nombre_tutor_edit').val(datos['nombre']);
      $(this).find('#apellido_tutor_edit').val(datos['apellido']);
      $(this).find('#documento_tutor_edit').val(datos['documento']);
      $(this).find('#vive_tutor_edit').val(datos['vive']);
      $(this).find('#rol_tutor_edit').val(datos['rol']);
      $(this).find('#correo_tutor_edit').val(datos['correo']);
      $(this).find('#telefono2_tutor_edit').val(datos['telefono_laboral']);
      $(this).find('#telefono1_tutor_edit').val(datos['telefono_particular']);
      $(this).find('#domicilio_tutor_edit').val(datos['domicilio']);
      $(this).find('#ocupacion_tutor_edit').val(datos['ocupacion']);
      $(this).find('#empresa_tutor_edit').val(datos['empresa']);
      $(this).find('#cargo_tutor_edit').val(datos['cargo']);
    });

   

  });

  function get_alumno(idpre) {
    $('#nombre_alumno_editar').val($('#nombre_alumno').text());
    $('#documento_alumno_editar').val($('#documento_alumno').text());
    $('#apellido_alumno_editar').val($('#apellido_alumno').text());
    $('#legajo_alumno_editar').val($('#legajo_alumno').text());
    $('#domicilio_alumno_editar').val($('#domicilio_alumno').text());
    $('#fecha_nac_alumno_editar').val($('#fecha_nac_alumno').text());
    $('#correo_alumno_editar').val($('#correo_alumno').text());
    $('#ciudad_alumno_editar').val($('#ciudad_alumno').text());
  }

 

  
</script>

</body>

</html>