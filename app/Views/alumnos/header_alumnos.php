<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Área Alumnos</title>
  <link rel="shortcut icon" href="<?=base_url()?>/public/demo/img/favicon.ico?v=2" type="image/x-icon">
	<link rel="icon" href="<?=base_url()?>/public/demo/img/favicon.ico?v=2" type="image/x-icon">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>/public/demo/plugins/fontawesome-free/css/all.min.css">
  <!-- Ekko Lightbox -->
  <link rel="stylesheet" href="<?=base_url()?>/public/demo/plugins/ekko-lightbox/ekko-lightbox.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>/public/demo/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?=base_url()?>/public/demo/css/jquery.flexdatalist.min.css">
  <link rel="stylesheet" href="<?=base_url()?>/public/demo/css/generic.css">

</head>
<body>