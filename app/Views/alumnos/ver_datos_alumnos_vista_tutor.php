<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Gallery</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Gallery</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  
  <div class="card-body">
               
                <div class="alert alert-warning alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-exclamation-triangle"></i>Información solicitada por el Establecimiento a <strong>Usted</strong></h5>
                  <div id="observaciones_usuario"><?php echo $alumno[0]["observacion"]; ?></div>
                  <hr>
                  <h6>Para completar la información solicitada presione el botón: <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_observacion">Completar</button></h6>
                  <div id="observaciones_tutor"><?php echo $alumno[0]["observacion_tutor"]; ?></div>
                </div>
              
              </div>
  
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5">
          <!-- Profile Image -->
          <div class="card card-success card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="<?= base_url() ?>/public/demo/img/usuario_azul.png" alt="">
              </div>
              <h3 class="profile-username text-center">
                <div class="row">
                  <div class="col-md-6 text-right" id="nombre_alumno"><?php echo $alumno[0]["nombre"]; ?></div>
                  <div class="col-md-6 text-left" id="apellido_alumno"><?php echo $alumno[0]["apellido"]; ?></div>
                </div>
              </h3>

              <p class="text-muted text-center">
                <div class="row">
                  <div id="anio_reserva" class="col-md-6 text-right">Reserva a <?php echo $anio_nivel_reserva[0]["anio_reserva"]; ?> </div>
                  <div id="nombre_nivel_reserva" class="col-md-4 text-left"><?php echo $anio_nivel_reserva[0]["nombre_nivel"]; ?></div>
                 
                </div>
              </p>
              <ul class="list-group list-group-unbordered mb-4">
                <li class="list-group-item">
                  <b>Curso actual</b> <a class="float-right">
                    <div id="anio_division_actual_alumno"><?php echo $alumno[0]["anio_actual"]; ?> <?php echo $alumno[0]["division_actual"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Legajo</b> <a class="float-right">
                    <div id="legajo_alumno"><?php echo $alumno[0]["legajo"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Documento</b> <a class="float-right">
                    <div id="documento_alumno"><?php echo $alumno[0]["documento"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Domicilio</b> <a class="float-right">
                    <div id="domicilio_alumno"><?php echo $alumno[0]["domicilio"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Fecha de nacimiento</b> <a class="float-right">
                    <div id="fecha_nac_alumno"><?php echo $alumno[0]["fecha_nac"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Correo</b> <a class="float-right">
                    <div id="correo_alumno"><?php echo $alumno[0]["correo"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Provincia</b> <a class="float-right">
                    <div id="provincia_alumno"><?php echo $alumno[0]["provincia"]; ?></div>
                  </a>
                </li>
                <li class="list-group-item">
                  <b>Departamento</b> <a class="float-right">
                    <div id="ciudad_alumno"><?php echo $alumno[0]["ciudad"]; ?></div>
                  </a>
                </li>
              </ul>
          
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-7">
          <div class="card card-primary card-outline">
            <div class="card-header p-2">
              <ul class="nav nav-tabs">
                <?php
                if (count($tutores) > 0) {
                  for ($i = 0; $i < count($tutores); $i++) {
                    $tutorId = $tutores[$i]['id'];

                    if ($i == 0)
                      $active = "active";
                    else
                      $active = "";
                ?>

                    <li class="nav-item"><a class="nav-link <?= $active ?>" data-toggle="tab" href="#tutor_<?= $tutorId ?>">Adulto Responsable <?= $i + 1 ?></a></li>

                <?php
                  } //fin for tutores
                } //fin if tutores
                ?>
              </ul>
            </div><!-- /.card-header -->

            <div class="card-body">
              <div class="tab-content">

                <?php
                if (count($tutores) > 0) {
                  for ($i = 0; $i < count($tutores); $i++) {
                    $tutorId = $tutores[$i]['id'];
                    if ($i == 0)
                      $active = "active";
                    else
                      $active = "";

                    $datosJson = json_encode($tutores[$i]);

                    if ($tutores[$i]["vive"] == 1)
                      $tutores[$i]["vive"] = "Si";
                    else
                      $tutores[$i]["vive"] = "No";

                    if ($tutores[$i]["rol"] == 1)
                      $tutores[$i]["rol"] = "Padre";
                    else
                          if ($tutores[$i]["rol"] == 2)
                      $tutores[$i]["rol"] = "Madre";
                    else
                      $tutores[$i]["rol"] = "Tutor";
                ?>

                    <div class="tab-pane <?= $active ?>" id="tutor_<?= $tutorId ?>">

                      <input type="hidden" class="datosTutor" value='<?= $datosJson ?>'>

                      <!-- Post -->
                      <div class="post">
                        <div class="user-block">
                          <img class="img-circle img-bordered-sm" src="<?= base_url() ?>/public/demo/img/usuario_azul.png">
                          <span class="username">
                            <?php echo $tutores[$i]["nombre"]; ?> <?php echo $tutores[$i]["apellido"]; ?>
                          </span>
                          <span class="description"><?php echo $tutores[$i]["rol"]; ?></span>
                        </div>
                        <!-- /.user-block -->
                        <p>
                          <div class="col-md-12 ">
                            <div class="table-responsive">
                              <table class="table m-0">
                                <tbody>
                                  <tr>
                                    <th scope="row">Vive</th>
                                    <td class="vive"><?php echo $tutores[$i]["vive"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Documento</th>
                                    <td class="documento"><?php echo $tutores[$i]["documento"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Teléfonos Particular</th>
                                    <td class="telefono1"><?php echo $tutores[$i]["telefono_particular"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Teléfonos Laboral</th>
                                    <td class="telefono2"><?php echo $tutores[$i]["telefono_laboral"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Domicilio</th>
                                    <td class="domicilio"><?php echo $tutores[$i]["domicilio"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Ocupación/Empresa/Cargo</th>
                                    <td class="ocupacion"><?php echo $tutores[$i]["ocupacion"]; ?> / <?php echo $tutores[$i]["empresa"]; ?> / <?php echo $tutores[$i]["cargo"]; ?></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Correo</th>
                                    <td class="correo"><?php echo $tutores[$i]["correo"]; ?></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </p>
                        <!--<p>
                            <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal_editar_tutor"><b>Editar</b></a>
                          </p>-->
  
                      </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="timeline">
                      <!-- The timeline -->
                    </div>
                    <!-- /.tab-pane -->

                <?php
                  } //fin for tutores
                } //fin if tutores
                ?>

              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->

          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <div class="card-body">
              
                <div class="alert alert-info alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-info"></i> Agregar imagénes solicitadas</h5>
                 A continuación debe agregar las imagénes faltantes en cada espacio vacio y agregar las imagénes que se le solicitaron en el aréa <strong>"Información solicitada por el Establecimiento"</strong>.
                </div>
                
              </div>
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success">
            <div class="card-header">
              <h4 class="card-title">Información Complementaria</h4>
            </div>
            <div class="card-body">
              <?php
              for ($k = 0; $k < count($array_divs_nombres_f); $k++) {
                echo "<div class='card card-outline card-info'>";
                //echo $array_divs_nombres_f[$k][1];
                $divFile = explode('/', $array_divs_nombres_f[$k]);
                //print_r($array_divs_nombres_f[$k]);
                echo "<center><b>" . $divFile[1] . "</b></center>";

              ?>
                <div class="row" id="row_img_<?php echo $divFile[0]; ?>">
                  <?php

                  for ($j = 0; $j < count($archivos); $j++) {
                    //print_r($archivos[$j]);
                    $tipo = explode('/', $archivos[$j][2]);
                    if (($tipo[0] != 'application') and ($archivos[$j][1] == $divFile[0])) {
                      echo <<< EOT
                      <div class="col-sm-2" id="div_file_{$archivos[$j][3]}">
                        <a href="{$archivos[$j][0]}" data-toggle="lightbox" data-title="{$archivos[$j][1]}" data-gallery="gallery" data-footer= "<button data-dismiss='modal'  type='button' id='botonEliminar' name='eliminar' class='eliminar btn btn-danger btn-sm ml-auto' data-dismiss='modal'>Cerrar</button>" >
                          <img src="{$archivos[$j][0]}" class="img-fluid mb-2" alt="{$archivos[$j][1]}" />
                        </a>
                      </div>
                    EOT;
                    }
                    //print_r($archivos[$j][2]);

                  }
                  ?>
                </div>
                <br>
                <div class="row" id="row_application_<?php echo $divFile[0]; ?>">
                  <?php
                  for ($j = 0; $j < count($archivos); $j++) {
                    $tipo = explode('/', $archivos[$j][2]);
                    if ($tipo[0] == 'application' and ($archivos[$j][1] == $divFile[0])) {
                      echo <<< EOT
                    <div class="col-sm-5" id="div_file_{$archivos[$j][3]}">
                      <a href="{$archivos[$j][0]}" data-toggle="lightbox" data-title="{$archivos[$j][1]}" data-gallery="gallery" data-footer= "<button type='button' id='botonEliminar' name='eliminar' class='eliminar btn btn-danger btn-sm ml-auto'>Cerrar</button>" >
                        <embed src="{$archivos[$j][0]}" type="{$archivos[$j][2]}" width="400" height="400" />
                      </a>
                    </div>
                    EOT;
                    }

                    //print_r($archivos[$j][2]);

                  }
                  ?>
                </div><br>
                <div class="card-footer">
                  Agregar Archivos
                  <input type="file" name="files[]" id="<?php echo 'filer_' . $divFile[0] ?>" class="validar  " multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Agregar Archivo" />

                </div>
                <!--fin card card-outline card-info-->
            </div>
            <hr>
          <?php
              }
          ?>
          </div>
        </div>
      </div>

    </div>

</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal_eliminar_file">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Eliminar Archivos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>¿Desea Eliminar el Archivo?</p>
      </div>
      <input type="hidden" id="id_file_modal_eliminar" value="0">
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger" onclick="delete_file()">Eliminar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->

<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url() ?>/public/demo/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url() ?>/public/demo/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Ekko Lightbox -->
<script src="<?= base_url() ?>/public/demo/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>/public/demo/dist/js/adminlte.min.js"></script>
<!-- Filterizr
<script src="<?= base_url() ?>/public/demo/plugins/filterizr/jquery.filterizr.min.js"></script>
-->
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>/public/demo/dist/js/demo.js"></script>
<!-- Sweet Alert 2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script src="<?= base_url() ?>/public/demo/js/jquery.flexdatalist.min.js"></script>

<!-- Page specific script -->
<script>
  var id_preinscripcion = <?php echo $id_preinscripcion ?>;
  var repositorio = '<?php echo $repositorio; ?>';
</script>
<script>
  function alert_chico(icono, titulo) {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
    });
    Toast.fire({
      icon: icono,
      title: titulo
    });
  }

  var ToastD = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

  $(function() {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    /*$('.filter-container').filterizr({
      gutterPixels: 3
    });*/
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })

  jQuery(document).ready(function() {
    //$('#modal_bloqueo').modal('show');
    var idpreinscripcion = $('#id_pre').val();

    //get_observacion($('#id_pre').val());
    //Cuando el modal se oculta resetea todos los campos
    jQuery('#provincia_alumno_editar').flexdatalist({
      valueProperty: 'id',
      minLength: 3,
      selectionRequired: true,
      visibleProperties: ["provincia"],
      searchContain: true,
      searchDelay: 200,
      searchIn: 'provincia',
      noResultsText: 'No se encontraron coincidencias para "{keyword}"',
      data: '<?php echo site_url(); ?>provincia/autocompletar_provincia/'
    });


    $('#observacion_tutor').on('hidden.bs.modal', function() {
      $(":text").val("");
    });
    $('#observacion_tutor').on('show.bs.modal', function() {
      get_observacion($('#id_pre').val());
    });
    $('#modal_editar_alumno').on('hidden.bs.modal', function() {
     $(this).find("input,textarea,select").val('');
    });
    $('#modal_editar_alumno').on('show.bs.modal', function() {
      get_alumno();
    });

    $('#modal_editar_tutor').on('show.bs.modal', function(e) {
      //var id = $(this).data('id');
      var tutorId = $(e.relatedTarget).data('tutor-id');

      var datos = $('#tutor_' + tutorId).find('.datosTutor').val();
      datos = JSON.parse(datos);

      $(this).find('#tutor_id').val(datos['id']);
      $(this).find('#nombre_tutor_edit').val(datos['nombre']);
      $(this).find('#apellido_tutor_edit').val(datos['apellido']);
      $(this).find('#documento_tutor_edit').val(datos['documento']);
      $(this).find('#vive_tutor_edit').val(datos['vive']);
      $(this).find('#rol_tutor_edit').val(datos['rol']);
      $(this).find('#correo_tutor_edit').val(datos['correo']);
      $(this).find('#telefono2_tutor_edit').val(datos['telefono_laboral']);
      $(this).find('#telefono1_tutor_edit').val(datos['telefono_particular']);
      $(this).find('#domicilio_tutor_edit').val(datos['domicilio']);
      $(this).find('#ocupacion_tutor_edit').val(datos['ocupacion']);
      $(this).find('#empresa_tutor_edit').val(datos['empresa']);
      $(this).find('#cargo_tutor_edit').val(datos['cargo']);
    });

    $(function() {
      $("#reserva_nivel_alumno_editar").on('change', function() {
        var opcion = $(this).val();
        switch (opcion) {
          case "1":
            $("#grado_alumno_editar").hide();
            $("#sala_alumno_editar").hide();
            $("#anio_alumno_editar").show();
            break;

          case "3":
            $("#grado_alumno_editar").show();
            $("#sala_alumno_editar").hide();
            $("#anio_alumno_editar").hide();
            break;

          case "4":
            $("#grado_alumno_editar").hide();
            $("#sala_alumno_editar").show();
            $("#anio_alumno_editar").hide();
            break;
        }
      }).change();
    });

  });

  function editar_observacion() {
    jQuery.ajax({
      method: "POST",
      url: '<?php echo site_url('alumnos/editar_observacion_tutor'); ?>',
      dataType: "JSON",
      data: jQuery("#observacion_tutor").serialize(),
    }).done(function(data) {
      $('#txt_observacion').text(data[0]['observacion_tutor']);
      $('#observaciones_tutor').text(data[0]['observacion_tutor']);
      alert_chico('success', 'Se realizó el cambio con éxito')
    }).fail(function() {
      alert_chico('error', 'Existe un error en la carga, intentélo mas tarde')
    });
  }

  function get_observacion(idpre) {
    jQuery.ajax({
      method: "POST",
      url: '<?php echo site_url('alumnos/get_observaciones'); ?>',
      dataType: "JSON",
      data: {
        id: idpre
      },
    }).done(function(data) {
      $('#txt_observacion').val(data[0]['observacion_tutor']);
      $('#observaciones_tutor').text(data[0]['observacion_tutor']);
    }).fail(function() {
      alert_chico('error', 'Existe un error en la carga, intentélo mas tarde')
    });
  }

  function editar_alumno() {
    jQuery.ajax({
      method: "POST",
      url: '<?php echo site_url('alumnos/editar_alumno'); ?>',
      dataType: "JSON",
      data: jQuery("#editar_alumno").serialize(),
    }).done(function(data) {
      console.log(data);
      if (data['status'] == true) {
        $('#nombre_alumno').text(data['alumno']['nombre']);
        $('#documento_alumno').text(data['alumno']['documento']);
        $('#apellido_alumno').text(data['alumno']['apellido']);
        $('#legajo_alumno').text(data['alumno']['legajo']);
        $('#domicilio_alumno').text(data['alumno']['domicilio']);
        $('#fecha_nac_alumno').text(data['alumno']['fecha_nac']);
        $('#correo_alumno').text(data['alumno']['correo']);

        $('#nombre_nivel_reserva').text($('select[name="reserva_nivel_alumno_editar"] option:selected').text());
        if ($('select[name="reserva_nivel_alumno_editar"] option:selected').text() == 'Inicial') {
          $('#anio_reserva').text('Reserva a ' + $('select[name="sala_alumno_editar"] option:selected').text());
        } else if ($('select[name="reserva_nivel_alumno_editar"] option:selected').text() == 'Inicial') {
          $('#anio_reserva').text('Reserva a ' + $('select[name="grado_alumno_editar"] option:selected').text());
        } else $('#anio_reserva').text('Reserva a ' + $('select[name="anio_alumno_editar"] option:selected').text());

        if (data['pre']['aceptado'] == '1') {

          data['pre']['aceptado'] = '<span class="badge bg-warning">Pendiente</span>';
        } else if (data['pre']['aceptado'] == '2') {
          data['pre']['aceptado'] = '<span class="badge bg-success">Aceptado</span>';
        } else data['pre']['aceptado'] = '<span class="badge bg-danger">Rechazado</span>';
        $('#estado_reserva').html(data['pre']['aceptado']);

        alert_chico('success', 'Los datos se actualizaron correctamente');
      } else alert_chico('error', 'Se produjo un error, intentelo mas tarde');
    }).fail(function() {
      alert_chico('error', 'Se produjo un error, intentelo mas tarde')
    });
  }

  function editar_tutor() {
    jQuery.ajax({
      method: "POST",
      url: '<?php echo site_url('alumnos/editar_tutor'); ?>',
      //dataType: "JSON",
      data: jQuery("#editar_tutor").serialize(),
    }).done(function(data, status, xhr) {

      //console.log("xhr: "+xhr.responseText);
      datos = JSON.parse(data);
      //console.log("datos: "+datos);

      if (datos['estado'] == true)
        alert_chico('success', 'Los datos se actualizaron correctamente');
      else
        alert_chico('error', 'Se produjo un error, intentelo mas tarde');

      var id = datos['id'];
      $('#tutor_' + id).find('.datosTutor').val(data);

      datos = datos['datos'];

      //console.log("datos: "+datos);

      if (datos['vive'] == 1)
        datos['vive'] = "Si";
      else
        datos['vive'] = "No";

      if (datos['rol'] == 1)
        datos['rol'] = "Padre";
      else
      if (datos['rol'] == 2)
        datos['rol'] = "Madre";
      else
        datos['rol'] = "Tutor";

      $('#tutor_' + id).find('.username').html(datos['nombre'] + ' ' + datos['apellido']);
      $('#tutor_' + id).find('.documento').html(datos['documento']);
      $('#tutor_' + id).find('.vive').html(datos['vive']);
      $('#tutor_' + id).find('.description').html(datos['rol']);
      $('#tutor_' + id).find('.correo').html(datos['correo']);
      $('#tutor_' + id).find('.telefono2').html(datos['telefono_laboral']);
      $('#tutor_' + id).find('.telefono1').html(datos['telefono_particular']);
      $('#tutor_' + id).find('.domicilio').html(datos['domicilio']);
      $('#tutor_' + id).find('.ocupacion').html(datos['ocupacion'] + ' / ' + datos['empresa'] + ' / ' + datos['cargo']);

    }).fail(function(jqXHR, textStatus, errorThrown) {
      console.log("jqXHR: " + jqXHR.responseText);

      alert_chico('error', 'Se produjo un error, intentelo mas tarde')
    });
  }

  function get_alumno(idpre) {
    $('#nombre_alumno_editar').val($('#nombre_alumno').text());
    $('#documento_alumno_editar').val($('#documento_alumno').text());
    $('#apellido_alumno_editar').val($('#apellido_alumno').text());
    $('#legajo_alumno_editar').val($('#legajo_alumno').text());
    $('#domicilio_alumno_editar').val($('#domicilio_alumno').text());
    $('#fecha_nac_alumno_editar').val($('#fecha_nac_alumno').text());
    $('#correo_alumno_editar').val($('#correo_alumno').text());
    $('#ciudad_alumno_editar').val($('#ciudad_alumno').text());
  }

  var urlApiArchivos = 'https://archivos-nodos.com.ar/api_archivos/index.php';
  var passApiKey = '8kg0kw8swcgkkogw8woksokg4k4cg8ssskg4ogso';

  function delete_file_modal(id_file) {
    $("#id_file_modal_eliminar").val(id_file);
    $("#modal_eliminar_file").modal("show");
  }

  function delete_file()
  {
    //alert(id_file);
    $("#modal_eliminar_file").modal("hide");
    id_file = $("#id_file_modal_eliminar").val();
    var urlRequest = urlApiArchivos + "/upload/delete_file";

    $("#div_file_"+id_file).remove();
    $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {
              'APIKEY': passApiKey,
              //'userid' : idUser,
              'origen': 'pre',
              'Access-Control-Allow-Origin': '*'
            },
            data: {
              idfile: id_file,
              idevento: 0
            },
          })
          .done(function(data) {

            if (data['status'] == false) {
              //console.log(data['message']);
              //if(data['message']=='NO LOGUIN')
              //location.href = "<?php //echo base_url('login'); 
                                  ?>"; //no està logueado
              //terminadoP4(1); terminadoP4(2); terminadoP4(3);
            } else
              /*Swal.fire({
              icon: 'warning',
              title: 'Archivo Eliminado',
              text: 'El Archivo Fue Elmininado'
            });*/
            ToastD.fire({
              icon: 'success',
              title: 'El Archivo Fue Elmininado'
            })

          });


        //fin ajax nuevo
    // ahora hay q eliminar el registro del file en pre
    jQuery.ajax({
      method: "POST",
      url: '<?php echo site_url('alumnos/delete_file/'); ?>',
      dataType: "JSON",
      data: {id_file : id_file},
    }).done(function(data) 
    {
      
                                        
      
    });

  }

  function asignarFileAlumno(id_file, div_file,tipoArchivo)
  {
    jQuery.ajax({
      method: "POST",
      url: '<?php echo site_url('alumnos/asignarFileAlumno/'); ?>',
      dataType: "JSON",
      data: {id_file : id_file['id'], id_preinscripcion: id_preinscripcion, div_file: div_file},
    }).done(function(data) 
    {
      tipoArchivo = tipoArchivo.split("/");
                                        
      if (data['status'] == true) 
      {
        //console.log(data);
        var url = repositorio+'/'+id_file['carpeta']+'/'+id_file['id']+'_'+id_file['nombre'];
        
        if(tipoArchivo[0] == 'application')
        {
          
          var string = '<div class="col-sm-5" id="div_file_'+id_file['id']+'"> '+
            '<a href="'+url+'" data-toggle="lightbox" data-title="'+div_file+'" data-gallery="gallery" >'+
            '<embed src="'+url+'"  type="'+tipoArchivo[0]+'/'+tipoArchivo[1]+'" width="400" height="400"/>'+
            '<a title="Eliminar" class="eliminar btn btn-danger btn-sm ml-auto" onclick="delete_file_modal('+id_file['id']+')"><span class="far fa-trash-alt"></span> Eliminar</a>'+
            '</a>'+
            '</div>';
            $("#row_application_"+div_file).append(string);
        }
        else
        {
          
          var string = '<div class="col-sm-2" id="div_file_'+id_file['id']+'"> '+
            '<a href="'+url+'" data-toggle="lightbox" data-title="'+div_file+'" data-gallery="gallery" >'+
            '<img src="'+url+'" class="img-fluid mb-2" alt="" />'+
            '<a title="Eliminar" class="eliminar btn btn-danger btn-sm ml-auto" onclick="delete_file_modal('+id_file['id']+')"><span class="far fa-trash-alt"></span> Eliminar</a>'+
            '</a>'+
            '</div>';
            $("#row_img_"+div_file).append(string);
        }
        
        
      } else {

        
      }
      
    });
  }

  function subirFile(event, div_file)
  {
    //alert('a');
    var sizeArchivo = 20; //MB
    event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening
    files = event.target.files; //esta lina es la unica diferencia con el prepareUpload2
    $.each(files, function(key, value) {
      var data = new FormData();
      data.append(key, value);
      console.log(value)
      var size = value.size;
      if (size > 1024) {
        var temp = size / 1024;
        if (temp > 1024) {
          size = (temp / 1024).toFixed(2) + " MB";
          if ((temp / 1024).toFixed(2) > sizeArchivo)
            ecxedesize = 1;
        } 
        else
          size = temp.toFixed(2) + " KB";
      } 
      else
        size = size.toFixed(2) + " Bytes";

      var name = value.name;
      var tipoArchivo = value.type; 
      name = name.replace(new RegExp(' ', 'g'), "_");
      var nameaux = name.replace('.jpeg', '.jpg').replace('.png', '.jpg').replace('.gif', '.jpg');
      //ajax nuevo
      ToastD.fire({
              icon: 'info',
              title: 'Subiendo Archivo. Espere Por Favor...'
            })
      var urlRequest = urlApiArchivos + "/upload/upload_file";
    $.ajax({
        url: urlRequest,
        type: 'POST',
        headers: {
          'APIKEY': passApiKey,
          //'userid' : idUser,
          'origen': 'pre',
          'Access-Control-Allow-Origin': '*'
        },
        data: data,
        cache: false,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        beforeSend: function() {
          $('#modal_loading').modal('show');
          //aqui iria un loading
          //$('#'+div_file).after("<img id='loadingUP' src='http://inscripciones.e-nodos.com/public/demo/img/loading.gif' height='15' width='15' />");
        }
      })
      .done(function(data) {
        if (console && console.log) {
         // console.log("Sample of data:", data.slice(0, 100));
        }
        //done codigo del otro ajax
       $('#modal_loading').modal('hide');
        var data = jQuery.parseJSON(data);
        if (data['error']) 
        {
          ToastD.fire({
              icon: 'error',
              title: 'Intente en Otro Momento'
            })
        } 
        else {

          ToastD.fire({
              icon: 'success',
              title: 'Archivo Subido'
            })
          data = data['files'];
          for (var i = 0; i < data.length; i++) //aca coloco id de archivo al li. y agrego tilde suces
          {
           // console.log(data[i]);
            //asignarFileAlumno(data[i]['id'], div_file);
            asignarFileAlumno(data[i], div_file, tipoArchivo);
          }
        }
      });

    });
  }
  jQuery('#filer_documento_alumno_archivo').on('change', function(event) {
    subirFile(event, 'documento_alumno_archivo');
  });
  jQuery('#filer_documento_tutor1_archivo').on('change', function(event) {
    subirFile(event, 'documento_tutor1_archivo');
  });
  jQuery('#filer_documento_tutor2_archivo').on('change', function(event) {
    subirFile(event, 'documento_tutor2_archivo');
  });
  jQuery('#filer_foto_alumno_archivo').on('change', function(event) {
    subirFile(event, 'foto_alumno_archivo');
  });
  jQuery('#filer_libre_deuda_alumno_archivo').on('change', function(event) {
    subirFile(event, 'libre_deuda_alumno_archivo');
  });
  jQuery('#filer_boleta_servicio_alumno_archivo').on('change', function(event) {
    subirFile(event, 'boleta_servicio_alumno_archivo');
  });
  jQuery('#filer_libreta_alumno_archivo').on('change', function(event) {
    subirFile(event, 'libreta_alumno_archivo');
  });
  jQuery('#filer_certificado_conducta_alumno_archivo').on('change', function(event) {
    subirFile(event, 'certificado_conducta_alumno_archivo');
  });
  jQuery('#filer_certificado_uni_peda_alumno_archivo').on('change', function(event) {
    subirFile(event, 'certificado_uni_peda_alumno_archivo');
  });
  jQuery('#filer_analisis_chagas_alumno_archivo').on('change', function(event) {
    subirFile(event, 'analisis_chagas_alumno_archivo');
  });
  jQuery('#filer_libreta_flia_cristiana_alumno_archivo').on('change', function(event) {
    subirFile(event, 'libreta_flia_cristiana_alumno_archivo');
  });
  jQuery('#filer_certificado_bautismo_alumno_archivo').on('change', function(event) {
    subirFile(event, 'certificado_bautismo_alumno_archivo');
  });
  jQuery('#filer_partida_nacimiento_alumno_archivo').on('change', function(event) {
    subirFile(event, 'partida_nacimiento_alumno_archivo');
  });
  jQuery('#filer_carnet_obra_social_alumno_archivo').on('change', function(event) {
    subirFile(event, 'carnet_obra_social_alumno_archivo');
  });
  jQuery('#filer_cartilla_sanitaria_alumno_archivo').on('change', function(event) {
    subirFile(event, 'cartilla_sanitaria_alumno_archivo');
  });
  jQuery('#filer_carnet_vacunas_alumno_archivo').on('change', function(event) {
    subirFile(event, 'carnet_vacunas_alumno_archivo');
  });
  jQuery('#filer_Ficha_medica_alumno_archivo').on('change', function(event) {
    subirFile(event, 'Ficha_medica_alumno_archivo');
  });
  jQuery('#filer_bucodental_alumno_archivo').on('change', function(event) {
    subirFile(event, 'bucodental_alumno_archivo');
  });
  jQuery('#filer_audiometria_alumno_archivo').on('change', function(event) {
    subirFile(event, 'audiometria_alumno_archivo');
  });
  jQuery('#filer_Electrocardiograma_alumno_archivo').on('change', function(event) {
    subirFile(event, 'Electrocardiograma_alumno_archivo');
  });
  jQuery('#filer_ficha_salud_alumno_archivo').on('change', function(event) {
    subirFile(event, 'ficha_salud_alumno_archivo');
  });
  jQuery('#filer_oftalmologico_alumno_archivo').on('change', function(event) {
    subirFile(event, 'oftalmologico_alumno_archivo');
  });
</script>

</body>

</html>