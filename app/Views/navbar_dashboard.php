<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <!--
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Inicio</a>
                </li>
-->
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">

                <!-- Notifications Dropdown Menu -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="" class="brand-link">
                <img style="width:50px; height:50px;" src="<?= base_url() ?>/public/demo/fotos_colegios/<?php echo $colegios[0]['logo'] ?>">
                <span class="brand-text font-weight-light">Preinscripión</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">

                    <div class="info">
                        <a class="d-block"><?php echo $userName;?></a>
                    </div>
                </div>

                <!-- SidebarSearch Form -->
                <div class="form-inline">
                    <div class="input-group" data-widget="sidebar-search">
                        <input class="form-control form-control-sidebar" type="search" placeholder="Buscar" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-sidebar">
                                <i class="fas fa-search fa-fw"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                    <?php
                    if( count($colegios) > 0){

                        foreach ($colegios as $cole) {
                            $colegioName = $cole['tipo'];
                            $colegioId   = $cole['id'];
                            $niveles     = $cole['niveles'];
                    ?>

                            <li class="nav-header"><?php echo ucwords($colegioName);?></li>

                            <?php
                                if( count($niveles) > 0){

                                foreach ($niveles as $nivel) {
                                    $nivelName = $nivel['nombre'];
                                    $nivelId   = $nivel['id'];
                                    $nivelId2  = $colegioId.'_'.$nivelId;
                                    $anios     = $nivel['anios'];
                            ?>

                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="nav-icon fas fa-child"></i>
                                            <p>
                                                <?php echo ucwords(strtolower($nivelName));?>
                                                <i class="fas fa-angle-left right"></i>
                                            </p>
                                        </a>
                                        <ul class="nav nav-treeview">
                                            <li class="nav-item">
                                                <a href="<?php echo site_url("dashboard/principal/$colegioId/$nivelId") ?>" class="nav-link" id="anio_<?php echo $nivelId2;?>" >
                                                    <i class="far fa-circle nav-icon"></i>
                                                    <p>Todo</p>
                                                </a>
                                            </li>
                                            <?php
                                                if( count($anios) > 0){

                                                foreach ($anios as $anio) {
                                                    $anioName = $anio['nombre'];
                                                    $anioId   = $anio['id'];
                                                    $anioId2  = $nivelId2.'_'. $anioId;
                                            ?>
                                                    <li class="nav-item">
                                                        <a href="<?php echo site_url("dashboard/principal/$colegioId/$nivelId/$anioId") ?>" class="nav-link" id="anio_<?php echo $anioId2;?>" >
                                                            <i class="far fa-circle nav-icon"></i>
                                                            <p><?php echo $anioName;?></p>
                                                        </a>
                                                    </li>

                                            <?php 
                                                } //fin for años
                                            } // fin if años
                                            ?>
                                           
                                        </ul>
                                    </li>
                    <?php  
                                } //fin for niveles
                            }// fin if niveles
                        } //fin for colegios
                    } // fin if colegios
                    ?>

                       
                            </ul>
                        </li>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
