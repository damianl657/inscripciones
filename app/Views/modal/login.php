<!-- Ventana Modal Login NODOS-->
<form method="POST" action="<?php echo site_url('front/cargar_datos'); ?>" id="formlogin">
  <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Login:</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="usuario_nodos">Usuario</label>
            <input type="text" class="form-control" id="usuario_nodos" name="usuario_nodos" placeholder="Ingrese su usuario" autocomplete="off">
          </div>
          <div class="form-group">
            <label for="contraseña_nodos">Contraseña</label>
            <input type="text" class="form-control" id="contraseña_nodos" name="contraseña_nodos" pattern=".{5,15}" placeholder="Contraseña" autocomplete="off">
            <input type="text" class="form-control" id="colegio" name="colegio" pattern=".{5,15}" placeholder="Contraseña" autocomplete="off"value="" hidden>
          </div>
        </div>
        <div class="modal-footer">

          <button class="btn btn-primary" type="button" id="comprobar_login" name="comprobar_login" onclick="loguearse()">Loguearse </button>
          <button class="btn btn-primary" type="button" id="spinner2" name="spinner2" disabled hidden>
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            Buscando...
          </button>
        </div>
      </div>
    </div>
  </div>
</form>
<script>
  function loguearse() {
    if (jQuery('#contraseña_nodos').val().length == 0) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'La contraseña no puede ser vacia '
      });
    } else {
      jQuery("#spinner2").attr("hidden", false);
      jQuery("#comprobar_login").hide();
      jQuery.ajax({
        method: "POST",
        url: '<?php echo site_url("front/login/") ?>',
        dataType: "JSON",
        data: {
          usuario_nodos: document.getElementById("usuario_nodos").value,
          contraseña_nodos: document.getElementById("contraseña_nodos").value
        }
      }).done(function(data) {
        console.log(data);
        if (data == true) {
          document.forms['formlogin'].submit();
        } else {
          Swal.fire({
            icon: 'info',
            title: 'Ups! Algún dato es incorrecto!',
            text: 'Si no recuerda la contraseña presione el botón "Recuperar Contraseña"',
            focusCancel: true,
            showCancelButton: true,
            cancelButtonText: 'Cerrar',
            confirmButtonText: 'Recuperar Contraseña'
          }).then((result) => {
            if (result.value) {
              window.open(href = "https://appweb-nodos.com.ar/login/olvido_pass");
            }
          });
        }
      });
    }
  }

  function ver_contraseña() {

    if (document.getElementById("contraseña_nodos").type == "password") {
      document.getElementById("contraseña_nodos").type = "text";
      document.getElementById('ojo').classList.value = "fa fa-eye";
    } else {
      document.getElementById("contraseña_nodos").type = "password";
      document.getElementById('ojo').classList.value = "fa fa-eye-slash";
    }
  }

  jQuery(document).ready(function() {
    jQuery.noConflict();
    jQuery('#contraseña_nodos').focus();
    /*
    $('#contraseña_nodos').click(function(event) { //LOGIN
      event.preventDefault();
      ingresar();
    });
    */
    jQuery(document).keypress(function(e) {
      if (e.which == 13) {
        ingresar();
      }
    });
  });
</script>