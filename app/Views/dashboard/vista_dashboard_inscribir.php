<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-md-12">
                    <h3>Estado de alumnos no inscriptos de <?php echo $nombre_nivel ?> </h3>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
      <!--
    <section class="content">
        <div class="container-fluid">
           
            <div class="row">
              
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-tasks"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Pendientes</span>
                            <span class="info-box-number"><?php echo $pendientes ?></span>
                        </div>
                      
                    </div>
                
                </div>
               

               
                <div class="clearfix hidden-md-up"></div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="far fa-thumbs-down"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Rechazadas</span>
                            <span class="info-box-number"><?php echo $rechazados ?></span>
                        </div>
                      
                    </div>
                 
                </div>
           
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Total</span>
                            <span class="info-box-number"><?php echo $total ?></span>
                        </div>
                       
                    </div>
                   
                </div>
               
            </div>
           


        </div>
   
    </section>
    -->
    <!-- Main content -->
    <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                        <div class="card">
                        <div class="card-header">
                            <h3 class="card-title col-md-8">Alumnos sin insripcion de <?php echo $nombre_nivel ?></h3>
                          
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tabla_reserva_alumno" class="table table-striped table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th >Legajo</th>
                                        <th >Documento</th>
                                        <th >Apellidos</th>
                                        <th >Nombres</th>
                                        <th >Reserva</th>
                                        <th >Fecha</th>
                                        <th >Observaciones</th>
                                        <th >Documental</th>
                                        <th >Económico</th>
                                        <th >Acción</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                            <!-- /.card -->

                         
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->


</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery 
<script src="<?= base_url() ?>/public/demo/plugins/jquery/jquery.min.js"></script>
-->
<script src="<?= base_url() ?>/public/demo/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

-->
<script src="<?= base_url() ?>/public/demo/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- overlayScrollbars -->
<script src="<?= base_url() ?>/public/demo/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>/public/demo/dist/js/adminlte.js"></script>
<!-- Sweet Alert 2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>/public/demo/dist/js/demo.js"></script>
<!-- DataTables  & Plugins -->

<!-- Datatable core JavaScript-->
<script src="<?= base_url() ?>/public/demo/js/datatable/datatableCodigoReusable.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/buttons.print.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/jszip.min.js"></script>
<script src="<?= base_url() ?>/public/demo/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url() ?>/public/demo/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<script>
    $(document).ready(function() {
       
        var tabla = dataTablese("tabla_reserva_alumno", "<?php echo site_url("dashboard/cargar_tabla_reserva_alumnos/") . $colegio . '/' . $id_anio ?>", 0);
       // var tabla_postu = dataTablese("tabla_reserva_postulantes", "<?php echo site_url("dashboard/cargar_tabla_reserva_postulantes/") . $colegio . '/' . $id_anio ?>",0);
        var editar = function(tbody, table) {
            $(tbody).on("click", "button.editar", function() {
                if (table.row(this).child.isShown()) {
                    var data = table.row(this).data();
                } else {
                    var data = table.row($(this).parents("tr")).data();
                }
                window.location.replace('<?php echo site_url("alumnos/alu/") ?>' + data[0]);

            });
        };

        editar("#tabla_reserva_alumno", tabla);
        var editar_postu = function(tbody, table) {
            $(tbody).on("click", "button.editar", function() {
                if (table.row(this).child.isShown()) {
                    var data = table.row(this).data();
                } else {
                    var data = table.row($(this).parents("tr")).data();
                }
                window.location.replace('<?php echo site_url("alumnos/postu/") ?>' + data[0]);
            });
        };

        editar_postu("#tabla_reserva_postulantes", tabla_postu);

        var pago_matricula = function(tbody, table) {
            $(tbody).on("click", "button.pago", function() {
                if (table.row(this).child.isShown()) {
                    var data = table.row(this).data();
                } else {
                    var data = table.row($(this).parents("tr")).data();
                }
                console.log(data);
                $('#id_pre').val(data[0]);
                $('#select_economico').val(data[10]);
                $('#modal_economico').modal('show');
            });
        };
        pago_matricula("#tabla_reserva_alumno", tabla);
        pago_matricula("#tabla_reserva_postulantes", tabla_postu);


    });

    function alert_chico(icono, titulo) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
        });
        Toast.fire({
            icon: icono,
            title: titulo
        });
    }

    function editar_estado_economico() {
        jQuery.ajax({
            method: "POST",
            url: '<?php echo site_url('dashboard/editar_estado_economico'); ?>',
            dataType: "JSON",
            data: jQuery("#editar_economico").serialize(),
        }).done(function(data) {
            console.log(data);
            $('#tabla_reserva_alumno').DataTable().ajax.reload();
            $('#tabla_reserva_postulantes').DataTable().ajax.reload();
           
            alert_chico('success', 'Se realizó el cambio con éxito')
        }).fail(function() {
            alert_chico('error', 'Existe un error en la carga, intentélo mas tarde')
        });
    }
</script>

</body>

</html>