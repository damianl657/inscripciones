<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Reservas de matrícula por Nivel</h1>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-child"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Inicial</span>
                            <span class="info-box-number"><?php echo $total_inicial ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-book"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Primaria</span>
                            <span class="info-box-number"><?php echo $total_primaria ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix hidden-md-up"></div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-graduation-cap"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Secundaria</span>
                            <span class="info-box-number"><?php echo $total_secundaria ?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Reservas realizadas</span>
                            <span class="info-box-number"><?php echo $total_total?></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


        </div>
        <!--/. container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12-md">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title col-md-8">Resumen de reserva de matrícula de alumnos</h3>
                            <button class="btn btn-primary col-md-4">
                                <span><i class="fa fa-plus"></i></span>
                                Generar reserva alumnos
                            </button>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tabla_reserva_alumno" class="table table-bordered  display nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:10%">Nro</th>
                                        <th>Documento</th>
                                        <th>Apellidos</th>
                                        <th>Nivel</th>
                                        <th>Reserva</th>
                                        <th>Fecha</th>
                                        <th>Estado</th>
                                        <th style="width:10%">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <div class="card">
                        <div class="card-header">
                            
                            <h3 class="card-title col-md-8">Resumen de reserva de matrícula de postulantes</h3>
                            <button class="btn btn-primary col-md-4">
                                <span><i class="fa fa-plus"></i></span>
                                Generar reserva postulantes
                            </button>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="tabla_reserva_postulante" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                        <th style="width:10%">Nro</th>
                                        <th>Documento</th>
                                        <th>Apellidos</th>
                                        <th>Nivel</th>
                                        <th>Reserva</th>
                                        <th>Proviene</th>
                                        <th>Fecha</th>
                                        <th>Estado</th>
                                        <th style="width:10%">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->


</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery 
<script src="<?= base_url() ?>/public/demo/plugins/jquery/jquery.min.js"></script>
-->
<script src="<?= base_url() ?>/public/demo/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

-->
<script src="<?= base_url() ?>/public/demo/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- overlayScrollbars -->
<script src="<?= base_url() ?>/public/demo/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>/public/demo/dist/js/adminlte.js"></script>


<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>/public/demo/dist/js/demo.js"></script>
<!-- DataTables  & Plugins -->

<!-- Datatable core JavaScript-->
<script src="<?= base_url() ?>/public/demo/js/datatable/datatableCodigoReusable.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/buttons.print.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>/public/demo/js/datatable/jszip.min.js"></script>
<!-- extension responsive -->
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script>
    $(document).ready(function() {
        var tabla = dataTablese("tabla_reserva_alumno", "<?php echo site_url("dashboard/cargar_tabla_reserva_alumno") ?>");
        var editar = function(tbody, table) {
            $(tbody).on("click", "button.editar", function() {
                if (table.row(this).child.isShown()) {
                    var data = table.row(this).data();
                } else {
                    var data = table.row($(this).parents("tr")).data();
                }
                window.location.replace('<?php echo site_url("alumnos/inicio/") ?>'+ data[0]);
                //console.log(data);
            });
        };

        editar("#tabla_reserva_alumno", tabla);
        $(document).on("click", "button.eliminar", function() {
          //  $('#eliminarMetodo').modal('show');
        });


    });

    $(document).ready(function() {
        var tabla = dataTablese("tabla_reserva_postulante", "<?php echo site_url("dashboard/cargar_tabla_reserva_postulante") ?>");
        var editar = function(tbody, table) {
            $(tbody).on("click", "button.editar", function() {
                if (table.row(this).child.isShown()) {
                    var data = table.row(this).data();
                } else {
                    var data = table.row($(this).parents("tr")).data();
                }
                console.log(data);
                $('#idMetodoModificado').val(data[0]).hide();
                $('#metodoModificado').val(data[1]);
                $('#editarMetodo').modal('show');
            });
        };

        editar("#tablaMetodo tbody", tabla);
        $(document).on("click", "button.eliminar", function() {
            $('#eliminarMetodo').modal('show');
        });


    });
</script>

</body>

</html>