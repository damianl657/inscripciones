<?php

namespace App\Controllers;


use App\Libraries\Variables;


class Front extends BaseController
{

	protected $tabla_colegio;
	protected $datos_alumno_tutor;
	protected $header_dinamico;
	protected $colegio;
	protected $tabla_nacionalidad;
	protected $alumno;
	protected $tabla_preinscripcion;
	protected $tabla_tutor;
	protected $tabla_tutor_alumno;
	protected $tabla_nombre_div;
	protected $archivos;
	protected $session;
	protected $tabla_niveles;

	public function __construct()
	{
		$this->tabla_colegio = new \App\Models\ColegioModel();
		$this->tabla_nacionalidad = new \App\Models\NacionalidadModel();
		$this->alumno = new \App\Models\AlumnoModel();
		$this->tabla_preinscripcion = new \App\Models\PreinscripcionModel();
		$this->tabla_tutor = new \App\Models\TutorModel();
		$this->tabla_tutor_alumno = new \App\Models\TutoralumnoModel();
		$this->archivos = new \App\Models\FileModel();
		$this->tabla_nombre_div = new \App\Models\NombredivModel();
		$this->tabla_niveles = new \App\Models\NivelesModel();
		$this->header_dinamico = array();
		$this->datos_alumno_tutor = array();


		//$this->session = \Config\Services::session();
		/*
		$config = config('Variables');
		$this->data["pathplantilla"] = $config->pathfront;
	
		$this->session = \Config\Services::session();
		$config = config('Variables');
		$this->data["pathplantilla"] = $config->pathfront;
		
		*/
	}

	/**
	 * index
	 *
	 * @return void
	 * @author Seba Morrone
	 */
	public function index()
	{
		$idcolegio = 18;
		$colegio = $this->tabla_colegio->find($idcolegio);
		$this->header_dinamico["logo"] = $colegio["logo"];
		$this->header_dinamico["nombre"] = $colegio["nombre"];
		$this->header_dinamico["idcolegio"] = $colegio["id"];
		$this->header_dinamico["titulo_web"] = $colegio["titulo_web"];
		$this->header_dinamico["requisitos"] = $colegio["requisitos"];
		echo view("front/vista_inc_header", $this->header_dinamico);
		echo view('front/vista_principal');
		echo view("front/vista_inc_footer");
		echo view("modal/login");
	}



	/**
	 * inicio
	 * Muestra publicidad de las inscripciones
	 * @param  mixed $idcolegio
	 * @return void
	 * @author Seba Morrone
	 */
	public function inicio($idcolegio)
	{
		$colegio = $this->tabla_colegio->find($idcolegio);
		$this->header_dinamico["logo"] = $colegio["logo"];
		$this->header_dinamico["nombre"] = $colegio["nombre"];
		$this->header_dinamico["idcolegio"] = $colegio["id"];
		$this->header_dinamico["titulo_web"] = $colegio["titulo_web"];
		$this->header_dinamico["requisitos"] = $colegio["requisitos"];
		echo view("front/vista_inc_header", $this->header_dinamico);
		echo view('front/vista_principal');
		echo view("front/vista_inc_footer");
	}

	/**
	 * inicio
	 * Muestra publicidad de las inscripciones
	 * @param  mixed $idcolegio
	 * @return void
	 * @author Seba Morrone
	 */
	public function inicioAlumnos($idcolegio, $dni, $token)
	{
		//chequeamos login desde nodos
		$nodosLogin = $this->checkLoginNodos($dni, $token);
		if (!!$nodosLogin) {
			//creamos la session de user
			$this->createSession($nodosLogin);
		}

		if ($this->session->get("logged_in")) {
			$colegio = $this->tabla_colegio->find($idcolegio);
			$this->header_dinamico["logo"] = $colegio["logo"];
			$this->header_dinamico["nombre"] = $colegio["nombre"];
			$this->header_dinamico["idcolegio"] = $colegio["id"];
			$this->header_dinamico["titulo_web"] = $colegio["titulo_web"];
			$this->header_dinamico["requisitos"] = $colegio["requisitos"];
			echo view("front/vista_inc_header", $this->header_dinamico);
			echo view('front/vista_principal_logueado');
			echo view("front/vista_inc_footer");
		}
	}

	/**
	 * createSession
	 *
	 * @param  mixed $datos
	 * @return void
	 */
	protected function createSession($datos)
	{
		//$session = session();
		$newdata = [
			//'email'    => 'johndoe@some-site.com',
			'dni'       => $datos['dni'],
			'userId'    => $datos['userId'], //este id (sin encriptar) no se si hace falta
			'userIdE'   => $datos['userIdE'],
			'colegioId' => $datos['colegioId'],
			'logged_in' => TRUE,
			'admin'     => false,
		];
		$this->session->set($newdata);
	}

	/**
	 * checkLoginNodos
	 *
	 * @param  mixed $dniNodos
	 * @param  mixed $tokenNodos
	 * @return void
	 */
	protected function checkLoginNodos($dniNodos, $tokenNodos)
	{

		if (cache()->get('loginNodos_' . $dniNodos)) {

			$dataNodosCache = cache()->get('loginNodos_' . $dniNodos);
			$dataNodosCache = json_decode($dataNodosCache, true);
			/*$arre = [
	        	'token'     => $token,
	        	'dni'       => $dni,
	        	'userId'    => $userId,
	        	'userIdE'   => $userIdE,
	        	'dni'       => $dni,
	        	'colegioId' => $idcolegio,
	        ];*/

			if ($dataNodosCache['token'] === $tokenNodos) {

				//NO VERIFICAMOS USER
				//$respuesta = $this->user->comprueba_dni($dniNodos);

				//borramos caché, por seguridad y ademas ya no sirve
				cache()->delete('loginNodos_' . $dniNodos);

				return $dataNodosCache;
			}
			return false;
		} else return false;
	}

	protected function destroySession()
	{
		$this->session->destroy();
		//$this->session->stop();
	}

	/**
	 * login
	 * le permite al usuario acceder a la información que posee nodos sobre el
	 * @param $uauasrio, $contraseña
	 * @return boolean
	 * @author Seba Morrone
	 */
	public function login()
	{
		$email = $this->request->getPost("usuario_nodos");
		$password = $this->request->getPost("contraseña_nodos");
		$variable_login = new Variables();
		$urlApi = $variable_login->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $variable_login->get_apikey();
		$url = $urlApi . "/login/acceder_principal";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('email' => $email, 'password' => $password),
			CURLOPT_SSL_VERIFYPEER => false,
		);
		curl_setopt_array($ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		$datos_login = json_decode($response);
		$respuesta = json_encode($datos_login->status);
		return $respuesta;
	}

	/**
	 * if_alumno
	 * Recibe alumno y tutores si existen, los alamcena y envia como respuesta true o false si el alumno existe
	 * @param $documento
	 * @return boolean
	 * @author Seba Morrone
	 */
	public function if_alumno()
	{
		$documento = $this->request->getPost("documento");
		$variable_login = new Variables();
		$idusuario = $variable_login->get_userid();
		$urlApi = $variable_login->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $variable_login->get_apikey();
		$url = $urlApi . "/alumno/info_para_inscripcion";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey", "userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('documento' => $documento),
			CURLOPT_SSL_VERIFYPEER => false,
		);
		curl_setopt_array($ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		$aux = $response;
		$this->datos_alumno_tutor = json_decode($response);
		$respuesta = json_encode($this->datos_alumno_tutor->status);
		$datos = json_decode($aux, true);
		if (!$datos_alumno_tutor = cache('datos_alumno_tutor')) {
			$datos_alumno_tutor = $datos;
			cache()->save('datos_alumno_tutor', $datos_alumno_tutor, 600);
		}

		return $respuesta;
	}


	/**
	 * revisar_inscripcion
	 *
	 * @return void
	 */
	public function revisar_inscripcion()
	{

		$documento = $this->request->getPost("documento");
		$idcolegio = $this->request->getPost("idcolegio");
		$alumno = $this->alumno->where('documento', $documento)->findAll();
		//Existe el alumno
		if ($alumno) {
			$datos = $this->tabla_preinscripcion->get_alumno_preinscripto($alumno[0]['id'], $idcolegio);
			if ($datos['status'] == true) {
				$data['status'] = true;
				$data['id'] = $datos['inscripto'][0]['id'];
				return json_encode($data);
			} else {
				$data['status'] = false;
				$data['alumno'] = true;
			}
			return json_encode($data);
		} else return json_encode($data['status'] = false);
	}

	/**
	 * revisar_inscripcion_relacion
	 *
	 * @return void
	 */
	public function revisar_inscripcion_relacion()
	{
		$documento_tutor = $this->session->get("dni");
		$documento = $this->request->getPost("documento");
		$idcolegio = $this->request->getPost("idcolegio");
		$alumno = $this->alumno->where('documento', $documento)->findAll();
		$tutor = $this->tabla_tutor->where('documento', $documento_tutor)->findAll();

		//Existe el alumno
		if ($alumno) {
			$datos = $this->tabla_preinscripcion->get_alumno_preinscripto($alumno[0]['id'], $idcolegio);
			if ($datos['status'] == true) {
				$data['status'] = true;
				$data['id'] = $datos['inscripto'][0]['id'];
				return json_encode($data);
			} elseif ($tutor) {
				$tutor_alumno = $this->tabla_tutor_alumno->buscar_parentesco($alumno[0]['id'], $tutor[0]['id']);
				if ($tutor_alumno) {
					//existe el tutor, el alumno es hijo del tutor y el alumno no esta inscripto
					$data['status'] = false;
					$data['relacion'] = true;
					$data['documento_tutor'] = $documento_tutor;
					$data['existe_tutor'] = true;
					$data['existe_alumno'] = true;
					$data['corresponde_alumno_nivel'] = $this->comprobar_nivel_alumno($alumno, $idcolegio);
					return json_encode($data);
				} else {
					// existe el tutor, el alumno NO es hijo del tutor y el alumno no esta inscripto
					$data['status'] = false;
					$data['relacion'] = false;
					$data['existe_tutor'] = true;
					$data['existe_alumno'] = true;
					return json_encode($data);
				}
			} else {
				//NO existe el tutor y el alumno no esta inscripto
				$data['status'] = false;
				$data['relacion'] = false;
				$data['existe_tutor'] = false;
				$data['existe_alumno'] = true;
				return json_encode($data);
			}
		} else {
			//NO existe el tutor y el alumno no existe
			$data['status'] = false;
			$data['relacion'] = false;
			$data['existe_tutor'] = false;
			$data['existe_alumno'] = false;
			return json_encode($data);
		}
	}

	public function comprobar_nivel_alumno($alumno, $idcolegio)
	{
		$nivel_colegio = $this->tabla_niveles->get_id_nivel($idcolegio);
		$alumno_nivel = $this->tabla_niveles->where('id', $alumno[0]['nodonivel_id'])->findAll();
		if ($alumno_nivel[0]['nodocolegio_id'] == $idcolegio) {
			return true;
		} elseif ($alumno[0]['anio_id'] == 6 && $nivel_colegio[0]['id'] == 1) {
			return true;
		} elseif ($alumno[0]['anio_id'] == 11 && $nivel_colegio[0]['id'] == 3) {
			return true;
		} else return false;

	}
}
