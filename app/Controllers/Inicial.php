<?php

namespace App\Controllers;

class Inicial extends BaseController
{
	
	protected $datos_inicial;

	public function __construct()
	{
		
		$this->datos_inicial = new \App\Models\InicialModel();
		
	}

	public function index()
	{
       
    }
    
    public function inicio($idcolegio)
	{/*
        $datos["total_inicial"]=$this->datos_inicial->total_reserva_alumnos_inicial();
        $datos["total_primaria"]=$this->info_reserva_alumno_primaria();
        $datos["total_secundaria"]=$this->info_reserva_alumno_secundaria();
        $datos["total_total"]=$this->info_reserva_alumno_total();
        echo view("dashboard/header");
        echo view("navbar_dashboard");
        echo view("dashboard/index",$datos);
        */
    }

    public function sala4()
    {
        $idcolegio=37;
        $sala='"Sala de 4"';
        $datos["aceptados_inicial"]=($this->datos_inicial->total_reserva_estado_por_sala($idcolegio,$estado=2,$sala))->total_inicial;
        $datos["pendientes_inicial"]=($this->datos_inicial->total_reserva_estado_por_sala($idcolegio,$estado=1,$sala))->total_inicial;
        $datos["rechazados_inicial"]=($this->datos_inicial->total_reserva_estado_por_sala($idcolegio,$estado=0,$sala))->total_inicial;
        $datos["total_inicial_sala4"]=($this->datos_inicial->total_reserva_por_sala($idcolegio,$sala))->total_inicial;
        $datos["nombre"]="Sala de 4";
        //print_r($datos["total_inicial_sala4"]);
        //die();
        echo view("dashboard/header");
        echo view("navbar_dashboard");
        echo view("carga_datos/don_bosco_inicial/vista_dashboard_inicial4",$datos);
    }

    public function sala5()
    {
        $idcolegio=37;
        $sala='"Sala de 5"';
        $datos["aceptados_inicial"]=($this->datos_inicial->total_reserva_estado_por_sala($idcolegio,$estado=2,$sala))->total_inicial;
        $datos["pendientes_inicial"]=($this->datos_inicial->total_reserva_estado_por_sala($idcolegio,$estado=1,$sala))->total_inicial;
        $datos["rechazados_inicial"]=($this->datos_inicial->total_reserva_estado_por_sala($idcolegio,$estado=0,$sala))->total_inicial;
        $datos["total_inicial_sala5"]=($this->datos_inicial->total_reserva_por_sala($idcolegio,$sala))->total_inicial;
        $datos["nombre"]="Sala de 5";
        echo view("dashboard/header");
        echo view("navbar_dashboard");
        echo view("carga_datos/don_bosco_inicial/vista_dashboard_inicial5",$datos);
    }
    
    public function cargar_tabla_reserva_sala4() 
    {
        $query = $this->datos_inicial->get_datos_preinscripcion_sala4();
        $data = array();

        foreach ($query->getResult() as $row) {
            if ($row->aceptado == '1') {
                $row->aceptado = '<span class="badge bg-warning">Pendiente</span>';
            } else if($row->aceptado=='2'){
                $row->aceptado = '<span class="badge bg-success">Aceptado</span>';
            }else $row->aceptado = '<span class="badge bg-danger">Rechazado</span>';
            $data[] = array(
                $row->id,
                $row->documento,
                $row->apellido,
                $row->nombre,
                $row->anio_inscripto,
                $row->escuela_proviene,
                $row->fecha,
                $row->aceptado,  
            );
        }


        $output = array(
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }
    
    public function info_reserva_alumno_inicial() 
    {
        $nro=$this->datos_preinscripcion->total_reserva_alumnos_inicial();
         return $nro->total_inicial;
        
    }

    public function info_reserva_alumno_primaria() 
    {
        $nro=$this->datos_preinscripcion->total_reserva_alumnos_primaria();
         return $nro->total_primaria;
        
    }

    public function info_reserva_alumno_secundaria() 
    {
        $nro=$this->datos_preinscripcion->total_reserva_alumnos_secundaria();
         return $nro->total_secundaria;
        
    }

    public function info_reserva_alumno_total() 
    {
        $nro=$this->datos_preinscripcion->total_reserva_alumnos_donbosco();
         return $nro->total_totales;
        
    }
    
}
