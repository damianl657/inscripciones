<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
	
	protected $datos_preinscripcion;

	public function __construct()
	{
		$this->datos_preinscripcion = new \App\Models\PreinscripcionModel();
        $this->datos_colegio = new \App\Models\ColegioModel();
        $this->datos_anios = new \App\Models\AniosModel();
        $this->datos_nivel = new \App\Models\NivelesModel();
	}

	public function index()
	{
        /*
        if (!$this->session->get("logged_in") && !$this->session->get("admin")) {
            echo "no autorizado";
            die();
        }
        */
        $datos["total_inicial"]=$this->info_reserva_alumno_inicial($idcolegio=37);
        $datos["total_primaria"]=$this->info_reserva_alumno_primaria($idcolegio=33);
        $datos["total_secundaria"]=$this->info_reserva_alumno_secundaria($idcolegio=32);
        $datos["total_total"]=$this->info_reserva_alumno_total();
        echo view("dashboard/header");
        echo view("navbar_dashboard");
        echo view("dashboard/index",$datos);
    }

    protected function checkLogin(){
        if ($this->session->get("logged_in") && $this->session->get("admin")) 
            return true;
        else return false;
    }

    protected function getInfoColegios($colegiosArray){

        $colegiosString = join(',',$colegiosArray);
        
        $colegios = $this->datos_colegio->getColegiosByIds($colegiosString);
        
        //BUSCAMOS COLEGIOS
        foreach ($colegios as &$cole) {
            //echo $cole['id'].'--'.$cole['nombre'];
            $id = $cole['id'];
            $planestudio = $this->datos_colegio->obtener_planestudio($cole['planestudio']);
            $cicloa = $planestudio['id'];
            $cole["total"] = $this->info_reserva_alumno($id);
            
            //BUSCAMOS NIVELES
            $niveles = $this->datos_nivel->getNivelesByColegiosIds($id, $cicloa);
            //print_r($niveles); die();
            $nivelesAux = array();
            foreach ($niveles as &$nivel) {
                if(!!$nivel['especialidadName'])
                    $nivelName = $nivel['nivelName'].' '.$nivel['especialidadName'];
                else $nivelName = $nivel['nivelName'];

                METER AÑOS ACÄ....

                $aux = [
                    'id'     => $nivel['id'],
                    'nombre' => $nivelName,
                ];
                $nivelesAux[] = $aux;
            }

            $cole["niveles"] = $nivelesAux;
        }

        return $colegios;
    }
    
    public function inicio($idcolegio=null, $dni=null, $token=null)
	{
        //chequeamos login desde nodos
        $nodosLogin = $this->checkLoginNodos($dni, $token);
        if (!!$nodosLogin) {
            //creamos la session de user
            $this->createSession($nodosLogin, true);
        }

        $this->createSessionPruebas($nodosLogin, true);

        //verifico que esté logueado como admin
        if(!$this->checkLogin()){
            echo "No Autorizado"; //redirect a mensaje de error
            die();
        }

        //print_r($this->session->get("colegiosId")); die();

        $colegiosArray = $this->session->get("colegiosId");
        $colegios = $this->getInfoColegios($colegiosArray);

        $total = 0;
        foreach ($colegios as &$cole) {
            //echo $cole['id'].'--'.$cole['nombre'];
            $total += $cole['total'];
        }

        print_r($colegios); die();

        /*$datos["total_primaria"]=$this->info_reserva_alumno($idcolegio=33);
        $datos["total_secundaria"]=$this->info_reserva_alumno($idcolegio=32);
        $datos["total_inicial"]=$this->info_reserva_alumno($idcolegio=37);*/

        $datos["colegios"] = $colegios;
        $datos["total_total"] = $total;

        echo view("dashboard/header");
        echo view("navbar_dashboard");
        echo view("dashboard/index",$datos);
    }
    
    public function cargar_tabla_reserva_alumno() 
    {
        $query = $this->datos_preinscripcion->get_datos_preinscripcion_alumno();
        $data = array();

        foreach ($query->getResult() as $row) {
            if ($row->aceptado == '1') {
                $row->aceptado = '<span class="badge bg-warning">Pendiente</span>';
            } else if($row->aceptado=='2'){
                $row->aceptado = '<span class="badge bg-success">Aceptado</span>';
            }else $row->aceptado = '<span class="badge bg-danger">Rechazado</span>';
            $data[] = array(
                $row->id,
                $row->documento,
                $row->apellido,
                $row->nombre,
                $row->anio_inscripto,
                $row->fecha,
                $row->aceptado,  
            );
        }
        $output = array(
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

    public function cargar_tabla_reserva_postulante() 
    {
        $query = $this->datos_preinscripcion->get_datos_preinscripcion_postulante();
        $data = array();

        foreach ($query->getResult() as $row) {
            if ($row->aceptado == '1') {
                $row->aceptado = '<span class="badge bg-warning">Pendiente</span>';
            } else if($row->aceptado=='2'){
                $row->aceptado = '<span class="badge bg-success">Aceptado</span>';
            }else $row->aceptado = '<span class="badge bg-danger">Rechazado</span>';
            $data[] = array(
                $row->id,
                $row->documento,
                $row->apellido,
                $row->nombre,
                $row->anio_inscripto,
                $row->escuela_proviene,
                $row->fecha,
                $row->aceptado,  
            );
        }


        $output = array(
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }
    
    /*public function info_reserva_alumno_inicial($idcolegio) 
    {
        $nro=$this->datos_preinscripcion->total_reserva_alumnos_por_colegio($idcolegio);
         return $nro->total_colegio;   
    }

    public function info_reserva_alumno_primaria($idcolegio)  
    {
        $nro=$this->datos_preinscripcion->total_reserva_alumnos_por_colegio($idcolegio);
        return $nro->total_colegio;   
    }

    public function info_reserva_alumno_secundaria($idcolegio) 
    {
        $nro=$this->datos_preinscripcion->total_reserva_alumnos_por_colegio($idcolegio);
        return $nro->total_colegio;  
    }*/

    public function info_reserva_alumno($idcolegio) 
    {
        $nro=$this->datos_preinscripcion->total_reserva_alumnos_por_colegio($idcolegio);
        return $nro->total_colegio;   
    }

    public function info_reserva_alumno_total() 
    {
        $nro=$this->datos_preinscripcion->total_reserva_donbosco();
         return $nro->total_totales;
        
    }


    /**
     * createSession
     *
     * @param  mixed $datos
     * @return void
     */
    protected function createSession($datos, $admin=false)
    {
        //$session = session();
        $newdata = [
            //'email'    => 'johndoe@some-site.com',
            'dni'       => $datos['dni'],
            'userId'    => $datos['userId'], //este id (sin encriptar) no se si hace falta
            'userIdE'   => $datos['userIdE'],
            'colegiosId' => json_decode($datos['colegiosId']),
            'logged_in' => TRUE,
            'admin'     => $admin,
        ];
        $this->session->set($newdata);
    }

    protected function createSessionPruebas($datos, $admin=false)
    {
        //$session = session();
        $newdata = [
            //'email'    => 'johndoe@some-site.com',
            'dni'       => 32689887,
            'userId'    => 9, //este id (sin encriptar) no se si hace falta
            'userIdE'   => '1234564sdfsdf',
            'colegiosId' => [32,33],
            'logged_in' => TRUE,
            'admin'     => $admin,
        ];
        $this->session->set($newdata);
    }

    /**
     * checkLoginNodos
     *
     * @param  mixed $dniNodos
     * @param  mixed $tokenNodos
     * @return void
     */
    protected function checkLoginNodos($dniNodos, $tokenNodos)
    {

        if (cache()->get('loginNodosAdmin_' . $dniNodos)) {

            $dataNodosCache = cache()->get('loginNodosAdmin_' . $dniNodos);
            $dataNodosCache = json_decode($dataNodosCache, true);  

            if ($dataNodosCache['token'] === $tokenNodos) {

                //NO VERIFICAMOS USER
                //$respuesta = $this->user->comprueba_dni($dniNodos);

                //borramos caché, por seguridad y ademas ya no sirve
                cache()->delete('loginNodosAdmin_' . $dniNodos);

                return $dataNodosCache;
            }
            return false;
        } else return false;
    }

    protected function destroySession()
    {
        $this->session->destroy();
        //$this->session->stop();
    }


}
