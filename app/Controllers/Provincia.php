<?php

namespace App\Controllers;

class Provincia extends BaseController
{
	protected $tabla_provincia;


	public function __construct()
	{
		$this->tabla_provincia = new \App\Models\ProvinciaModel();
	}

	public function index()
	{
	}

	public function autocompletar_provincia()
	{
        $resultado = $this->tabla_provincia->get_tabla_provincias();
        foreach ($resultado as $row) {
            $arr_result[] = $row;
        }
        echo json_encode($arr_result);
		
	}
}
