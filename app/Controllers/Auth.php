<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Libraries\Variables;

class Auth extends BaseController
{
	protected $variablesLib;
	

	public function __construct()
	{
        //$this->tabla_colegio = new \App\Models\ColegioModel();
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }

		$this->variablesLib = new Variables();
		
	}


    protected function randomPassword($size=25)
    {
        //$alphabet    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890()!@#$%&{}<>?[]=+-*&~|%';
        $alphabet    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass        = []; //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $size; $i++) {
            $n      = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass); //turn the array into a string
    }

    public function generate_token(){

        //$headers = apache_request_headers();
        $apiTokenRecibido = $this->request->getHeader('Apikey')->getValue();
        $userIdE   = $this->request->getHeader('Useridenc')->getValue();

        $userId    = $_POST['idusuario'];
        $dni       = $_POST['dni'];
        $idcolegio = $_POST['idcolegio'];

        $apiTokenReal = $this->variablesLib->get_apiToken();

        if($apiTokenReal != $apiTokenRecibido){
            $data = ["status"=>0, "mensaje"=>"No Autorizado"];
            echo json_encode($data);      
            die();   
        }

        $token = $this->randomPassword();

        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        cache()->delete('loginNodos_'.$dni);
        $arre = [
            'admin'     => false,
        	'token'     => $token,
        	'dni'       => $dni,
        	'userId'    => $userId,
        	'userIdE'   => $userIdE,
        	'dni'       => $dni,
        	'colegioId' => $idcolegio,
        ];
        cache()->save('loginNodos_'.$dni, json_encode($arre), 60*60*1);
        
        $link = base_url() ."/front/inicioAlumnos/$idcolegio/$dni/$token";

        $data = ["status"=>1, "mensaje"=>$link];
        return $this->response->setJSON($data); 
    }

    public function generate_token_admin(){

        //$headers = apache_request_headers();
        $apiTokenRecibido = $this->request->getHeader('Apikey')->getValue();
        $userIdE   = $this->request->getHeader('Useridenc')->getValue();

        $userId    = $_POST['idusuario'];
        $dni       = $_POST['dni'];
        $idcolegio = $_POST['idcolegio'];
        $acciones = $_POST['acciones'];
        $userData = $_POST['userData'];

        $apiTokenReal = $this->variablesLib->get_apiToken();

        if($apiTokenReal != $apiTokenRecibido){
            $data = ["status"=>0, "mensaje"=>"No Autorizado"];
            echo json_encode($data);      
            die();   
        }

        $token = $this->randomPassword(50);

        //$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        cache()->delete('loginNodosAdmin_'.$dni);
        $arre = [
            'admin'     => true,
            'token'     => $token,
            'dni'       => $dni,
            'userId'    => $userId,
            'userIdE'   => $userIdE,
            'dni'       => $dni,
            'colegiosId' => $idcolegio,
            'acciones' => $acciones,
            'userData' => $userData,
        ];
        cache()->save('loginNodosAdmin_'.$dni, json_encode($arre), 60*60*1);

        $idcolegios = json_decode($idcolegio);
        //print_r($idcolegio); die();

        if(is_array($idcolegios))
            $primerCole = $idcolegios[0];
        else $primerCole = $idcolegios;
        
        $link = base_url() ."/dashboard/inicio/$primerCole/$dni/$token";

        $data = [
            "status"=>1, 
            "mensaje"=>$link, 
            //"colegiosJson"=>$idcolegio,
            //"colegiosArray"=>$idcolegios,
        ];
        return $this->response->setJSON($data); 
    }
	
}
