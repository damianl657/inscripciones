<?php

namespace App\Controllers;

use App\Libraries\Variables;

class Alumnos extends BaseController
{

    protected $tabla_preinscripcion;
    protected $archivos;
    protected $tabla_tutor_alumno;
    protected $tabla_nombre_div;
    protected $tabla_file;
    protected $tabla_alumno;
    protected $tabla_nombrediv;
    protected $tabla_colegio;

    public function __construct()
    {
        $this->tabla_preinscripcion = new \App\Models\PreinscripcionModel();
        $this->archivos = new \App\Models\FileModel();
        $this->tabla_tutor_alumno = new \App\Models\TutoralumnoModel();
        $this->tabla_alumno = new \App\Models\AlumnoModel();
        $this->tabla_tutor = new \App\Models\TutorModel();
        $this->tabla_nombre_div = new \App\Models\NombredivModel();
        $this->tabla_colegio = new \App\Models\ColegioModel();
    }

    public function index()
    {
        /*
        echo view("alumnos/header_alumnos");
        echo view("alumnos/ver_datos_alumnos");
        echo view("navbar_dashboard");
        */
    }

    public function alu($idpreinscripcion)
    {
        $datos["alumno"] = $this->get_info_alumno($idpreinscripcion);
        $this->inicio($idpreinscripcion, $datos);
    }

    public function postu($idpreinscripcion)
    {
        $datos["alumno"] = $this->get_info_postulante($idpreinscripcion);
        $datos["alumno"][0]["anio_actual"] = 'No ';
        $datos["alumno"][0]["division_actual"] = 'No posee';
        $datos["alumno"][0]["legajo"] = 'No posee';
        $this->inicio($idpreinscripcion, $datos);
    }

    public function inicio($idpreinscripcion, $datos)
    {
        $userData = $this->session->get("userData");
        $colegios = $this->session->get("datosColegios");
        /*
        if (!$this->session->get("logged_in") && !$this->session->get("admin")) {
            echo "no autorizado";
            die();
        }
*/
        $noc = new Variables();
        $datos["repositorio"] = $noc->get_url_storage();
        $datos["anio_nivel_reserva"] = $this->get_anio_nivel_reserva_alumno($idpreinscripcion);

        if ($datos["anio_nivel_reserva"][0]["nombre_nivel"] == 'SECUNDARIA CICLO BASICO' || $datos["anio_nivel_reserva"][0]["nombre_nivel"] == 'EDUCACIÓN SECUNDARIA CICLO ORIENTADO') {
            $datos["anio_nivel_reserva"][0]["nombre_nivel"] = 'SECUNDARIA';
        }
        //$datos["alumno"] = $this->get_info_alumno($idpreinscripcion);
        $datos["archivos"] = $this->get_api_archivos_alumno($idpreinscripcion);
        $datos["tutores"] = $this->get_info_tutor_de($datos["alumno"][0]["id"]);
        $datos["id_preinscripcion"] = $idpreinscripcion;
        $datos["colegios"] = $colegios;;
        // Modificar la siguiente linea cuando se suba a produccion  $datos["userName"] = $userData->nombre;

        $datos["userName"] = $userData;
        switch ($datos["alumno"][0]["aceptado"]) {
            case 0:
                $datos["estado_reserva"] = '<span class="badge bg-danger">Rechazado</span>';
              break;
            case 1:
                $datos["estado_reserva"] = '<span class="badge bg-warning">Pendiente</span>';
              break;
            case 2:
                $datos["estado_reserva"] = '<span class="badge bg-success">Completado 1</span>';
              break;
            case 3:
                $datos["estado_reserva"] = '<span class="badge bg-primary">Completado 2</span>';
              break;
          }
       
        if ($datos["alumno"][0]["division_actual"] == 'No posee') {
            $archivos_x_colegio_anio = $this->get_div_x_colegio_y_anios_postulante($datos["alumno"][0]["colegio_id"], $datos["alumno"][0]["anio_id"]);
        } elseif ($datos["anio_nivel_reserva"][0]["niveles_id"]==2) {
            $archivos_x_colegio_anio = $this->get_div_x_colegio_y_anios_alumno($datos["alumno"][0]["colegio_id"], 3);
        }
        else $archivos_x_colegio_anio = $this->get_div_x_colegio_y_anios_alumno($datos["alumno"][0]["colegio_id"], $datos["alumno"][0]["anio_id"]);
       // print_r($datos["alumno"][0]);die();
        $array_divs_nombres_f = array();
        for ($k = 0; $k < count($archivos_x_colegio_anio); $k++) {
            array_push($array_divs_nombres_f, $archivos_x_colegio_anio[$k]['nombre'] . '/' . $archivos_x_colegio_anio[$k]['nombre_mostrar']);
        }

        $datos["array_divs_nombres_f"] = $array_divs_nombres_f;
        echo view("alumnos/header_alumnos");
        echo view("alumnos/ver_datos_alumnos", $datos);
        echo view("navbar_dashboard");
        echo view("modales/modal_observacion");
        echo view("modales/modal_editar_alumno");
        echo view("modales/modal_editar_tutor");
        echo view("modales/modal_loading");
    }

    public function get_info_alumno($idpreinscripcion)
    {
        $query = $this->tabla_preinscripcion->get_datos_pre_alumno($idpreinscripcion);
        return $query->getResultArray();
    }

    public function get_info_postulante($idpreinscripcion)
    {
        $query = $this->tabla_preinscripcion->get_datos_pre_postulante($idpreinscripcion);
        return $query->getResultArray();
    }

    public function get_archivos_alumno($idpreinscripcion)
    {
        $query = $this->archivos->get_archivos_preinscripcion_alumno($idpreinscripcion);
        return $query->getResultArray();
    }
    public function get_info_tutor_de($idalumno)
    {
        $query = $this->tabla_alumno->get_info_padres($idalumno);
        $info_tutor = $query->getResultArray();
        return $info_tutor;
    }

    public function get_api_archivos_alumno($idpreinscripcion)
    {
        $array_id_archivos = array();
        $array_urls_nombres = array();
        $archivos = $this->get_archivos_alumno($idpreinscripcion);
        //print_r($archivos );
        //die();
        for ($i = 0; $i < count($archivos); $i++) {
            array_push($array_id_archivos, $archivos[$i]["api_archivos_id"]);
        }
        $comas = implode(",", $array_id_archivos);
        //print_r($comas);die();
        $variable_login = new Variables();
        $idusuario = $variable_login->get_userid();
        $urlApi = $variable_login->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
        $passApiKey = $variable_login->get_apikey();
        $url = $urlApi . "/upload/get_files_xids";
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey", "userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array('ids' => $comas, 'origen' => 'pre'),
            CURLOPT_SSL_VERIFYPEER => false,
        );
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //print_r($response);
        //die();
        $datos = json_decode($response, true);
        for ($i = 0; $i < $datos["cant_files"]; $i++) {
            for ($j = 0; $j < count($archivos); $j++) {
                if ($archivos[$j]["api_archivos_id"] == $datos["files"][$i]["id"])
                    array_push($array_urls_nombres, array($datos["files"][$i]["url"], $archivos[$j]["nombre"], $datos["files"][$i]["tipo"], $datos["files"][$i]["id"]));
            }
        }
        //print_r($array_urls_nombres);
        //die();
        return $array_urls_nombres;
    }

    public function editar_tutor()
    {
        //verifico que esté logueado como admin
        if (!$this->session->get("logged_in") && !$this->session->get("admin")) {
            return false;
        }

        $id = (int)$this->request->getPost("tutor_id");

        $datos = [
            'nombre'    => $this->request->getPost("nombre_tutor_edit"),
            'apellido'  => $this->request->getPost("apellido_tutor_edit"),
            'documento' => $this->request->getPost("documento_tutor_edit"),
            'vive'      => (int) $this->request->getPost("vive_tutor_edit"),
            'rol'       => (int) $this->request->getPost("rol_tutor_edit"),
            'correo'    => $this->request->getPost("correo_tutor_edit"),
            'telefono_laboral' => $this->request->getPost("telefono2_tutor_edit"),
            'telefono_particular' => $this->request->getPost("telefono1_tutor_edit"),
            'domicilio' => $this->request->getPost("domicilio_tutor_edit"),
            'ocupacion' => $this->request->getPost("ocupacion_tutor_edit"),
            'empresa'   => $this->request->getPost("empresa_tutor_edit"),
            'cargo'     => $this->request->getPost("cargo_tutor_edit")
        ];

        $respuesta = $this->tabla_tutor->actualizar_tutor($id, $datos);
        //echo $respuesta;
        $arre = [
            'estado' => $respuesta,
            'id' => $id,
            'datos' => $datos,
        ];
        echo json_encode($arre);
    }

    public function editar_observacion()
    {
        $obser = $this->request->getPost("txt_observacion");
        $idpre = $this->request->getPost("id_pre");

        $this->tabla_preinscripcion->modificar_obser($idpre, $obser);
        $respuesta = $this->tabla_preinscripcion->where('id', $idpre)->find();

        return json_encode($respuesta);
    }

    public function editar_observacion_tutor()
    {
        $obser = $this->request->getPost("txt_observacion");
        $idpre = $this->request->getPost("id_pre");

        $this->tabla_preinscripcion->modificar_obser_tutor($idpre, $obser);
        $respuesta = $this->tabla_preinscripcion->where('id', $idpre)->find();

        return json_encode($respuesta);
    }



    public function get_observaciones()
    {
        $id = $this->request->getPost("id");
        $respuesta = $this->tabla_preinscripcion->where('id', $id)->find();
        return json_encode($respuesta);
    }
    public function get_anio_nivel_reserva_alumno($idpreinscripcion)
    {
        $query = $this->tabla_preinscripcion->get_anio_reserva_alumno($idpreinscripcion);
        return $query->getResultArray();
    }


    public function asignarFileAlumno()
    {
        $id_file = $this->request->getPost("id_file");
        $id_preinscripcion = $this->request->getPost("id_preinscripcion");
        $div_file = $this->request->getPost("div_file");


        $result = $this->archivos->asignarFileAlumno($id_file, $id_preinscripcion, $div_file);

        $data['status'] = true;
        return json_encode($data);
    }

    public function delete_file()
    {
        $id_file = $this->request->getPost("id_file");
        $result = $this->archivos->delete_file($id_file);
        $data['status'] = true;
        return json_encode($data);
    }
    public function editar_alumno()
    {
        $alumno_id = $this->request->getPost("id_alumno_editar");
        $preinscripcion_id = $this->request->getPost("id_preinscripcion_editar");
        $datos_alumno["apellido"] = $this->request->getPost("apellido_alumno_editar");
        $datos_alumno["nombre"] = $this->request->getPost("nombre_alumno_editar");
        $datos_alumno["documento"] = $this->request->getPost("documento_alumno_editar");
        $datos_alumno["fecha_nac"] = $this->request->getPost("fecha_nac_alumno_editar");
        $datos_alumno["legajo"] = $this->request->getPost("legajo_alumno_editar");
        if ($this->request->getPost("sexo_alumno")) {
            $datos_alumno["sexo"] = $this->request->getPost("sexo_alumno");
        }
        $datos_alumno["domicilio"] = $this->request->getPost("domicilio_alumno_editar");
        $datos_alumno["telefono"] = $this->request->getPost("telefono_alumno_editar");;
        $datos_alumno["correo"] = $this->request->getPost("correo_alumno_editar");
        $datos_alumno["ciudad"] = $this->request->getPost("ciudad_alumno_editar");
        $datos_alumno["provincia_id"] = $this->request->getPost("provincia_alumno_editar");
        //$datos_alumno["nacionalidad_id"] = $this->request->getPost("nacionalidad_alumno_editar");
        if ($this->request->getPost("escuela_proviene_alumno")) {
            $datos_alumno["escuela_proviene"] = $this->request->getPost("escuela_proviene_alumno");
        }

        $datos_pre['aceptado'] = $this->request->getPost('estado_reserva_alumno_editar');
        $datos_pre['niveles_id'] = $this->request->getPost("reserva_nivel_alumno_editar");
        if ($datos_pre['niveles_id'] == 1) {
            $datos_pre['anio_id'] = $this->request->getPost("anio_alumno_editar");
            if ($datos_pre['anio_id'] > 3) {
                $datos_pre['niveles_id'] = 2;
            }
            ///////////////////////////////////
            $colegios_nivel = $this->tabla_colegio->get_colegios_x_nivel('Secundaria');
            $colegios_permisos = $this->session->get('colegiosId');
            $datos_pre['colegio_id'] = null;
            for ($i = 0; $i < count($colegios_permisos); $i++) {
                for ($j = 0; $j < count($colegios_nivel); $j++) {
                    if ($colegios_permisos[$i] == $colegios_nivel[$j]['id']) {
                        $datos_pre['colegio_id'] = $colegios_permisos[$i];
                    }
                }
            }
            //////////////////////////////
        } elseif ($datos_pre['niveles_id'] == 3) {
            $datos_pre['anio_id'] = $this->request->getPost("grado_alumno_editar");
            ///////////////////////////////////

            $colegios_nivel = $this->tabla_colegio->get_colegios_x_nivel('Primaria');
            $colegios_permisos = $this->session->get('colegiosId');
            $datos_pre['colegio_id'] = null;
            for ($i = 0; $i < count($colegios_permisos); $i++) {
                for ($j = 0; $j < count($colegios_nivel); $j++) {
                    if ($colegios_permisos[$i] == $colegios_nivel[$j]['id']) {
                        $datos_pre['colegio_id'] = $colegios_permisos[$i];
                    }
                }
            }
            //////////////////////////////
        } else {
            $datos_pre['anio_id'] = $this->request->getPost("sala_alumno_editar");
            ///////////////////////////////////
            $colegios_nivel = $this->tabla_colegio->get_colegios_x_nivel('Inicial');
            $colegios_permisos = $this->session->get('colegiosId');
            $datos_pre['colegio_id'] = null;
            for ($i = 0; $i < count($colegios_permisos); $i++) {
                for ($j = 0; $j < count($colegios_nivel); $j++) {
                    if ($colegios_permisos[$i] == $colegios_nivel[$j]['id']) {
                        $datos_pre['colegio_id'] = $colegios_permisos[$i];
                    }
                }
            }
            //////////////////////////////
        }
        if (is_numeric($alumno_id) && $alumno_id > 0 && is_numeric($preinscripcion_id) && $preinscripcion_id > 0) {
            //print_r($datos_alumno);
            //print_r($datos_pre);
            //die();
            $this->tabla_alumno->actualizar_alumno($alumno_id, $datos_alumno);
            $this->tabla_preinscripcion->modificar_preinscripcion($preinscripcion_id, $datos_pre);
            $data['status'] = true;
            $data['alumno'] = $datos_alumno;
            $data['pre'] = $datos_pre;
            return json_encode($data);
        } else return json_encode($data['status'] = false);
    }
    public function legajo()
    {
        //comentar todo lo de sesion antes de subir a produccion
       /* 
        $admin = false;
         //$session = session();
         $permisos[32] = ['admin_pagos', 'admin_sync', 'admin_abm'];
        $permisos[33] = ['admin_pagos', 'admin_sync', 'admin_abm'];
        $permisos[37] = ['admin_pagos', 'admin_sync', 'admin_abm'];
         $newdata = [
             //'email'    => 'johndoe@some-site.com',
             'dni'        => 28218739,
             'userId'     => 9, //este id (sin encriptar) no se si hace falta
             'userIdE'    => '1234564sdfsdf',
             'colegiosId' => [32, 33, 37],
             'permisos'   => $permisos,
             'logged_in'  => TRUE,
             'admin'      => $admin,
             'userdata'  => 'seba',
         ];
         $this->session->set($newdata);
//hasta aca comentar al subir a produccion
*/
        $hijos_preinscriptos = $this->tabla_tutor_alumno->get_hijos_x_tutor_con_preinscripcion($this->session->get("dni"));
        $id_reserva = $hijos_preinscriptos[0]['id_reserva'];
        $this->session->set(['hijos_tutor' => $hijos_preinscriptos]);
        //print_r($hijos_preinscriptos);die();
        $this->ver_datos_tutor_alumno($id_reserva);
    }

    public function ver_datos_tutor_alumno($id_reserva = null)
    {

        if (!$this->session->get("logged_in") && !$this->session->get("admin")) {
            echo "no autorizado";
            die();
        }


        $userData = $this->session->get("userData");

        $datos["alumno"] = $this->get_info_alumno($id_reserva);
        $noc = new Variables();
        $datos["repositorio"] = $noc->get_url_storage();
        $datos["anio_nivel_reserva"] = $this->get_anio_nivel_reserva_alumno($id_reserva);
        $datos["tutores"] = $this->get_info_tutor_de($datos["alumno"][0]["id"]);
        $datos["id_preinscripcion"] = $id_reserva;
        $datos["hijos_tutor"] = $this->session->get("hijos_tutor");
        if ($datos["alumno"][0]["estado_economico"] == 2) {
            $datos["archivos"] = $this->get_api_archivos_alumno($id_reserva);
            //print_r($datos["alumno"][0]["anio_actual"]);die();
            if ($datos["alumno"][0]["division_actual"] == 'No posee') {
                $archivos_x_colegio_anio = $this->get_div_x_colegio_y_anios_postulante($datos["alumno"][0]["colegio_id"], $datos["alumno"][0]["anio_id"]);
            } elseif ($datos["anio_nivel_reserva"][0]["niveles_id"]==2) {
                $archivos_x_colegio_anio = $this->get_div_x_colegio_y_anios_alumno($datos["alumno"][0]["colegio_id"], 3);
            }
            else $archivos_x_colegio_anio = $this->get_div_x_colegio_y_anios_alumno($datos["alumno"][0]["colegio_id"], $datos["alumno"][0]["anio_id"]);
           // print_r($datos["alumno"][0]);die();
            $array_divs_nombres_f = array();

            for ($k = 0; $k < count($archivos_x_colegio_anio); $k++) {
                array_push($array_divs_nombres_f, $archivos_x_colegio_anio[$k]['nombre'] . '/' . $archivos_x_colegio_anio[$k]['nombre_mostrar']);
            }
            $datos["array_divs_nombres_f"] = $array_divs_nombres_f;
            //print_r($array_divs_nombres_f);die();
            // print_r( $datos["archivos_x_colegio_anio"][0]['nombre_mostrar']);die();

            // Modificar la siguiente linea cuando se suba a produccion  $datos["userName"] = $userData->nombre;
            echo view("alumnos/header_alumnos");
            echo view("alumnos/ver_datos_alumnos_vista_tutor", $datos);

            echo view("modales/modal_observacion_tutor");
            //echo view("modales/modal_editar_alumno");
            //echo view("modales/modal_editar_tutor");
            echo view("modales/modal_loading");
            echo view("modales/modal_bloqueo_pantalla");
            echo view("navbar_vista_alumno_tutor");
        } else {
            echo view("alumnos/header_alumnos");
            echo view("alumnos/ver_datos_alumno_tutor_bloqueado", $datos);
            echo view("navbar_vista_alumno_tutor");
        }
    }

    public function get_archivos_x_colegio($idcolegio)
    {
        $query = $this->tabla_nombre_div->get_div_x_colegio($idcolegio);
        return $query->getResultArray();
    }

    public function get_div_x_colegio_y_anios_alumno($idcolegio, $anio)
    {
        $query = $this->tabla_nombre_div->get_div_x_colegio_y_anios_alumno($idcolegio, $anio);
        return $query->getResultArray();
    }
    public function get_div_x_colegio_y_anios_postulante($idcolegio, $anio)
    {
        $query = $this->tabla_nombre_div->get_div_x_colegio_y_anios_postulante($idcolegio, $anio);
        return $query->getResultArray();
    }
}
