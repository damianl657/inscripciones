<?php

namespace App\Controllers;


class CargarDatos extends BaseController
{
	protected $tabla_colegio;
	protected $header_dinamico;

	public function __construct()
	{
		$this->tabla_colegio = new \App\Models\ColegioModel();
	}

	public function index()
	{
	}

	public function inicio($idcolegio)
	{
		$colegio = $this->tabla_colegio->find($idcolegio);
		$this->header_dinamico["logo"] = $colegio["logo"];
		$this->header_dinamico["nombre"] = $colegio["nombre"];
		$this->header_dinamico["idcolegio"] = $colegio["id"];
		$this->header_dinamico["titulo_web"] = $colegio["titulo_web"];
		$this->header_dinamico["requisitos"] = $colegio["requisitos"];
		$ruta = "carga_datos/" . $colegio["ruta_datos"] . "/vista_datos_alumno_tutor_vacia";
		echo view("front/vista_inc_header", $this->header_dinamico);
		echo view($ruta);
		echo view("front/vista_inc_footer");
	}
}
