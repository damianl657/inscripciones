<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Datos extends BaseController
{
	protected $tabla_colegio;
	protected $header_dinamico;
	protected $tabla_alumno;
	protected $tabla_tutor;
	protected $tabla_tutor_alumno;
	protected $tabla_niveles;
	protected $tabla_preinscripcion;
	protected $tabla_file;
	protected $tabla_nombre_div;

	public function __construct()
	{
		$this->tabla_colegio = new \App\Models\ColegioModel();
		$this->tabla_alumno = new \App\Models\AlumnoModel();
		$this->tabla_tutor = new \App\Models\TutorModel();
		$this->tabla_tutor_alumno = new \App\Models\TutoralumnoModel();
		$this->tabla_niveles = new \App\Models\NivelesModel();
		$this->tabla_preinscripcion = new \App\Models\PreinscripcionModel();
		$this->tabla_file = new \App\Models\FileModel();
		$this->tabla_nombre_div = new \App\Models\NombredivModel();
	}

	public function index()
	{
	}

	public function verificar_alumno()
	{
		$dni = $this->request->getPost("dni");
		$tipo_inscrip = $this->request->getPost("tipo_inscrip");
		//print_r($dni);die();
		$alumno = $this->tabla_alumno->where('documento', $dni)->findAll();
		//print_r($alumno);die();
		if (!empty($alumno)) {
			if (!empty($this->tabla_tutor_alumno->get_dni_tutor($dni))) {
				$data['alumno'] = true;
				$data['tutor'] = true;
				$data['tipo']=$tipo_inscrip;
				//print_r($data);die();
				return json_encode($data);
			} else {
				$data['alumno'] = true;
				$data['tutor'] = false;
				$data['tipo']=$tipo_inscrip;
				return json_encode($data);
			}
		} else {
			$data['alumno'] = false;
			$data['tutor'] = false;
			$data['tipo']=$tipo_inscrip;
			return json_encode($data);
		}
	}

	public function preinscripcion_alumno_admin()
	{
		$idcolegio = $this->request->getPost("id_col");
		$dni = $this->request->getPost("documento_alumno_preinscribir");
		$tipo_insc = $this->request->getPost("select_tipo_alu");

		if ($tipo_insc == 1) {
			$this->inicio($idcolegio, $dni);
		} else {
			$dni_tutor = $this->tabla_tutor_alumno->get_dni_tutor($dni);
			$this->inicio_alumno($idcolegio, $dni, $dni_tutor);
		}
	}

	public function inicio($idcolegio, $dni = null)
	{
		$datos_alumno["documento"] = $this->request->getPost("documento");
		if (empty($datos_alumno["documento"])) {
			$datos_alumno["documento"] = $dni;
		}
		$colegio = $this->tabla_colegio->find($idcolegio);
		$this->header_dinamico["logo"] = $colegio["logo"];
		$this->header_dinamico["nombre"] = $colegio["nombre"];
		$this->header_dinamico["idcolegio"] = $colegio["id"];
		$this->header_dinamico["tipo"] = $colegio["tipo"];
		$this->header_dinamico["requisitos"] = $colegio["requisitos"];
		$ruta = "carga_datos/" . $colegio["ruta_datos"] . "/vista_datos_alumno_tutor_vacia";
		echo view("front/vista_header_datos_alumno_tutor", $this->header_dinamico);
		echo view($ruta, $datos_alumno);
		echo view("front/vista_inc_footer");
		echo view("funcionesfiles");
	}


	public function inicio_alumno($idcolegio, $dni = null, $dni_tutor = null)
	{
		$vista_alumno_tutor = "/vista_datos_alumno_tutor";
		$documento = $this->request->getPost("documento");
		$documento_tutor = $this->request->getPost("documento_tutor");
		if (empty($documento)) {
			//print_r($dni);die();
			$documento = $dni;
			$documento_tutor = $dni_tutor[0]['documento'];
		}

		$alumno = $this->tabla_alumno->where('documento', $documento)->findAll();
		$tutor1 = $this->tabla_tutor->where('documento', $documento_tutor)->findAll();
		$datos_alumno_tutor["alumno"] = $alumno[0];
		$datos_alumno_tutor["tutor1"] = $tutor1[0];
		$colegio = $this->tabla_colegio->find($idcolegio);
		$this->header_dinamico["logo"] = $colegio["logo"];
		$this->header_dinamico["nombre"] = $colegio["nombre"];
		$this->header_dinamico["idcolegio"] = $colegio["id"];
		$this->header_dinamico["tipo"] = $colegio["tipo"];
		$this->header_dinamico["requisitos"] = $colegio["requisitos"];
		if ($alumno[0]['anio_id'] == 6 && $alumno[0]['nodonivel_id'] == 86) {
			$vista_alumno_tutor = "/vista_datos_alumno_tutor_1_anio";
		}
		$ruta = "carga_datos/" . $colegio["ruta_datos"] . $vista_alumno_tutor;
		echo view("front/vista_header_datos_alumno_tutor", $this->header_dinamico);
		echo view($ruta, $datos_alumno_tutor);
		echo view("front/vista_inc_footer");
		echo view("funcionesfiles");
	}
	public function guardar_priamria_postulante($matriz_archivos)
	{
		$colegio_id = $this->request->getPost("colegio_id");
		$colegio = $this->tabla_colegio->find($colegio_id);
		$datos_alumno["apellido"] = $this->request->getPost("apellido_alumno");
		$datos_alumno["nombre"] = $this->request->getPost("nombre_alumno");
		$datos_alumno["documento"] = $this->request->getPost("documento_alumno");
		$datos_alumno["fecha_nac"] = $this->request->getPost("fecha_nacimiento_alumno");
		$datos_alumno["sexo"] = $this->request->getPost("sexo_alumno");
		$datos_alumno["domicilio"] = $this->request->getPost("domicilio_alumno");
		$datos_alumno["telefono"] = $this->request->getPost("telefono_alumno");;
		$datos_alumno["correo"] = $this->request->getPost("correo_alumno");
		$datos_alumno["ciudad"] = $this->request->getPost("ciudad_alumno");
		$datos_alumno["provincia_id"] = $this->request->getPost("provincia_alumno");
		$datos_alumno["nacionalidad_id"] = $this->request->getPost("nacionalidad_alumno");
		$datos_alumno["escuela_proviene"] = $this->request->getPost("escuela_proviene_alumno");

		$datos_alumno_tutor1["alumno_id"] = $this->tabla_alumno->insert($datos_alumno);

		$datos_tutor1["nombre"] = $this->request->getPost("nombre_tutor1");
		$datos_tutor1["apellido"] = $this->request->getPost("apellido_tutor1");
		$datos_tutor1["documento"] = $this->request->getPost("documento_tutor1");
		$datos_tutor1["vive"] = $this->request->getPost("vive_tutor1");
		$datos_tutor1["domicilio"] = $this->request->getPost("domicilio_tutor1");
		$datos_tutor1["telefono_particular"] = $this->request->getPost("tel_particular_tutor1");
		$datos_tutor1["telefono_laboral"] = $this->request->getPost("tel_laboral_tutor1");
		$datos_tutor1["rol"] = $this->request->getPost("relacion_tutor1");
		$datos_tutor1["correo"] = $this->request->getPost("correo_tutor1");
		$datos_tutor1["ocupacion"] = $this->request->getPost("ocupacion_tutor1");
		$datos_tutor1["empresa"] = $this->request->getPost("empresa_tutor1");
		$datos_tutor1["cargo"] = $this->request->getPost("cargo_tutor1");
		$datos_alumno_tutor1["tutor_id"] = $this->tabla_tutor->insert($datos_tutor1);
		$id_alumno_tutor1 = $this->tabla_tutor_alumno->insert($datos_alumno_tutor1);

		$posee_adulto2 = $this->request->getPost("posee_adulto2");
		if ($posee_adulto2 == true) {
			$datos_tutor2["nombre"] = $this->request->getPost("nombre_tutor2");
			$datos_tutor2["apellido"] = $this->request->getPost("apellido_tutor2");
			$datos_tutor2["documento"] = $this->request->getPost("documento_tutor2");
			$datos_tutor2["vive"] = $this->request->getPost("vive_tutor2");
			$datos_tutor2["domicilio"] = $this->request->getPost("domicilio_tutor2");
			$datos_tutor2["telefono_particular"] = $this->request->getPost("tel_particular_tutor2");
			$datos_tutor2["telefono_laboral"] = $this->request->getPost("tel_laboral_tutor2");
			$datos_tutor2["rol"] = $this->request->getPost("relacion_tutor2");
			$datos_tutor2["correo"] = $this->request->getPost("correo_tutor2");
			$datos_tutor2["ocupacion"] = $this->request->getPost("ocupacion_tutor2");
			$datos_tutor2["empresa"] = $this->request->getPost("empresa_tutor2");
			$datos_tutor2["cargo"] = $this->request->getPost("cargo_tutor2");
			$datos_alumno_tutor2["alumno_id"] = $datos_alumno_tutor1["alumno_id"];
			$datos_alumno_tutor2["tutor_id"] = $this->tabla_tutor->insert($datos_tutor2);
			$id_alumno_tutor2 = $this->tabla_tutor_alumno->insert($datos_alumno_tutor2);
		}
		$datos_preinscripcion["alumno_id"] = $datos_alumno_tutor1["alumno_id"];
		$datos_preinscripcion["colegio_id"] = $colegio_id;
		$datos_preinscripcion["anio_id"] = $this->request->getPost("anio_alumno");
		$nivel = $this->tabla_niveles->get_id_nivel($colegio_id);
		if ($nivel[0]["tipo"] == 'Secundaria') {
			if ($datos_preinscripcion["anio_id"] > 3) {
				$datos_preinscripcion["niveles_id"] = $nivel[1]["id"];
			} else $datos_preinscripcion["niveles_id"] = $nivel[0]["id"];
		} else $datos_preinscripcion["niveles_id"] = $nivel[0]["id"];
		//print_r($datos_preinscripcion);die();
		$numero_preinscripcion = $this->tabla_preinscripcion->insert($datos_preinscripcion);
		if ($numero_preinscripcion) {
			$archivos = json_decode($matriz_archivos);
			for ($i = 0; $i < count($archivos); $i++) {

				for ($j = 1; $j < count($archivos[$i]); $j++) {
					$datos_files["nombre_div_id"] = $archivos[$i][0];
					$datos_files["api_archivos_id"] = $archivos[$i][$j];
					$datos_files["preinscripcion_id"] = $numero_preinscripcion;
					$this->tabla_file->insert($datos_files);
				}
			}
			$data['status'] = true;
			$data['id'] = $numero_preinscripcion;
			return json_encode($data);
		} else return json_encode($data['status'] = false);
	}

	//Arreglar
	public function guardar_priamria($matriz_archivos)
	{
		$colegio_id = $this->request->getPost("colegio_id");
		$colegio = $this->tabla_colegio->find($colegio_id);
		$alumno_id = $this->request->getPost("alumno_id");
		$datos_alumno["apellido"] = $this->request->getPost("apellido_alumno");
		$datos_alumno["nombre"] = $this->request->getPost("nombre_alumno");
		$datos_alumno["documento"] = $this->request->getPost("documento_alumno");
		$datos_alumno["fecha_nac"] = $this->request->getPost("fecha_nacimiento_alumno");
		$datos_alumno["sexo"] = $this->request->getPost("sexo_alumno");
		$datos_alumno["domicilio"] = $this->request->getPost("domicilio_alumno");
		$datos_alumno["telefono"] = $this->request->getPost("telefono_alumno");;
		$datos_alumno["correo"] = $this->request->getPost("correo_alumno");
		$datos_alumno["ciudad"] = $this->request->getPost("ciudad_alumno");
		$datos_alumno["provincia_id"] = $this->request->getPost("provincia_alumno");
		$datos_alumno["nacionalidad_id"] = $this->request->getPost("nacionalidad_alumno");

		if (is_numeric($alumno_id) && $alumno_id > 0) {
			$this->tabla_alumno->actualizar_alumno($alumno_id, $datos_alumno);
		} else return json_encode($data['status'] = false);

		//print_r($datos_alumno);
		$tutor1_id = $this->request->getPost("tutor1_id");
		$datos_tutor1["nombre"] = $this->request->getPost("nombre_tutor1");
		$datos_tutor1["apellido"] = $this->request->getPost("apellido_tutor1");
		$datos_tutor1["documento"] = $this->request->getPost("documento_tutor1");
		$datos_tutor1["vive"] = $this->request->getPost("vive_tutor1");
		$datos_tutor1["domicilio"] = $this->request->getPost("domicilio_tutor1");
		$datos_tutor1["telefono_particular"] = $this->request->getPost("tel_particular_tutor1");
		$datos_tutor1["telefono_laboral"] = $this->request->getPost("tel_laboral_tutor1");
		$datos_tutor1["rol"] = $this->request->getPost("relacion_tutor1");
		$datos_tutor1["correo"] = $this->request->getPost("correo_tutor1");
		$datos_tutor1["ocupacion"] = $this->request->getPost("ocupacion_tutor1");
		$datos_tutor1["empresa"] = $this->request->getPost("empresa_tutor1");
		$datos_tutor1["cargo"] = $this->request->getPost("cargo_tutor1");

		if (is_numeric($tutor1_id) && $tutor1_id > 0) {
			$this->tabla_tutor->actualizar_tutor($tutor1_id, $datos_tutor1);
		} else return json_encode($data['status'] = false);

		$posee_adulto2 = $this->request->getPost("posee_adulto2");
		if ($posee_adulto2 == true) {
			$datos_tutor2["nombre"] = $this->request->getPost("nombre_tutor2");
			$datos_tutor2["apellido"] = $this->request->getPost("apellido_tutor2");
			$datos_tutor2["documento"] = $this->request->getPost("documento_tutor2");
			$datos_tutor2["vive"] = $this->request->getPost("vive_tutor2");
			$datos_tutor2["domicilio"] = $this->request->getPost("domicilio_tutor2");
			$datos_tutor2["telefono_particular"] = $this->request->getPost("tel_particular_tutor2");
			$datos_tutor2["telefono_laboral"] = $this->request->getPost("tel_laboral_tutor2");
			$datos_tutor2["rol"] = $this->request->getPost("relacion_tutor2");
			$datos_tutor2["correo"] = $this->request->getPost("correo_tutor2");
			$datos_tutor2["ocupacion"] = $this->request->getPost("ocupacion_tutor2");
			$datos_tutor2["empresa"] = $this->request->getPost("empresa_tutor2");
			$datos_tutor2["cargo"] = $this->request->getPost("cargo_tutor2");
			$datos_tutor2["tutor2_id"] = $this->request->getPost("cargo_tutor2");
			$tutor2 = $this->tabla_tutor->where('documento', $datos_tutor2["documento"])->findAll();
			if ($tutor2) {
				if (is_numeric($tutor1_id) && $tutor1_id > 0) {
					$this->tabla_tutor->actualizar_tutor($tutor2[0]['id'], $datos_tutor2);
				} else return json_encode($data['status'] = false);
			} else {
				$datos_alumno_tutor2["tutor_id"]  = $this->tabla_tutor_alumno->insert($datos_tutor2);
				$datos_alumno_tutor2["alumno_id"] = $alumno_id;
				$id_alumno_tutor2 = $this->tabla_tutor_alumno->insert($datos_alumno_tutor2);
			}
		}
		$datos_preinscripcion["alumno_id"] = $alumno_id;
		$datos_preinscripcion["colegio_id"] = $colegio_id;
		$datos_preinscripcion["anio_id"] = $this->request->getPost("anio_alumno");
		$nivel = $this->tabla_niveles->get_id_nivel($colegio_id);
		if ($nivel[0]["tipo"] == 'Secundaria') {
			if ($datos_preinscripcion["anio_id"] > 3) {
				$datos_preinscripcion["niveles_id"] = $nivel[1]["id"];
			} else $datos_preinscripcion["niveles_id"] = $nivel[0]["id"];
		} else $datos_preinscripcion["niveles_id"] = $nivel[0]["id"];
		//print_r($datos_preinscripcion);die();
		$datos_preinscripcion["tutor_id"] = $tutor1_id;
		//print_r($datos_preinscripcion);
		//die();
		$numero_preinscripcion = $this->tabla_preinscripcion->insert($datos_preinscripcion);
		if ($numero_preinscripcion) {
			$archivos = json_decode($matriz_archivos);
			for ($i = 0; $i < count($archivos); $i++) {

				for ($j = 1; $j < count($archivos[$i]); $j++) {
					$datos_files["nombre_div_id"] = $archivos[$i][0];
					$datos_files["api_archivos_id"] = $archivos[$i][$j];
					$datos_files["preinscripcion_id"] = $numero_preinscripcion;
					$this->tabla_file->insert($datos_files);
				}
			}
			$data['status'] = true;
			$data['id'] = $numero_preinscripcion;
			return json_encode($data);
		} else return json_encode($data['status'] = false);
	}

	public function buscar_nombre_div_archivos()
	{
		$datos_div = $this->request->getPost("nombre");
		$nombre_div = $this->tabla_nombre_div->where('nombre', $datos_div)->findAll();
		//print($nombre_div);
		if ($nombre_div) {
			$data['id'] = $nombre_div[0]['id'];
			$data['status'] = true;
			return json_encode($data);
		} else return json_encode($data['status'] = false);
	}

	public function inicio_alumno_administrativo($idcolegio)
	{
		$vista_alumno_tutor = "/vista_datos_alumno_tutor";
		$documento = $this->request->getPost("documento");
		$documento_tutor = $this->request->getPost("documento_tutor");
		$alumno = $this->tabla_alumno->where('documento', $documento)->findAll();
		$tutor1 = $this->tabla_tutor->where('documento', $documento_tutor)->findAll();
		$datos_alumno_tutor["alumno"] = $alumno[0];
		$datos_alumno_tutor["tutor1"] = $tutor1[0];
		$colegio = $this->tabla_colegio->find($idcolegio);
		$this->header_dinamico["logo"] = $colegio["logo"];
		$this->header_dinamico["nombre"] = $colegio["nombre"];
		$this->header_dinamico["idcolegio"] = $colegio["id"];
		$this->header_dinamico["tipo"] = $colegio["tipo"];
		$this->header_dinamico["requisitos"] = $colegio["requisitos"];
		if ($alumno[0]['anio_id'] == 6 && $alumno[0]['nodonivel_id'] == 86) {
			$vista_alumno_tutor = "/vista_datos_alumno_tutor_1_anio";
		}
		$ruta = "carga_datos/" . $colegio["ruta_datos"] . $vista_alumno_tutor;
		echo view("front/vista_header_datos_alumno_tutor", $this->header_dinamico);
		echo view($ruta, $datos_alumno_tutor);
		echo view("front/vista_inc_footer");
		echo view("funcionesfiles");
	}
}
