<?php

namespace App\Controllers;

class Nacionalidad extends BaseController
{
	protected $tabla_nacionalidad;


	public function __construct()
	{
		$this->tabla_nacionalidad = new \App\Models\NacionalidadModel();
	}

	public function index()
	{
	}

	public function autocompletar_nacionalidad()
	{
        $resultado = $this->tabla_nacionalidad->get_tabla_nacionalidad();
        foreach ($resultado as $row) {
            $arr_result[] = $row;
        }
        echo json_encode($arr_result);
		
	}
}

