<?php

namespace App\Controllers;

use App\Libraries\Variables;

class Dashboard extends BaseController
{

    protected $datos_preinscripcion;
    protected $datos_dashboard;
    protected $tabla_colegio;
    protected $tabla_alumnos;

    public function __construct()
    {
        $this->datos_preinscripcion = new \App\Models\PreinscripcionModel();
        $this->datos_colegio = new \App\Models\ColegioModel();
        $this->datos_anios = new \App\Models\AniosModel();
        $this->datos_nivel = new \App\Models\NivelesModel();
        $this->datos_dashboard = new \App\Models\DashboardModel();
        $this->tabla_colegio = new \App\Models\ColegioModel();
        $this->tabla_alumnos = new \App\Models\AlumnoModel();
    }

    public function index()
    {
        /*
        if (!$this->session->get("logged_in") && !$this->session->get("admin")) {
            echo "no autorizado";
            die();
        }
        */
    }

    protected function checkLogin()
    {
        if ($this->session->get("logged_in") && $this->session->get("admin"))
            return true;
        else return false;
    }

    protected function getInfoColegios($colegiosArray)
    {
        if (is_array($colegiosArray))
            $colegiosString = join(',', $colegiosArray);
        else $colegiosString = $colegiosArray;

        $colegios = $this->datos_colegio->getColegiosByIds($colegiosString);

        //BUSCAMOS COLEGIOS
        foreach ($colegios as &$cole) {
            //echo $cole['id'].'--'.$cole['nombre'];
            $id = $cole['id'];
            $planestudio = $this->datos_colegio->obtener_planestudio($cole['planestudio']);
            $cicloa = $planestudio['id'];
            //$cole["total"] = $this->info_reserva_alumno($id);

            //BUSCAMOS NIVELES
            $niveles = $this->datos_nivel->getNivelesSinEspByColegiosIds($id, $cicloa);
            $nivelesAux = array();
            foreach ($niveles as &$nivel) {
                $nivelId = $nivel['id'];

                if (isset($nivel['especialidadName']))
                    //$nivelName = $nivel['nivelName'].' '.$nivel['especialidadName'];
                    $nivelName = $nivel['especialidadName'];
                else {
                    if (!!$nivel['nivelNameAbrev'])
                        $nivelName = $nivel['nivelNameAbrev'];
                    else $nivelName = $nivel['nivelName'];
                }

                $anios = $this->datos_anios->getAniosByNivelId($nivelId, $cicloa);
                $aniosAux = array();
                foreach ($anios as &$anio) {
                    $aux2 = [
                        'id'     => $anio['anio_id'],
                        'nombre' => $anio['anioName'],
                    ];
                    $aniosAux[] = $aux2;
                }

                $aux = [
                    'id'     => $nivelId,
                    'nombre' => $nivelName,
                    'anios' => $aniosAux,
                ];
                $nivelesAux[] = $aux;
            }

            $cole["niveles"] = $nivelesAux;
        }

        return $colegios;
    }

    public function inicio($idcolegio = null, $dni = null, $token = null)
    {
        //chequeamos login desde nodos
        $nodosLogin = $this->checkLoginNodos($dni, $token);
        if (!!$nodosLogin) {
            //creamos la session de user
            $this->createSession($nodosLogin, true);
        }

        $this->createSessionPruebas($nodosLogin, true); //COMENTAR ESTA LINEA (solo para pruebas)

        //verifico que esté logueado como admin
        if (!$this->checkLogin()) {
            echo "No Autorizado"; //redirect a mensaje de error
            die();
        }

        $colegiosArray = $this->session->get("colegiosId");

        $colegios = $this->getInfoColegios($colegiosArray);

        $this->session->set(['datosColegios' => $colegios]);

        $datos["colegios"] = $colegios;

        $idcolegio = $colegios[0]['id']; //por defecto el primero, 
        $idnivel   = $colegios[0]['niveles'][0]['id'];
        // print_r($datos);die();
        $this->principal($idcolegio, $idnivel);
    }


    public function principal($idcolegio, $idnivel, $idanio = null)
    {
        //verifico que esté logueado como admin
        if (!$this->checkLogin()) {
            echo "No Autorizado"; //redirect a mensaje de error
            die();
        }

        $colegios = $this->session->get("datosColegios");
        $userData = $this->session->get("userData");


        if (!empty($idanio)) {
            $datos["id_anio"] = $idanio;
        } else $datos["id_anio"] = 0;

        $datos["aceptados"] = ($this->datos_dashboard->total_reserva_estado($idcolegio, $estado = 2))->total;
        $datos["pendientes"] = ($this->datos_dashboard->total_reserva_estado($idcolegio, $estado = 1))->total;
        $datos["rechazados"] = ($this->datos_dashboard->total_reserva_estado($idcolegio, $estado = 0))->total;
        $datos["total"] = ($this->datos_dashboard->total_reserva($idcolegio))->total;
        $datos["colegio"] = $idcolegio;
        $datos["colegios"] = $colegios;
        $datos["userName"] = $userData;

        for ($i = 0; $i < count($colegios); $i++) {
            for ($j = 0; $j < count($colegios[$i]['niveles']); $j++) {
                if ($colegios[$i]['niveles'][$j]['id'] == $idnivel) {
                    $datos['nombre_nivel'] = $colegios[$i]['niveles'][$j]['nombre'];
                }
            }
        }

        $id_colegios_permisos = $this->session->get("colegiosId");
        $permisos         = $this->session->get("permisos");
        for ($i = 0; $i < count($id_colegios_permisos); $i++) {

            if (is_array($permisos[$id_colegios_permisos[$i]])) {
                if (in_array("admin_sync", $permisos[$id_colegios_permisos[$i]])) {
                    $permiso_sync0[$id_colegios_permisos[$i]] = '<li class="nav-item"><a href="#" class="nav-link" id="sinc" onclick="sincronizar(';
                    $permiso_sync1[$id_colegios_permisos[$i]] = ');"><i class="fas fa-sync nav-icon"></i><p>Sincronizar alumnos</p></a></li>';
                }
                if (in_array("admin_abm", $permisos[$id_colegios_permisos[$i]])) {
                    $permiso_abm0[$id_colegios_permisos[$i]] = '<li class="nav-item"><a href="#" class="nav-link" id="pre_inscrip" onclick="cargar_modal(';
                    $permiso_abm1[$id_colegios_permisos[$i]] = ');" data-toggle="modal" data-target="#reserva_alumno"><i class="fa fa-plus nav-icon"></i><p>Generar preinscripci&oacuten</p></a></li>';
                }
            }
        }

        //TODO: tratar los permisos por colegios separados; si lo tiene para un colegio, no implica que lo tenga para todos.
        $datos['sync0'] = $permiso_sync0;
        $datos['sync1'] = $permiso_sync1;
        $datos['abm0'] = $permiso_abm0;
        $datos['abm1'] = $permiso_abm1;

        echo view("dashboard/header");
        echo view("navbar_dashboard_super_admin", $datos);
        echo view("dashboard/vista_dashboard", $datos);
        echo view("modales/estado_economico");
        echo view("modales/modal_loading");
        echo view("modales/modal_reserva_alumno");
    }

    /**
     * createSession
     *
     * @param  mixed $datos
     * @return void
     */
    protected function createSession($datos, $admin = false)
    {
        $this->stopSession();
        //$session = session();
        $newdata = [
            //'email'    => 'johndoe@some-site.com',
            'dni'        => $datos['dni'],
            'userId'     => $datos['userId'], //este id (sin encriptar) no se si hace falta
            'userIdE'    => $datos['userIdE'],
            'colegiosId' => json_decode($datos['colegiosId']),
            'permisos'   => json_decode($datos['acciones'], true),
            'userData'   => json_decode($datos['userData']),
            'logged_in'  => TRUE,
            'admin'      => $admin,
            'userData' => 'Pepito',
        ];
        $this->session->set($newdata);
    }

    protected function createSessionPruebas($datos, $admin = false)
    {
        //$session = session();
        /*
        $permisos[32] = ['admin_pagos'];
        $permisos[33] = ['admin_doc'];
        $permisos[37] = ['admin_pagos'];
        $permisos[37] = ['admin_doc'];
        $permisos[0]=['admin_inscripcion'];
        */
        $permisos[32] = ['admin_pagos', 'admin_sync', 'admin_abm'];
        $permisos[33] = ['admin_pagos', 'admin_sync', 'admin_abm'];
        $permisos[37] = ['admin_pagos', 'admin_sync', 'admin_abm'];
        $newdata = [
            //'email'    => 'johndoe@some-site.com',
            'dni'        => 32689887,
            'userId'     => 9, //este id (sin encriptar) no se si hace falta
            'userIdE'    => '1234564sdfsdf',
            'colegiosId' => [32, 33, 37],
            'permisos'   => $permisos,
            'logged_in'  => TRUE,
            'admin'      => $admin,
            'userdata'  => 'seba',
        ];
        $this->session->set($newdata);
    }

    /**
     * checkLoginNodos
     *
     * @param  mixed $dniNodos
     * @param  mixed $tokenNodos
     * @return void
     */
    protected function checkLoginNodos($dniNodos, $tokenNodos)
    {

        if (cache()->get('loginNodosAdmin_' . $dniNodos)) {

            $dataNodosCache = cache()->get('loginNodosAdmin_' . $dniNodos);
            $dataNodosCache = json_decode($dataNodosCache, true);

            if ($dataNodosCache['token'] === $tokenNodos) {

                //NO VERIFICAMOS USER
                //$respuesta = $this->user->comprueba_dni($dniNodos);

                //borramos caché, por seguridad y ademas ya no sirve
                cache()->delete('loginNodosAdmin_' . $dniNodos);

                return $dataNodosCache;
            }
            return false;
        } else return false;
    }

    protected function destroySession()
    {
        $this->session->destroy();
    }

    protected function stopSession()
    {
        $this->session->stop();
    }

    public function cargar_tabla_reserva_alumnos($idcolegio, $anio)
    {
        //verifica que permisos especiales tiene dentro del colegio
        $permisos         = $this->session->get("permisos");
        $permisosEsteCole = $permisos[$idcolegio];
        if (!!$permisosEsteCole && in_array("admin_pagos", $permisosEsteCole)) {
            $permiso = 1;
        } else $permiso = 0;

        if ($anio == 0) {
            $query = $this->datos_dashboard->get_datos_preinscripcion_alumnos($idcolegio);
        } elseif ($anio == 10) {
            $anio = "'Sala de 4'";
            // print_r($idcolegio);die();
            $query = $this->datos_dashboard->get_datos_preinscripcion_alumnos_x_anio($idcolegio, $anio);
            //print_r($query);die();
        } elseif ($anio == 11) {
            $anio = "'Sala de 5'";
            $query = $this->datos_dashboard->get_datos_preinscripcion_alumnos_x_anio($idcolegio, $anio);
        } else  $query = $this->datos_dashboard->get_datos_preinscripcion_alumnos_x_anio($idcolegio, $anio);

        $data = array();


        foreach ($query->getResult() as $row) {
            switch ($row->aceptado) {
                case 0:
                    $row->aceptado = '<span class="badge bg-danger">Rechazado</span>';
                    break;
                case 1:
                    $row->aceptado = '<span class="badge bg-warning">Pendiente</span>';
                    break;
                case 2:
                    $row->aceptado = '<span class="badge bg-success">Completado 2</span>';
                    break;
                case 3:
                    $row->aceptado = '<span class="badge bg-primary">Completado 1</span>';
                    break;
            }

            //$eco = $row->estado_economico;
            if ($row->estado_economico == '1') {
                $row->estado_economico = '<span class="badge bg-warning">Pendiente</span>';
            } else $row->estado_economico = '<span class="badge bg-success">Aprobado</span>';

            if ($permiso == 0) {
                $accion = "<button type='button' id='botonEditar' name='editar' class='editar btn btn-primary btn-sm ml-auto'><span class='far fa-edit'></span></button>";
            } else $accion = "<button type='button' id='botonEditar' name='editar' class='editar btn btn-primary btn-sm ml-auto'><span class='far fa-edit'></span></button>\n
            <button type='button' id='botonpago' name='pago' class='pago btn btn-info btn-sm ml-auto'><i class='fas fa-dollar-sign'></i></button>";
            $data[] = array(
                $row->id,
                $row->legajo,
                $row->documento,
                $row->apellido,
                $row->nombre,
                $row->anio_inscripto,
                $row->fecha,
                $row->observacion,
                $row->aceptado,
                $row->estado_economico,
                //$eco,
                $accion,
            );
        }
        //print_r($data);
        $output = array(
            "data" => $data
        );
        return json_encode($output);
        // echo json_encode($output);
        // exit();
    }

    public function cargar_tabla_reserva_postulantes($idcolegio, $anio)
    {
        $permisos         = $this->session->get("permisos");
        $permisosEsteCole = $permisos[$idcolegio];
        if (!!$permisosEsteCole && in_array("admin_pagos", $permisosEsteCole)) {
            $permiso = 1;
        } else $permiso = 0;
        if ($anio == 0) {
            $query = $this->datos_dashboard->get_datos_preinscripcion_postulantes($idcolegio);
        } elseif ($anio == 10) {
            $anio = "'Sala de 4'";
            $query = $this->datos_dashboard->get_datos_preinscripcion_postulantes_x_anio($idcolegio, $anio);
        } elseif ($anio == 11) {
            $anio = "'Sala de 5'";
            $query = $this->datos_dashboard->get_datos_preinscripcion_postulantes_x_anio($idcolegio, $anio);
        } else  $query = $this->datos_dashboard->get_datos_preinscripcion_postulantes_x_anio($idcolegio, $anio);
        $data = array();

        foreach ($query->getResult() as $row) {
            switch ($row->aceptado) {
                case 0:
                    $row->aceptado = '<span class="badge bg-danger">Rechazado</span>';
                    break;
                case 1:
                    $row->aceptado = '<span class="badge bg-warning">Pendiente</span>';
                    break;
                case 2:
                    $row->aceptado = '<span class="badge bg-success">Completado 1</span>';
                    break;
                case 3:
                    $row->aceptado = '<span class="badge bg-primary">Completado 2</span>';
                    break;
            }

            // $eco = $row->estado_economico;
            if ($row->estado_economico == '1') {
                $row->estado_economico = '<span class="badge bg-warning">Pendiente</span>';
            } else $row->estado_economico = '<span class="badge bg-success">Aprobado</span>';

            if ($permiso == 0) {
                $accion = "<button type='button' id='botonEditar' name='editar' class='editar btn btn-primary btn-sm ml-auto'><span class='far fa-edit'></span></button>";
            } else $accion = "<button type='button' id='botonEditar' name='editar' class='editar btn btn-primary btn-sm ml-auto'><span class='far fa-edit'></span></button>\n
            <button type='button' id='botonpago' name='pago' class='pago btn btn-info btn-sm ml-auto'><i class='fas fa-dollar-sign'></i></button>";

            $data[] = array(
                $row->id,
                $row->documento,
                $row->apellido,
                $row->nombre,
                $row->escuela_proviene,
                $row->anio_inscripto,
                $row->fecha,
                $row->observacion,
                $row->aceptado,
                $row->estado_economico,
                // $eco,
                $accion
            );
        }


        $output = array(
            "data" => $data
        );
        return json_encode($output);
        // echo json_encode($output);
        //exit();
    }

    public function editar_estado_economico()
    {
        $preinscripcion_id = $this->request->getPost("id_pre");
        $datos_pre['estado_economico'] = $this->request->getPost("select_economico");
        //print_r($datos_pre);die();
        $this->datos_preinscripcion->modificar_preinscripcion($preinscripcion_id, $datos_pre);
        return json_encode($datos_pre);
    }

    public function sincronizar_alumnos()
    {
        $colegio = $this->request->getPost("idcolegio");
        $query = $this->datos_preinscripcion->get_info_preinscripcion_sincronizar_alumnos($colegio);
        $datos = array();
        $inscripcion = array();
        $alumno = array();
        $control_alumnos = array();
        $data = array();
        if ($query) {
            $migracion = $query->getResult();

            for ($j = 0; $j < count($migracion); $j++) {
                $inscripcion['anio'] = $migracion[$j]->anio_id;
                $inscripcion['nivel'] = $migracion[$j]->niveles_id;
                $inscripcion['espec'] = null;
                $inscripcion['division'] = $migracion[$j]->nombre_division;
                $alumno['apellido'] = $migracion[$j]->alu_apellido;
                $alumno['nombre'] = $migracion[$j]->alu_nombre;
                $alumno['sexo'] = $migracion[$j]->alu_sexo;
                $alumno['dni'] = $migracion[$j]->alu_documento;
                array_push($control_alumnos, $migracion[$j]->alumno_id);
                //$control_alumnos['id'] = $migracion[$j]->alumno_id;
                $alumno['legajo'] = $migracion[$j]->legajo;
                $alumno['email'] = $migracion[$j]->alu_correo;
                $alumno['domicilio'] = $migracion[$j]->alu_domicilio;
                $alumno['telefono'] = $migracion[$j]->alu_telefono;
                $alumno['fechanac'] = $migracion[$j]->fecha_nac;

                $arre_tutores = $this->tabla_alumnos->get_info_tutores_para_sincronizar($migracion[$j]->alumno_id);
                if (!empty($arre_tutores)) {
                    for ($i = 0; $i < count($arre_tutores); $i++) {
                        if ($arre_tutores[$i]->sexo == 1) {
                            $arre_tutores[$i]->sexo = 'M';
                        } else $arre_tutores[$i]->sexo = 'F';
                        $arre_tutores[$i] = json_decode(json_encode($arre_tutores[$i]), true);
                    }
                }
                if (count($arre_tutores) == 2) {
                    $datos = array('inscripcion' => $inscripcion, 'alumno' => $alumno, 'tutor1' => $arre_tutores[0], 'tutor2' => $arre_tutores[1]);
                } else  $datos = array('inscripcion' => $inscripcion, 'alumno' => $alumno, 'tutor1' => $arre_tutores[0], 'tutor2' => null);

                array_push($data, $datos);
            }

            $datos_enviar = array('colegio' => $colegio, 'cicloa' => '2021', 'data' => $data);
            //echo "SOLICITUD:";
            //print_r($datos_enviar); 
            //print_r($control_alumnos);
           // die();
        }
        
        $envio = json_encode($datos_enviar);

        $variable = new Variables();
        $inscripcioneskey = $variable->get_apiToken();
        $urlApi = $variable->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
        $passApiKey = $variable->get_apikey();
        //$url = "http://andres.api.ngrok.io/nodos/nodosapi/index.php/api/v1/InscripcionesApp/migrar_usuarios";
        $url = $urlApi . "/InscripcionesApp/migrar_usuarios";
        //die();
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey", "inscripKey: $inscripcioneskey"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array('datos' => $envio),
            CURLOPT_SSL_VERIFYPEER => false,
        );
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        
        //echo "RESPUESTA:";
        //print_r($response);
        //die();
        $respuesta = json_decode($response);
        //print_r($respuesta->status);

        if ($respuesta->status == 1) {
            for ($j = 0; $j < count($control_alumnos); $j++) {
                $this->tabla_alumnos->actualizar_if_nodos($control_alumnos[$j]);
            }
            if ($respuesta->errosDni != 0) {
                for ($j = 0; $j < count($respuesta->errosDni); $j++) {
                    $this->tabla_alumnos->actualizar_no_if_nodos($respuesta->errosDni[$j]);
                }
            }
        } else {
            if ($respuesta->errosDni != 0) {
                for ($j = 0; $j < count($respuesta->errosDni); $j++) {
                    $this->tabla_alumnos->actualizar_no_if_nodos($respuesta->errosDni[$j]);
                }
            }
        }
        return $respuesta->status;
      
    }

    public function inscribir_alumnos()
    {
        //verifico que esté logueado como admin
        if (!$this->checkLogin()) {
            echo "No Autorizado"; //redirect a mensaje de error
            die();
        }

        $colegios = $this->session->get("datosColegios");
        $userData = $this->session->get("userData");

        print_r($colegios);
        die();
        // if (!empty($idanio)) {
        //     $datos["id_anio"] = $idanio;
        //  } else $datos["id_anio"] = 0;

        // $datos["aceptados"] = ($this->datos_dashboard->total_reserva_estado($idcolegio, $estado = 2))->total;
        //$datos["pendientes"] = ($this->datos_dashboard->total_reserva_estado($idcolegio, $estado = 1))->total;
        //$datos["rechazados"] = ($this->datos_dashboard->total_reserva_estado($idcolegio, $estado = 0))->total;
        // $datos["total"] = ($this->datos_dashboard->total_reserva($idcolegio))->total;
        $datos["colegio"] = $idcolegio;
        $datos["colegios"] = $colegios;
        $datos["userName"] = $userData;

        for ($i = 0; $i < count($colegios); $i++) {
            for ($j = 0; $j < count($colegios[$i]['niveles']); $j++) {
                if ($colegios[$i]['niveles'][$j]['id'] == $idnivel) {
                    $datos['nombre_nivel'] = $colegios[$i]['niveles'][$j]['nombre'];
                }
            }
        }

        $id_colegios_permisos = $this->session->get("colegiosId");
        $permisos         = $this->session->get("permisos");
        /*
        for ($i = 0; $i < count($id_colegios_permisos); $i++) {
            if (in_array("admin_sync", $permisos[$id_colegios_permisos[$i]])) {
                $permiso_sync0[$id_colegios_permisos[$i]] = '<li class="nav-item"><a href="#" class="nav-link" id="sinc" onclick="sincronizar(';
                $permiso_sync1[$id_colegios_permisos[$i]] = ');"><i class="fas fa-sync nav-icon"></i><p>Sincronizar alumnos</p></a></li>';
            }
            if (in_array("admin_abm", $permisos[$id_colegios_permisos[$i]])) {
                $permiso_abm0[$id_colegios_permisos[$i]] = '<li class="nav-item"><a href="#" class="nav-link" id="pre_inscrip" onclick="cargar_modal(';
                $permiso_abm1[$id_colegios_permisos[$i]] = ');" data-toggle="modal" data-target="#reserva_alumno"><i class="fa fa-plus nav-icon"></i><p>Generar preinscripci&oacuten</p></a></li>';
            }
        }
        */
        //  $datos['sync0'] = $permiso_sync0;
        //  $datos['sync1'] = $permiso_sync1;
        // $datos['abm0'] = $permiso_abm0;
        // $datos['abm1'] = $permiso_abm1;

        echo view("dashboard/header");
        echo view("navbar_dashboard_super_admin", $datos);
        echo view("dashboard/vista_dashboard_inscribir", $datos);
        // echo view("modales/estado_economico");
        echo view("modales/modal_loading");
        //echo view("modales/modal_reserva_alumno");
    }
}
