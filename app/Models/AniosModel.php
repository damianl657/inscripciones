<?php namespace App\Models;

use CodeIgniter\Model;

class AniosModel extends Model
{
    protected $table      = 'anios';
    protected $primaryKey = 'id';

    protected $allowedFields = ['orden','nombre',];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
    }

    public function getAniosByNivelId($nivelId, $ciclo)
    {
        $sql = "SELECT anios.nombre as anioName, nodoanios.* 
                FROM nodoanios 
                JOIN anios ON anios.id = nodoanios.anio_id
                WHERE nodoniv_id = $nivelId
                AND estado = 1
                AND cicloa_id = $ciclo
                ";

        $query = $this->db->query($sql);
        $res = $query->getResultArray();

        if(count($res)>0){
            return $res;
        }
        else{
            return false;
        }
    }

}