<?php namespace App\Models;

use CodeIgniter\Model;

class MunicipalidadesModel extends Model
{
    protected $table      = 'municipalidades';
    protected $primaryKey = 'id';

    protected $allowedFields = ['nombre','descripcion','web'];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
    }
}