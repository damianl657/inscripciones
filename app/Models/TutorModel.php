<?php namespace App\Models;

use CodeIgniter\Model;

class TutorModel extends Model
{
    protected $table      = 'tutor';
    protected $primaryKey = 'id';

    protected $allowedFields = ['apellido','nombre','documento','domicilio','telefono_particular','telefono_laboral','rol','correo','ocupacion','empresa','cargo', 'vive'];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('tutor');
    }
    public function actualizar_tutor($id, $datos)
    {
        if(!!$id){
            $this->builder->where('id', $id);
            return $this->builder->update($datos);
        }
        else return false;
    }
}