<?php namespace App\Models;

use CodeIgniter\Model;

class NivelesModel extends Model
{
    protected $table      = 'niveles';
    protected $primaryKey = 'id';

    protected $allowedFields = ['nombre','orden'];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
        $this->builder = $this->db->table('nodonivel');
    }

    //con especialidad
    public function getNivelesByColegiosIds($colegioId, $ciclo)
    {
        $sql = "SELECT niveles.nombre as nivelName, 
                        especialidades.id as especialidadId,
                        especialidades.nombre as especialidadName,
                        nodonivel.*
                FROM nodonivel
                JOIN niveles ON niveles.id = nodonivel.niveles_id
                LEFT JOIN especialidades ON especialidades.id = nodonivel.esp_id
                WHERE nodocolegio_id = $colegioId
                AND estado = 1
                AND cicloa_id = $ciclo
                ";

        $query = $this->db->query($sql);
        $res = $query->getResultArray();

        if(count($res)>0){
            return $res;
        }
        else{
            return false;
        }
    }

    //sin especialidad
    public function getNivelesSinEspByColegiosIds($colegioId, $ciclo)
    {
        $sql = "SELECT niveles.nombre as nivelName, 
                        niveles.abrev as nivelNameAbrev, 
                        nodonivel.*
                FROM nodonivel
                JOIN niveles ON niveles.id = nodonivel.niveles_id
                WHERE nodocolegio_id = $colegioId
                AND estado = 1
                AND cicloa_id = $ciclo
                GROUP BY niveles_id
                ";

        $query = $this->db->query($sql);
        $res = $query->getResultArray();

        if(count($res)>0){
            return $res;
        }
        else{
            return false;
        }
    }

    public function get_id_nivel($idcolegio)
    {
        $sql = "SELECT niveles.abrev,niveles.id,nodocolegio.tipo 
        FROM niveles 
        JOIN nodonivel ON nodonivel.niveles_id=niveles.id 
        JOIN nodocolegio ON nodocolegio.id=nodonivel.nodocolegio_id 
        WHERE nodonivel.nodocolegio_id =$idcolegio
        GROUP BY abrev";
        $query = $this->db->query($sql);
        return $query->getResultArray();
    }

}   