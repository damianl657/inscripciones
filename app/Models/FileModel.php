<?php namespace App\Models;

use CodeIgniter\Model;

class FileModel extends Model
{
    protected $table      = 'file';
    protected $primaryKey = 'id';

    protected $allowedFields = ['api_archivos_id','type','preinscripcion_id','nombre_div_id','carpeta','origen','estado'];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
    }

    public function get_archivos_preinscripcion_alumno($idpre)
    {
        $sql = "SELECT file.api_archivos_id,nombre_div.nombre FROM file
        JOIN preinscripcion ON preinscripcion.id=file.preinscripcion_id
        JOIN nombre_div ON nombre_div.id=file.nombre_div_id
        WHERE preinscripcion.id=$idpre";
        $query = $this->db->query($sql);
        return $query;

    }

    public function asignarFileAlumno($id_file,$id_preinscripcion,$div_file)
    {
       /* echo "string";
         echo $id_file." <br>";
        echo $id_preinscripcion." <br>";
        echo $div_file." <br>";*/
        $sql = "INSERT INTO file (nombre_div_id, api_archivos_id, preinscripcion_id) SELECT id, $id_file, $id_preinscripcion FROM nombre_div WHERE nombre = '$div_file'";
        $query = $this->db->query($sql);
        //echo "string";
        return $query;
    }

    public function delete_file($id_file)
    {
        if(is_numeric($id_file) and $id_file > 0)
        {
            $sql = "DELETE FROM file WHERE api_archivos_id = $id_file";
            $query = $this->db->query($sql);
            //echo "string";
            return $query;
        }
        
    }

 
}