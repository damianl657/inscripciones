<?php namespace App\Models;

use CodeIgniter\Model;

class NombredivModel extends Model
{
    protected $table      = 'nombre_div';
    protected $primaryKey = 'id';

    protected $allowedFields = ['nombre'];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_div_x_colegio($idcolegio)
    {
        $sql = "SELECT nombre_div.* FROM nodocolegio 
        JOIN nodonivel ON nodocolegio.id=nodonivel.nodocolegio_id
        JOIN nodoanios ON nodoanios.nodoniv_id=nodonivel.id 
        JOIN anios ON anios.id=nodoanios.anio_id
        JOIN nodoanios_nombre_div ON nodoanios_nombre_div.nodoanios_id=nodoanios.id
        JOIN nombre_div ON nombre_div.id=nodoanios_nombre_div.nombre_div_id
        WHERE nodocolegio.id=$idcolegio ";
        $query = $this->db->query($sql);
        return $query;
    }
    public function get_div_x_colegio_y_anios_alumno($idcolegio,$anio)
    {
        //print_r($anio);print_r($idcolegio);die();
        $sql = "SELECT nombre_div.nombre,nombre_div.nombre_mostrar  
        FROM nodocolegio 
        JOIN nodonivel ON nodocolegio.id=nodonivel.nodocolegio_id
        JOIN nodoanios ON nodoanios.nodoniv_id=nodonivel.id 
        JOIN anios ON anios.id=nodoanios.anio_id
        JOIN nodoanios_nombre_div ON nodoanios_nombre_div.nodoanios_id=nodoanios.id
        JOIN nombre_div ON nombre_div.id=nodoanios_nombre_div.nombre_div_id
        WHERE nodocolegio.id=$idcolegio AND anios.id=$anio AND nodoanios_nombre_div.alumno=1";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_div_x_colegio_y_anios_postulante($idcolegio,$anio)
    {
        //print_r($anio);die();
        $sql = "SELECT nombre_div.nombre,nombre_div.nombre_mostrar  
        FROM nodocolegio 
        JOIN nodonivel ON nodocolegio.id=nodonivel.nodocolegio_id
        JOIN nodoanios ON nodoanios.nodoniv_id=nodonivel.id 
        JOIN anios ON anios.id=nodoanios.anio_id
        JOIN nodoanios_nombre_div ON nodoanios_nombre_div.nodoanios_id=nodoanios.id
        JOIN nombre_div ON nombre_div.id=nodoanios_nombre_div.nombre_div_id
        WHERE nodocolegio.id=$idcolegio AND anios.id=$anio";
        $query = $this->db->query($sql);
        return $query;
    }
 
}