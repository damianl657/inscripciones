<?php

namespace App\Models;

use CodeIgniter\Model;

class DashboardModel extends Model
{
    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
        // $this->builder = $this->db->table('preinscripcion');
    }

    public function total_reserva($idcolegio)
    {
        $sql = "SELECT COUNT(preinscripcion.aceptado) as total
     FROM preinscripcion 
     JOIN alumno ON alumno.id=preinscripcion.alumno_id 
     JOIN anios ON anios.id=preinscripcion.anio_id 
     WHERE preinscripcion.colegio_id=$idcolegio";
        $query = $this->db->query($sql);
        return $query->getRow(0);
    }
    //recibir idcolegio no hardcodear
    public function total_reserva_secundario_anio($idcolegio, $anio)
    {
        $sql = "SELECT COUNT(preinscripcion.aceptado) as total
     FROM preinscripcion 
     JOIN alumno ON alumno.id=preinscripcion.alumno_id 
     JOIN anios ON anios.id=preinscripcion.anio_id 
     WHERE preinscripcion.colegio_id=$idcolegio
     AND anios.nombre=$anio";
        $query = $this->db->query($sql);
        return $query->getRow(0);
    }

    public function total_reserva_estado($idcolegio, $estado)
    {

        $sql = "SELECT COUNT(preinscripcion.aceptado) as total
        FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE preinscripcion.colegio_id=$idcolegio
        AND preinscripcion.aceptado=$estado";
        $query = $this->db->query($sql);
        return $query->getRow(0);
    }

    
    public function total_reserva_estado_por_anio($idcolegio, $estado, $sala)
    {
       
        $sql = "SELECT COUNT(preinscripcion.aceptado) as total_secundario 
        FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE preinscripcion.colegio_id=$idcolegio
        AND preinscripcion.aceptado=$estado
        AND anios.nombre=$sala";
        $query = $this->db->query($sql);
        return $query->getRow(0);
    }

    public function get_datos_preinscripcion_alumnos($idcolegio)
    {
        $sql = "SELECT preinscripcion.id, alumno.legajo, preinscripcion.observacion, alumno.documento,alumno.apellido,alumno.nombre,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,preinscripcion.estado_economico
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE alumno.legajo IS NOT NULL 
        AND preinscripcion.colegio_id=$idcolegio";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_datos_preinscripcion_postulantes($idcolegio)
    {
        $sql = "SELECT preinscripcion.id, preinscripcion.observacion,alumno.documento, alumno.apellido, alumno.nombre, alumno.escuela_proviene,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,preinscripcion.estado_economico
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE alumno.legajo IS NULL 
        AND preinscripcion.colegio_id=$idcolegio";
        $query = $this->db->query($sql);
        return $query;
    }
    public function get_datos_preinscripcion_alumnos_x_anio($idcolegio,$anio)
    {
        $sql = "SELECT preinscripcion.id, preinscripcion.observacion, alumno.legajo,alumno.documento,alumno.apellido,alumno.nombre ,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,preinscripcion.estado_economico
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN niveles on niveles.id=preinscripcion.niveles_id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE alumno.legajo IS NOT NULL 
        AND preinscripcion.colegio_id=$idcolegio
        AND anios.nombre=$anio";
            $query = $this->db->query($sql);
            return $query;
       
    }

    public function get_datos_preinscripcion_postulantes_x_anio($idcolegio,$anio)
    {
        $sql = " SELECT preinscripcion.observacion, preinscripcion.id,alumno.documento, alumno.apellido, alumno.nombre, alumno.escuela_proviene,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,preinscripcion.estado_economico
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN niveles on niveles.id=preinscripcion.niveles_id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE alumno.legajo IS NULL 
        AND preinscripcion.colegio_id=$idcolegio
        AND anios.nombre=$anio";
            $query = $this->db->query($sql);
            return $query;
       
    }
}
