<?php

namespace App\Models;

use CodeIgniter\Model;

class ColegioModel extends Model
{
    protected $table      = 'nodocolegio';
    protected $primaryKey = 'id';

    protected $allowedFields = ['nombre', 'tipo', 'logo', 'planestudio', 'cicloa', 'estado', 'link', 'titulo_web', 'ruta_datos'];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
        $this->builder = $this->db->table('nodocolegio');
    }

    public function getColegiosByIds($ids)
    {
        $sql = "SELECT * 
                FROM nodocolegio
                WHERE id IN ($ids)";
        $query = $this->db->query($sql);
        $res = $query->getResultArray();

        if (count($res) > 0) {
            return $res;
        } else {
            return false;
        }
    }

    public function obtener_planestudio($anioA)
    {
        $sql = "SELECT id FROM planesestudio WHERE nombre='$anioA' LIMIT 1";
        $query = $this->db->query($sql);
        $res = $query->getResultArray();

        if (count($res) > 0) {
            return $res[0];
        } else {
            return false;
        }
    }
    public function get_colegios_x_nivel($nivel)
    {
       
        $sql = "SELECT nodocolegio.id FROM nodocolegio WHERE tipo='$nivel'";
        $query = $this->db->query($sql);
        $res = $query->getResultArray();

        if (count($res) > 0) {
            return $res;
        } else {
            return false;
        }
    }
   
}
