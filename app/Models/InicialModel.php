<?php

namespace App\Models;

use CodeIgniter\Model;

class InicialModel extends Model
{
    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('preinscripcion');
    }
 //recibir idcolegio no hardcodear
 public function total_reserva_por_sala($idcolegio,$sala)
 {
     //$idcolegio = 37;
     $sql = "SELECT COUNT(preinscripcion.aceptado) as total_inicial 
     FROM preinscripcion 
     JOIN alumno ON alumno.id=preinscripcion.alumno_id 
     JOIN anios ON anios.id=preinscripcion.anio_id 
     WHERE preinscripcion.colegio_id=$idcolegio
     AND anios.nombre=$sala";
     $query = $this->db->query($sql);
     return $query->getRow(0);
 }

    //recibir idcolegio no hardcodear
    public function total_reserva_estado_por_sala($idcolegio,$estado,$sala)
    {
       // echo $estado,$sala,$idcolegio;
       //die();
        //$idcolegio = 37;
        $sql = "SELECT COUNT(preinscripcion.aceptado) as total_inicial 
        FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE preinscripcion.colegio_id=$idcolegio
        AND preinscripcion.aceptado=$estado
        AND anios.nombre=$sala";
        $query = $this->db->query($sql);
        return $query->getRow(0);
    }

    public function get_datos_preinscripcion_sala4()
    {
        $sql = " SELECT preinscripcion.id,alumno.documento,alumno.apellido,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,niveles.nombre,alumno.escuela_proviene 
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN niveles on niveles.id=preinscripcion.niveles_id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE niveles.nombre='INICIAL' AND anios.nombre='Sala de 4'";
            $query = $this->db->query($sql);
            return $query;
       
    }

    public function get_datos_preinscripcion_sala5_alumnos()
    {
        $sql = " SELECT preinscripcion.id,alumno.documento,alumno.apellido,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,niveles.nombre 
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN niveles on niveles.id=preinscripcion.niveles_id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE alumno.escuela_proviene IS NULL AND niveles.nombre='INICIAL' AND anios.nombre='Sala de 5'";
            $query = $this->db->query($sql);
            return $query;
       
    }

    public function get_datos_preinscripcion_sala5_postulantes()
    {
        $sql = " SELECT preinscripcion.id,alumno.documento,alumno.apellido,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,niveles.nombre 
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN niveles on niveles.id=preinscripcion.niveles_id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE alumno.escuela_proviene IS NOT NULL AND niveles.nombre='INICIAL' AND anios.nombre='Sala de 5'";
            $query = $this->db->query($sql);
            return $query;
       
    }









    

    public function get_administracion_tutor_alumno($idpre)
    {
        $sql = "SELECT preinscripcion.id AS id_pre,alumno.*,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,niveles.nombre,nacionalidad.nombre,tutor.* 
    FROM alumno 
    JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id 
    JOIN niveles on niveles.id=preinscripcion.niveles_id 
    JOIN anios ON anios.id=preinscripcion.anio_id 
    JOIN nacionalidad ON nacionalidad.id=alumno.nacionalidad_id 
    JOIN provincias ON provincias.id=alumno.provincia_id 
    JOIN tutoralumno ON alumno.id=tutoralumno.alumno_id 
    JOIN tutor ON tutoralumno.tutor_id=tutor.id 
    WHERE alumno.escuela_proviene IS NULL AND preinscripcion.id=$idpre";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_datos_pre_alumno($idpre)
    {
        $sql = "SELECT alumno.*,divisiones.nombre AS division_actual,anios.nombre AS anio_actual, provincias.provincia, preinscripcion.observacion FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN provincias ON provincias.id=alumno.provincia_id
        JOIN anios ON alumno.anio_id=anios.id
        JOIN divisiones ON alumno.division_id=divisiones.id
        WHERE preinscripcion.id=$idpre";
        $query = $this->db->query($sql);
        return $query;
    }

    public function modificar_obser($idpre,$obser)
    {
        $sql = "UPDATE preinscripcion SET preinscripcion.observacion='$obser' WHERE id=$idpre";
            $query = $this->db->query($sql);   
            return $obser;
    }

    public function get_anio_reserva_alumno($idpre)
    {
        $sql = "SELECT anios.nombre AS anio_reserva, niveles.nombre AS nombre_nivel FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN provincias ON provincias.id=alumno.provincia_id
        JOIN anios ON preinscripcion.anio_id=anios.id
        JOIN niveles ON preinscripcion.niveles_id=niveles.id
        WHERE preinscripcion.id=$idpre";
        $query = $this->db->query($sql);
        return $query;
    }

    public function modificar_preinscripcion($preinscripcion_id, $datos_pre)
    {
        $this->builder->where('id', $preinscripcion_id);
        $this->builder->update($datos_pre);
    }

    
}