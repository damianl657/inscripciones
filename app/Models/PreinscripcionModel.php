<?php

namespace App\Models;

use CodeIgniter\Model;

class PreinscripcionModel extends Model
{
    protected $db;
    protected $builder;

    protected $table      = 'preinscripcion';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['alumno_id', 'colegio_id', 'anio_id', 'niveles_id', 'aceptado', 'tutor_id', 'especialidad_id', 'observacion'];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('preinscripcion');
    }

    public function get_alumno_preinscripto($alumno, $idcolegio)
    {
        $sql = "SELECT * FROM preinscripcion WHERE alumno_id=$alumno AND colegio_id=$idcolegio";
        $query = $this->db->query($sql);
        $row = $query->getResultArray();
        if ($row) {
            $data['status'] = true;
            $data['inscripto'] = $row;
            return $data;
        } else {
            $data['status'] = false;
            $data['inscripto'] = false;
            return $data;
        }
    }
    public function get_datos_preinscripcion_alumno()
    {
        $sql = "SELECT preinscripcion.id,alumno.documento,alumno.apellido,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,niveles.nombre 
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN niveles on niveles.id=preinscripcion.niveles_id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE alumno.escuela_proviene IS NULL";
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_datos_preinscripcion_postulante()
    {
        $sql = "SELECT preinscripcion.id,alumno.documento,alumno.apellido,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,niveles.nombre,alumno.escuela_proviene 
        FROM alumno 
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        JOIN niveles on niveles.id=preinscripcion.niveles_id
        JOIN anios ON anios.id=preinscripcion.anio_id 
        WHERE alumno.escuela_proviene IS NOT NULL";
        $query = $this->db->query($sql);
        return $query;
    }

    public function total_reserva_alumnos_por_colegio($idcolegio)
    {
        $sql = "SELECT COUNT(preinscripcion.aceptado) as total_colegio 
        FROM preinscripcion 
        WHERE preinscripcion.colegio_id=$idcolegio";
        $query = $this->db->query($sql);
        return $query->getRow(0);
    }

    public function total_reserva_donbosco()
    {
        $sql = "SELECT COUNT(preinscripcion.colegio_id ) as total_totales 
    FROM preinscripcion 
    JOIN alumno ON alumno.id=preinscripcion.alumno_id 
    WHERE preinscripcion.colegio_id=33 
    OR preinscripcion.colegio_id=32 
    OR preinscripcion.colegio_id=37";
        $query = $this->db->query($sql);
        return $query->getRow(0);
    }

    public function get_administracion_tutor_alumno($idpre)
    {
        $sql = "SELECT preinscripcion.id AS id_pre,alumno.*,anios.nombre as anio_inscripto,preinscripcion.aceptado, DATE_FORMAT(CAST(preinscripcion.created_at as Date), '%d/%m/%Y') as fecha,niveles.nombre,nacionalidad.nombre,tutor.* 
    FROM alumno 
    JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id 
    JOIN niveles on niveles.id=preinscripcion.niveles_id 
    JOIN anios ON anios.id=preinscripcion.anio_id 
    JOIN nacionalidad ON nacionalidad.id=alumno.nacionalidad_id 
    JOIN provincias ON provincias.id=alumno.provincia_id 
    JOIN tutoralumno ON alumno.id=tutoralumno.alumno_id 
    JOIN tutor ON tutoralumno.tutor_id=tutor.id 
    WHERE alumno.escuela_proviene IS NULL AND preinscripcion.id=$idpre";
        $query = $this->db->query($sql);
        return $query;
    }


    /**
     * Recibe el id de reserva y deuelve los datos del alumno en caso de no ser ingresante y no tener anio actual se usa el if
     *
     * @param  mixed $idpre
     * @return $query
     */
    public function get_datos_pre_alumno($idpre)
    {
        $sql = "SELECT alumno.*,divisiones.nombre AS division_actual,anios.nombre AS anio_actual, provincias.provincia, provincias.id AS provincia_id, preinscripcion.observacion, preinscripcion.aceptado, preinscripcion.observacion_tutor, preinscripcion.estado_economico, preinscripcion.colegio_id,preinscripcion.anio_id
        FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN provincias ON provincias.id=alumno.provincia_id
        JOIN anios ON alumno.anio_id=anios.id
        JOIN divisiones ON alumno.division_id=divisiones.id
        WHERE preinscripcion.id=$idpre";
        $query = $this->db->query($sql);
        if (empty($query->getResultArray())) {
            $sql = "SELECT  alumno.id, alumno.apellido, alumno.nombre,alumno.documento,alumno.fecha_nac,alumno.sexo,alumno.domicilio, alumno.telefono,alumno.correo, alumno.ciudad, alumno.legajo,alumno.escuela_proviene,alumno.ifnodos, alumno.nodonivel_id, alumno.division_id AS division_actual,alumno.anio_id AS anio_actual, provincias.provincia, provincias.id AS provincia_id, preinscripcion.observacion, preinscripcion.observacion_tutor, preinscripcion.aceptado, preinscripcion.colegio_id,preinscripcion.anio_id, preinscripcion.estado_economico 
            FROM preinscripcion 
            JOIN alumno ON alumno.id=preinscripcion.alumno_id 
            JOIN provincias ON provincias.id=alumno.provincia_id
            WHERE preinscripcion.id=$idpre";
            $query = $this->db->query($sql);
        }
        return $query;
    }

    public function get_datos_pre_postulante($idpre)
    {
        $sql = "SELECT alumno.*, provincias.provincia, provincias.id AS provincia_id, preinscripcion.observacion, preinscripcion.aceptado,preinscripcion.observacion_tutor, preinscripcion.colegio_id,preinscripcion.anio_id
        FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN provincias ON provincias.id=alumno.provincia_id
        WHERE preinscripcion.id=$idpre";
        $query = $this->db->query($sql);
        return $query;
    }

    public function modificar_obser($idpre, $obser)
    {
        $sql = "UPDATE preinscripcion SET preinscripcion.observacion='$obser' WHERE id=$idpre";
        $query = $this->db->query($sql);
        return $obser;
    }

    public function modificar_obser_tutor($idpre, $obser)
    {
        $sql = "UPDATE preinscripcion SET preinscripcion.observacion_tutor='$obser' WHERE id=$idpre";
        $query = $this->db->query($sql);
        return $obser;
    }

    public function get_anio_reserva_alumno($idpre)
    {
        $sql = "SELECT anios.nombre AS anio_reserva, niveles.nombre AS nombre_nivel, preinscripcion.niveles_id, preinscripcion.anio_id 
        FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN provincias ON provincias.id=alumno.provincia_id
        JOIN anios ON preinscripcion.anio_id=anios.id
        JOIN niveles ON preinscripcion.niveles_id=niveles.id
        WHERE preinscripcion.id=$idpre";
        $query = $this->db->query($sql);
        return $query;
    }

    public function modificar_preinscripcion($preinscripcion_id, $datos_pre)
    {
        //print_r($preinscripcion_id);print_r($datos_pre);die();
        $this->builder->where('id', $preinscripcion_id);
        $this->builder->update($datos_pre);
    }

    public function get_info_preinscripcion_sincronizar_alumnos($idcolegio)
    {
        $sql = "SELECT preinscripcion.id,preinscripcion.colegio_id,preinscripcion.anio_id,preinscripcion.niveles_id,preinscripcion.especialidad_id,alumno.id as alumno_id, alumno.apellido AS alu_apellido,alumno.nombre AS alu_nombre ,alumno.sexo AS alu_sexo,alumno.documento AS alu_documento,alumno.legajo,alumno.correo AS alu_correo,alumno.domicilio AS alu_domicilio,alumno.telefono AS alu_telefono,alumno.fecha_nac,divisiones.nombre as nombre_division
        FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        JOIN divisiones on divisiones.id=alumno.division_id 
        WHERE preinscripcion.colegio_id=$idcolegio 
        AND preinscripcion.estado_economico=2 
        AND alumno.ifnodos=0
        UNION 
        SELECT preinscripcion.id,preinscripcion.colegio_id,preinscripcion.anio_id,preinscripcion.niveles_id,preinscripcion.especialidad_id,alumno.id as alumno_id, alumno.apellido AS alu_apellido,alumno.nombre AS alu_nombre ,alumno.sexo AS alu_sexo,alumno.documento AS alu_documento,alumno.legajo,alumno.correo AS alu_correo,alumno.domicilio AS alu_domicilio,alumno.telefono AS alu_telefono,alumno.fecha_nac,alumno.division_id as nombre_division 
        FROM preinscripcion 
        JOIN alumno ON alumno.id=preinscripcion.alumno_id 
        WHERE preinscripcion.colegio_id=$idcolegio
        AND preinscripcion.estado_economico=2 
        AND alumno.ifnodos=0
        AND alumno.division_id=0 ";
        $query = $this->db->query($sql);
        if ($query) {
            return $query;
        } else return false;
    }
}
