<?php namespace App\Models;

use CodeIgniter\Model;

class NacionalidadModel extends Model
{
    protected $table      = 'nacionalidad';
    protected $primaryKey = 'id';

    protected $allowedFields = ['nombre'];

    protected $returnType = 'array';
    protected $useSoftDeletes = false;

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
    }
    public function get_tabla_nacionalidad() {

        $sql = "SELECT * FROM nacionalidad ";
        $query = $this->db->query($sql);
        return $query->getResult();
    }
}