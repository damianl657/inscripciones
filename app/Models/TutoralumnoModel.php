<?php

namespace App\Models;

use CodeIgniter\Model;

class TutoralumnoModel extends Model
{
    protected $table      = 'tutoralumno';
    protected $primaryKey = 'id';

    protected $allowedFields = ['tutor_id', 'alumno_id'];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
    }

    public function buscar_parentesco($alumno_id, $tutor_id)
    {
        $sql = "SELECT * FROM tutoralumno WHERE alumno_id=$alumno_id AND tutor_id=$tutor_id ";
        $query = $this->db->query($sql);
        return $query->getResult();
    }
    public function buscar_tutor_de($alumno_id, $tutor_id)
    {
        $sql = "SELECT * FROM tutoralumno WHERE alumno_id=$alumno_id";
        $query = $this->db->query($sql);
        return $query->getResult();
    }
    public function buscar_hijo_de($tutor_id)
    {
        $sql = "SELECT * FROM tutoralumno WHERE tutor_id=$tutor_id ";
        $query = $this->db->query($sql);
        return $query->getResult();
    }

    public function get_hijos_x_tutor_con_preinscripcion($dni_tutor)
    {
        $sql = "SELECT alumno.id, alumno.nombre,alumno.anio_id as anio_actual, preinscripcion.colegio_id, preinscripcion.anio_id, preinscripcion.niveles_id,preinscripcion.tutor_id,preinscripcion.especialidad_id,preinscripcion.aceptado,preinscripcion.estado_economico,preinscripcion.observacion, preinscripcion.observacion_tutor, preinscripcion.id AS id_reserva FROM tutoralumno
        JOIN tutor ON tutor.id=tutoralumno.tutor_id
        JOIN alumno ON alumno.id=tutoralumno.alumno_id
        JOIN preinscripcion ON preinscripcion.alumno_id=alumno.id
        WHERE tutor.documento=$dni_tutor";
        $query = $this->db->query($sql);
        return $query->getResultArray();
    }

    public function get_dni_tutor($dni)
    {
        $sql = "SELECT tutor.documento FROM `alumno` 
        JOIN tutoralumno ON alumno.id=tutoralumno.alumno_id
        JOIN tutor ON tutor.id=tutoralumno.tutor_id
        WHERE alumno.documento=$dni";
        $query = $this->db->query($sql);
        return $query->getResultArray();
    }
}
