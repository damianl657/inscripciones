<?php namespace App\Models;

use CodeIgniter\Model;

class AlumnoModel extends Model
{
    protected $table      = 'alumno';
    protected $primaryKey = 'id';

    protected $allowedFields = ['apellido','nombre','documento','fecha_nac','sexo','domicilio','correo','telefono','otro_telefono','ciudad','provincia_id','nacionalidad_id','lagajo','escuela_proviene'];

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
        $this->builder = $this->db->table('alumno');
    }
    public function actualizar_alumno($alumno_id, $datos_alumno)
    {
        $this->builder->where('id', $alumno_id);
        $this->builder->update($datos_alumno);
    }

    public function get_info_padres($idalumno)
    {
        $sql = "SELECT tutor.* FROM alumno
        JOIN tutoralumno ON alumno.id=tutoralumno.alumno_id
        JOIN tutor ON tutor.id=tutoralumno.tutor_id
        WHERE alumno.id=$idalumno";
        $query = $this->db->query($sql);
        return $query;
    }

    public function actualizar_if_nodos($id)
    {
        $sql = "UPDATE alumno SET ifnodos = '1' WHERE alumno.id = $id";
        $query = $this->db->query($sql);
    }

    public function actualizar_no_if_nodos($dni)
    {
        $sql = "UPDATE alumno SET ifnodos = '2' WHERE alumno.documento = $dni";
        $query = $this->db->query($sql);
     
    }

    public function get_info_tutores_para_sincronizar($idalumno)
    {
        $sql = "SELECT tutor.apellido, tutor.nombre, tutor.rol AS sexo, tutor.documento AS dni, tutor.correo AS email, tutor.domicilio, tutor.telefono_particular AS telefono
        FROM alumno
        JOIN tutoralumno ON alumno.id=tutoralumno.alumno_id
        JOIN tutor ON tutor.id=tutoralumno.tutor_id
        WHERE alumno.id=$idalumno";
        $query = $this->db->query($sql);
        if($query){
            return $query->getResult();
        } else return false;
    }
}