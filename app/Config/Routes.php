<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */

$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Front');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

$routes->get('/donboscoinicial', 'Front::inicio/37');
$routes->get('/donboscoprimaria', 'Front::inicio/33');
$routes->get('/donboscosecundaria', 'Front::inicio/32');
$routes->get('/dantealighieriinicial', 'Front::inicio/42');
$routes->get('/dantealighieriprimaria', 'Front::inicio/19');
$routes->get('/dantealighierisecundaria', 'Front::inicio/20');
/*
$routes->get('/donboscoinicial', 'Mantenimiento::index');
$routes->get('/donboscoprimaria', 'Mantenimiento::index');
$routes->get('/donboscosecundaria', 'Mantenimiento::index');
$routes->get('/dantealighieriinicial', 'Mantenimiento::index');
$routes->get('/dantealighieriprimaria', 'Mantenimiento::index');
$routes->get('/dantealighierisecundaria', 'Mantenimiento::index');
$routes->get('/donboscoinicial/(:any)', 'Mantenimiento::index');
$routes->get('/donboscoprimaria/(:any)', 'Mantenimiento::index');
$routes->get('/donboscosecundaria/(:any)', 'Mantenimiento::index');
$routes->get('/dantealighieriinicial/(:any)', 'Mantenimiento::index');
$routes->get('/dantealighieriprimaria/(:any)', 'Mantenimiento::index');
$routes->get('/dantealighierisecundaria/(:any)', 'Mantenimiento::index');
*/
//$routes->get('/colegiocantealighierisecundaria/cargadatos', 'cargardatos::inicio/20');
/*
$routes->post('resultado', 'Front::resultado');
$routes->post('resultadopaso', 'Front::resultado_paso');
$routes->get('preguntas-frecuentes', 'Front::preguntasfrecuentes');
$routes->get('contacto', 'Front::contacto');
$routes->post('contactoenviar', 'Front::contactoenviar');

$routes->get('panel', 'Dashboard::panel');

$routes->get('departamento', 'Departamento::index');
$routes->get('departamento/addmod/(:num)', 'Departamento::addmod/$1');
$routes->get('departamento/edit/(:num)', 'Departamento::edit/$1');
$routes->get('departamento/del/(:num)', 'Departamento::del/$1');

$routes->resource('api/departamento', ['controller' =>'Departamento_api']);

$routes->group('usuario', ['namespace' => 'App\Controllers'], function ($routes) {
	$routes->add('acceder', 'Usuario::acceder');
	$routes->get('salir', 'Usuario::salir');
	$routes->get('listar', 'Usuario::listar');
	$routes->add('olvido_clave', 'Usuario::olvido_clave');
	$routes->add('cambiar_clave', 'Usuario::cambiar_clave');
	$routes->add('editar/(:num)', 'Usuario::editar/$1');
	$routes->add('activar/(:num)', 'Usuario::activar/$1');
	$routes->add('desactivar/(:num)', 'Usuario::desactivar/$1');
	$routes->add('editar_grupo/(:num)', 'Usuario::editar_grupo/$1');
	$routes->add('crear_grupo', 'Usuario::crear_grupo');
	
});

*/
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
