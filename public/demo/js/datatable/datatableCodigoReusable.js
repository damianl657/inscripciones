function dataTablese(tabla, direccionControlador, ocultar) {
    var aux = [ocultar, 4];
    var table = $('#' + tabla).DataTable({
        "responsive": true,
        "columnDefs": [{

                "targets": aux,
                "visible": false,
                "searchable": false,
            },


        ],
        dom: 'Bfrtip',
        buttons: [
            'colvis', 'excel', 'print'
        ],
        destroy: true,
        "ajax": {
            url: direccionControlador,
            type: 'POST'
        },

        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad",
                "print": "Imprimir"
            }
        }

    });
    return table;

}

function dataTableMatriz(tabla, direccionControlador, ocultar) {
    var table = $('#' + tabla).DataTable({

        "columnDefs": [{
                //"targets": [0,1,2,3],
                "targets": ocultar,
                "visible": false,
                "searchable": false
            },
            {
                "targets": 0,
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "searchable": false,
                "defaultContent": '',
                "render": function() {
                    return '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                },
                width: "20px"
            },
            {
                "targets": -1,
                "data": null,
                "defaultContent": "<button type='button' id='botonEditar' name='editar' class='editar btn btn-primary btn-sm ml-auto'><span class='far fa-edit'></span></button>\n\
                               <button type='button' id='botonEliminar' name='eliminar' class='eliminar btn btn-danger btn-sm ml-auto'><i class='far fa-trash-alt'></i></button>"
            }
        ],
        dom: 'Bfrtip',
        buttons: [
            'colvis', 'excel', 'print'
        ],
        destroy: true,
        "ajax": {
            url: direccionControlador,
            type: 'GET'
        },

        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla =(",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad",
                "print": "Imprimir"
            }
        }

    });
    return table;

}