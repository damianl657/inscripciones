
function autoCompletar(campoDeBusqueda, direccionControlador, id, campoQueMuestra,modificarInput) {

    // Iniciar
    $('#' + campoDeBusqueda).flexdatalist({
        valueProperty: id,
        minLength: 3,
        selectionRequired: modificarInput,
        visibleProperties: [campoQueMuestra],
        searchContain: true,
        searchDelay: 200,
        searchIn: campoQueMuestra,
        noResultsText: 'No se encontraron coincidencias para "{keyword}"',
        data: direccionControlador
    });

}

//Version que muestra el id u otro campo mas
function autoCompletarMultipleMatriz(campoDeBusqueda, direccionControlador, id, campoQueMuestra, dondeMuestraMonto) {
    var suma = 0;
    var arregloDeterminacion = [];
    $('#' + campoDeBusqueda).flexdatalist({
        valueProperty: id,
        minLength: 3,
        selectionRequired: true,
        visibleProperties: [campoQueMuestra],
        textProperty: '{dete_monto}',
        searchContain: true,
        searchDelay: 200,
        removeOnBackspace: false,
        searchIn: campoQueMuestra,
        noResultsText: 'No se encontraron coincidencias para "{keyword}"',
        data: direccionControlador
    }).on("select:flexdatalist", function (event, data) {
        if (arregloDeterminacion.indexOf(data.id_determinacion) == -1) {
            if ($('#' + dondeMuestraMonto).val().length === 0) {
                suma = data.dete_monto;
                arregloDeterminacion.push(data.id_determinacion);
            } else {
                suma = parseInt($('#' + dondeMuestraMonto).val()) + parseInt(data.dete_monto);
                arregloDeterminacion.push(data.id_determinacion);
            }
            $('#' + dondeMuestraMonto).val(suma);
        }
    });
    $('#' + campoDeBusqueda).on('after:flexdatalist.remove', function (result, val, eliminado) {
        suma = suma - parseInt(eliminado[0].value.dete_monto);
        var id = arregloDeterminacion.indexOf(eliminado[0].value.id_determinacion);
        if (id > -1) {
            arregloDeterminacion.splice(id, 1);
         }
        $('#' + dondeMuestraMonto).val(suma);
    });
}

function autoCompletarMultipleMatrizEditar(campoDeBusqueda, direccionControlador, id, campoQueMuestra, dondeMuestraMonto,arregloDeterminacion) {
    var suma = 0;
    //var arregloDeterminacion = [];
    $('#' + campoDeBusqueda).flexdatalist({
        valueProperty: id,
        minLength: 3,
        selectionRequired: true,
        visibleProperties: [campoQueMuestra],
        textProperty: '{dete_monto}',
        searchContain: true,
        searchDelay: 200,
        removeOnBackspace: false,
        searchIn: campoQueMuestra,
        noResultsText: 'No se encontraron coincidencias para "{keyword}"',
        data: direccionControlador
    }).on("select:flexdatalist", function (event, data) {
        if (arregloDeterminacion.indexOf(data.id_determinacion) == -1) {
            if ($('#' + dondeMuestraMonto).val().length === 0) {
                suma = data.dete_monto;
                arregloDeterminacion.push(data);
            } else {
                suma = parseInt($('#' + dondeMuestraMonto).val()) + parseInt(data.dete_monto);
                arregloDeterminacion.push(data);
            }
            $('#' + dondeMuestraMonto).val(suma);
        }
    });
    $('#' + campoDeBusqueda).on('after:flexdatalist.remove', function (result, val, eliminado) {
        var i;
                        for (i = 0; i < arregloDeterminacion.length; i++) {
                            if(arregloDeterminacion[i].id_determinacion==eliminado[0].value[0]){
                            suma = parseInt($('#' + dondeMuestraMonto).val()) - parseInt(arregloDeterminacion[i].dete_monto);
                            if (id > -1) {
                                arregloDeterminacion.splice(id, 1);
                             }
                            $('#' + dondeMuestraMonto).val(suma);
                            i=arregloDeterminacion.length;
                            }
                        }
    });
}